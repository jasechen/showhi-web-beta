(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");

    (function () {

        __ngApp.constant("$FILE_CACHE_TIME", "?v=" + ( new Date() ).getTime()) ;
        __ngApp.constant("$NECESSARY_API_KEY", "necessary-api-key");
        __ngApp.constant("$OPTIONAL_API_KEY", "optional-api-key");
        __ngApp.constant("$TEMPLATE_PATH", "/asset/template/");

    }());

}(window.allot));
(function (__allot, __angular) {

    "use strict";

    var __config = __allot("config");

    (function () {

        var _ngApp = __angular.module(__config.NGAPP_NAME, ["ngRoute"]);
        _runInit.$inject = ["$rootScope", "$window", "$location", "$session", "$account", "$translator"];
        _ngApp.run(_runInit);
        __allot("ng", _ngApp);

    }());

    function _runInit(_rootScope, _window, _location, _session, _account, _translator) {

        _rootScope.initialled = false;
        _rootScope.$on("$locationChangeStart", _onLocationChange);
        _translator.set();

        function _onLocationChange() {
            window.scrollTo(0, 0);
            console.groupEnd();
            console.group(_location.path());
            _session.read(_onSessionAllow, _onNoSession, _onSessionOut, _onSessionError, _onFinish);
        }

        function _onSessionAllow() {
            _rootScope.isLogined = true;
            if ( !_account.confirmToken() ) return _account.logout();
            if ( _confirmPrivacyPath(false) ) return _location.path("/");
        }

        function _onNoSession() {
            if ( _confirmPrivacyPath(true) ) _location.path("/home");
            else _session.create(_onSessionCreated, null, _onFinish);
        }

        function _onSessionOut() {
            _rootScope.isLogined = false;
            if ( _confirmPrivacyPath(true) ) _location.path("/home");
        }

        function _onSessionError() {
            _rootScope.isLogined = false;
            _session.create(_onSessionRestore, null, _onFinish);
        }

        function _onSessionCreated() {
            _onLocationChange();
        }

        function _onSessionRestore() {
            _window.location.reload();
        }

        function _onFinish() {
            _rootScope.initialled = true;
            _rootScope.$apply();
        }

        function _confirmPrivacyPath(_judgePrivate) {
            var _path = _location.path();
            var _isPublicPath = /^\/(publicgroups)|404/.test(_path);
            var _isUnloginPath = /^\/(home|register|verify|langpicker-normal|pedia)/.test(_path);
            if ( _isPublicPath ) return false;
            _judgePrivate = _judgePrivate || false ;
            return ( _judgePrivate !== _isUnloginPath );
        }

    }

}(window.allot, window.angular));
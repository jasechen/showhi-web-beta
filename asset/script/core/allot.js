(function (__allotConfig) {

    "use strict";

    var _allotDict;

    (function () {

        _allotDict = {};
        window.allot = window.allot || _allotter ;
        if ( __allotConfig ) _allotter("config", __allotConfig);
        delete window.allotConfig;

    }());

    function _allotter(_key, _source) {

        if ( _source ) _allotDict[_key] = _source;
        return _allotDict[_key];

    }

}(window.allotConfig));
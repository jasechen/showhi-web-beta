(function (__allot, __angular) {

    "use strict";

    var __config = __allot("config");

    (function () {

        __angular.bootstrap(document, [__config.NGAPP_NAME]);

    }());

}(window.allot, window.angular));
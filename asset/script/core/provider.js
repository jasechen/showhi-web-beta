(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");

    (function () {

        _configProvider.$inject = ["$locationProvider", "$routeProvider", "$TEMPLATE_PATH", "$FILE_CACHE_TIME"];
        __ngApp.config(_configProvider);

    }());

    function _configProvider(_locationProvider, _routeProvider, _TEMPLATE_PATH, _FILE_CACHE_TIME) {

        _locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        _routeProvider
            .when("/", {
                templateUrl: _TEMPLATE_PATH + "dashboard.html" + _FILE_CACHE_TIME ,
                controller: "DashboardController"
            })
            .when("/user/:userId", {
                templateUrl: _TEMPLATE_PATH + "dashboard.html" + _FILE_CACHE_TIME ,
                controller: "DashboardController"
            })
            .when("/profile/:userId", {
                templateUrl: _TEMPLATE_PATH + "profile.html" + _FILE_CACHE_TIME ,
                controller: "ProfileController"
            })
            .when("/group/:groupID", {
                templateUrl: _TEMPLATE_PATH + "group.html" + _FILE_CACHE_TIME ,
                controller: "GroupController"
            })
            .when("/newgroup", {
                templateUrl: _TEMPLATE_PATH + "newgroup.html" + _FILE_CACHE_TIME ,
                controller: "NewgroupController"
            })
            .when("/groupmember/:groupID", {
                templateUrl: _TEMPLATE_PATH + "groupmember.html" + _FILE_CACHE_TIME ,
                controller: "GroupmemberController"
            })
            .when("/groupedit/:groupID", {
                templateUrl: _TEMPLATE_PATH + "groupedit.html" + _FILE_CACHE_TIME ,
                controller: "GroupeditController"
            })
            .when("/publicgroups", {
                templateUrl: _TEMPLATE_PATH + "publicgroups.html" + _FILE_CACHE_TIME ,
                controller: "PublicgroupsController"
            })
            .when("/invite", {
                templateUrl: _TEMPLATE_PATH + "invite.html" + _FILE_CACHE_TIME ,
                controller: "InviteController"
            })
            .when("/groupinvite/:groupId", {
                templateUrl: _TEMPLATE_PATH + "invite.html" + _FILE_CACHE_TIME ,
                controller: "GroupinviteController"
            })
            .when("/appointmaker/:groupId", {
                templateUrl: _TEMPLATE_PATH + "appointmaker.html" + _FILE_CACHE_TIME ,
                controller: "AppointmakerController"
            })
            .when("/appoint/:groupId/:memberId", {
                templateUrl: _TEMPLATE_PATH + "appoint.html" + _FILE_CACHE_TIME ,
                controller: "AppointController"
            })
            .when("/access/:groupId", {
                templateUrl: _TEMPLATE_PATH + "access.html" + _FILE_CACHE_TIME ,
                controller: "AccessController"
            })
            .when("/langpicker", {
                templateUrl: _TEMPLATE_PATH + "langpicker.html" + _FILE_CACHE_TIME ,
                controller: "LangpickerController"
            })
            .when("/langpicker-normal", {
                templateUrl: _TEMPLATE_PATH + "langpicker.html" + _FILE_CACHE_TIME ,
                controller: "LangpickerController"
            })
            .when("/register/:qrcodeID", {
                templateUrl: _TEMPLATE_PATH + "register.html" + _FILE_CACHE_TIME ,
                controller: "RegisterController"
            })
            .when("/verify/:userID", {
                templateUrl: _TEMPLATE_PATH + "verify.html" + _FILE_CACHE_TIME ,
                controller: "VerifyController"
            })
            .when("/home", {
                templateUrl: _TEMPLATE_PATH + "home.html" + _FILE_CACHE_TIME ,
                controller: "HomeController"
            })
            .when("/404", {
                templateUrl: _TEMPLATE_PATH + "404.html" + _FILE_CACHE_TIME
            })
            .when("/r/s/n/pedia", {
                templateUrl: _TEMPLATE_PATH + "pedia.html" + _FILE_CACHE_TIME ,
                controller: "PediaController"
            })
            .when("/r/s/n/pedia/edit", {
                templateUrl: _TEMPLATE_PATH + "pedia.html" + _FILE_CACHE_TIME ,
                controller: "PediaController"
            })
            .otherwise({
                redirectTo: "404"
            });

    }

}(window.allot));
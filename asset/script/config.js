(function () {

    "use strict";

    (function () {

        window.allotConfig= {
                VERSION: "1061215",
                NGAPP_NAME: "NGApp",
                RELEASE_HOST_PATH: "//showhi.co/",
                RELEASE_API_PATH: "//api.showhi.co/",
                RELEASE_PUBLIC_PATH: "//api.showhi.co/",
                STAGING_HOST_PATH: "//test.beta.showhi.co/",
                STAGING_API_PATH: "//api.w.test.beta.showhi.co/",
                STAGING_PUBLIC_PATH: "//api.w.test.beta.showhi.co/",
                DEVELOP_HOST_PATH: "//dev.beta.showhi.co/",
                DEVELOP_API_PATH: "//dev.api.beta.showhi.co/",
                DEVELOP_PUBLIC_PATH: "//dev.api.beta.showhi.co/",
                COOKIE_NAME: "ali-hotdog-salt"
            };

    }());

}());
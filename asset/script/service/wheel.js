(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __this;
    var _top;
    var _center;
    var _bottom;
    var _whenNumber;
    var _eventArray;
    var _whenList;

    (function () {

        __ngApp.service("$wheel", _wheelService());

    }());

    function _wheelService() {

        return Service;

        function Service() {
            __this = this;
            _defineVariable();
            _defineFunction();
        }

    }

    function _defineVariable() {

        _top = 0;
        _whenNumber = 0;
        _eventArray = [];
        _whenList = [];
        Object.defineProperty(__this, "top", {
                get: _topGetter
            });
        Object.defineProperty(__this, "center", {
                get: _centerGetter
            });
        Object.defineProperty(__this, "bottom", {
                get: _bottomGetter
            });

    }

    function _defineFunction() {

        __this.addEvent = _addEvent;
        __this.when = _when;
        __this.nowhen = _nowhen;
        __this.reset = _reset;
        document.addEventListener("wheel", _onWheel);

    }

    function _topGetter() {

        return _top;

    }

    function _centerGetter() {

        return _center;

    }

    function _bottomGetter() {

        return _bottom;

    }

    function _addEvent(_function) {

        _eventArray.unshift(_function);

    }

    function _when(_topInteger, _function) {

        _whenList.unshift({
                id: _whenNumber,
                top: _topInteger,
                callback: _function
            });
        return _whenNumber++;

    }

    function _nowhen(_id) {

        var _index = _whenList.length;
        var _item;
        while ( _index-- > 0 ) {
            _item = _whenList[_index];
            if ( _item.id === _id ) return _whenList.splice(_index, 1);
        }

    }

    function _reset() {

        _whenNumber = 0;
        _eventArray = [];
        _whenList = [];

    }

    function _onWheel() {

        var _windowHeight = window.innerHeight;
        _top = window.scrollY;
        _center = _top + _windowHeight * 0.5 ;
        _bottom = _top + _windowHeight ;
        _processEvents();
        _judgeWhenList();

    }

    function _processEvents() {

        var _index = _eventArray.length;
        var _function;
        while ( _index-- > 0 ) {
            _function = _eventArray[_index];
            if ( typeof(_function) === "function" ) _function();
        }

    }

    function _judgeWhenList() {

        var _index = _whenList.length;
        var _item;
        while ( _index-- > 0 ) {
            _item = _whenList[_index];
            if ( _top < _item.top ) continue;
            if ( typeof(_item.callback) === "function" ) _item.callback();
        }

    }

}(window.allot));
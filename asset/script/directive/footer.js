(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");

    (function () {

        _footerDirective.$inject = ["$TEMPLATE_PATH", "$FILE_CACHE_TIME"];
        __ngApp.directive("footer", _footerDirective);

    }());

    function _footerDirective(_TEMPLATE_PATH, _FILE_CACHE_TIME) {

        return {
                scope: true,
                restrict: "E",
                templateUrl: _TEMPLATE_PATH + "footer.html" + _FILE_CACHE_TIME ,
                controller: _footerController
            };

    }

    function _footerController() {



    }

}(window.allot));
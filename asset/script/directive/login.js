(function (__allot) {

    "use strict";

    var _ngApp = __allot("ng");
    var __dialogue;
    var __account;
    var __scope;

    (function () {

        _loginDirective.$inject = ["$dialogue", "$account"];
        _ngApp.directive("login", _loginDirective);

    }());

    function _loginDirective(_dialogue, _account) {

        __dialogue = _dialogue;
        __account = _account;
        return {
                restrict: "C",
                link: _loginController
            };

    }

    function _loginController(_scope) {

        __scope = _scope;
        _scope.onFormMousedown = _onFormMousedown;
        _scope.onForwardClick = _onForwardClick;
        _scope.onEixtClick = _onExitClick;

        function _onFormMousedown() {
            _scope.isLoginForwardError = false;
        }

        function _onForwardClick() {
            _presentLogin();
        }

        function _onExitClick() {
            __dialogue.close();
        }

    }

    function _presentLogin() {

        var _account = __scope.account;
        var _password = __scope.password;
        __account.login(_account, _password, _onSuccess, _onError, _onFinish);

        function _onSuccess() {
            __scope.account = "";
            __dialogue.close();
        }

        function _onError() {
            __scope.isLoginForwardError = true;
        }

        function _onFinish() {
            __scope.password = "";
            __scope.$apply();
        }

    }

}(window.allot));
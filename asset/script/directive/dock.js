(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __api;
    var __account;
    var __dialogue;
    var __friend;
    var _MODE_USER;
    var _MODE_GROUP;

    (function () {

        _dockDirective.$inject = ["$TEMPLATE_PATH", "$FILE_CACHE_TIME"];
        _dockController.$inject = ["$scope", "$api", "$account", "$dialogue", "$friend"];
        __ngApp.directive("dock", _dockDirective);

    }());

    function _dockDirective(_TEMPLATE_PATH, _FILE_CACHE_TIME) {

        return {
                scope: true,
                restrict: "C",
                templateUrl: _TEMPLATE_PATH + "dock.html" + _FILE_CACHE_TIME ,
                controller: _dockController
            };

    }

    function _dockController(_scope, _api, _account, _dialogue, _friend) {

        __scope = _scope;
        __api = _api;
        __account = _account;
        __dialogue = _dialogue;
        __friend = _friend;
        _defineVariable();
        _defineFunction();
        _readAccount();

    }

    function _defineVariable() {

        _MODE_USER = __scope.MODE_USER = ( __scope.modeName === "User" );
        _MODE_GROUP = __scope.MODE_GROUP = ( __scope.modeName === "Group" );

    }

    function _defineFunction() {

        __scope.getName = _getName;
        __scope.getAvatarImage = _getImageStyle("avatar");
        __scope.getCoverImage = _getImageStyle("cover");
        __scope.getUserLink = _getLink("user");
        __scope.getGroupLink = _getLink("group");
        __scope.getGroupinviteLink = _getLink("groupinvite");
        __scope.getGroupeditLink = _getLink("groupedit");
        __scope.confirmFriend = _confirmFriend;
        __scope.confirmGroupPrivacy = _confirmGroupPrivacy;
        __scope.onAvatarChangeClick = _onAvatarChangeClick;
        __scope.onCoverChangeClick = _onCoverChangeClick;
        __scope.onFriendAddClick = _onFriendAddClick;
        __scope.onFriendRemoveClick = _onFriendRemoveClick;
        __scope.onJoinClick = _onJoinClick;

    }

    function _readAccount() {

        __account.read(_onRead);

        function _onRead(_response) {
            __scope.accountDatum = _response.user || {} ;
        }

    }

    function _getName() {

        if ( _MODE_USER ) return ( __scope.userDatum.firstName || "" ) + " " + ( __scope.userDatum.lastName || "" ) ;
        if ( _MODE_GROUP ) return __scope.groupDatum.title;

    }

    function _getImageStyle(_type) {

        var _TYPE_AVATAR = ( _type === "avatar" );
        var _TYPE_COVER = ( _type === "cover" );
        if ( !( _TYPE_AVATAR || _TYPE_COVER ) ) return;
        return _process;

        function _process() {
            var _datum = _getDatum();
            var _url = _getUrl(_datum);
            if ( !(_url) ) return;
            if ( !/\.(jpg|jpeg|png|gif)$/.test(_url) ) _url += "_o.png";
            return {
                    "background-image": "url(\"" + _url + "\")"
                };
        }

        function _getDatum() {
            if ( _MODE_USER ) return __scope.userDatum;
            if ( _MODE_GROUP ) return __scope.groupDatum;
            return {};
        }

        function _getUrl(_datum) {
            if ( _TYPE_AVATAR ) return _datum.avatarUrl;
            if ( _TYPE_COVER ) return _datum.coverUrl;
        }

    }

    function _getLink(_type) {

        var _TYPE_USER = ( _type === "user" );
        var _TYPE_GROUP = ( _type === "group" );
        var _TYPE_GROUPINVITE = ( _type === "groupinvite" );
        var _TYPE_GROUPEDIT = ( _type === "groupedit" );
        if ( _MODE_GROUP ) {
            if ( !( _TYPE_GROUP || _TYPE_GROUPEDIT || _TYPE_GROUPINVITE ) ) return;
            return _processGroup;
        } else if ( _MODE_USER ) {
            if ( !(_TYPE_USER) ) return;
            return _processUser;
        }

        function _processGroup() {
            var _id = __scope.groupDatum.id;
            if ( !(_id) ) return;
            if ( _TYPE_GROUP ) return "/group/" + _id ;
            if ( _TYPE_GROUPEDIT ) return "/groupedit/" + _id ;
            if ( _TYPE_GROUPINVITE ) return "/groupinvite/" + _id ;
        }

        function _processUser() {
            var _id = __scope.userDatum.id;
            if ( !(_id) ) return;
            if ( _id === ( __scope.accountDatum || {} ).id ) return "/";
            if ( _TYPE_USER ) return "/user/" + _id ;
        }

    }

    function _confirmFriend() {

        if ( !(__scope.accountDatum) ) return false;
        if ( __friend.match(__scope.accountDatum.id, __scope.userDatum.id) ) return true;

    }

    function _confirmGroupPrivacy(_privacyType) {

        var _currentMember = __scope.groupDatum.currentUser;
        if ( _currentMember ) return _processConfirm();

        function _processConfirm() {
            var _ROLE_OWNER = ( _currentMember.role === "owner" );
            var _ROLE_ADMIN = ( _currentMember.role === "admin" );
            var _ROLE_RECRUITER = ( _currentMember.role === "recruiter" );
            var _ROLE_EDITOR = ( _currentMember.role === "editor" );
            var _TYPE_INVITE = ( _privacyType === "invite" );
            var _TYPE_ACCESS = ( _privacyType === "access" );
            var _TYPE_MODIFY = ( _privacyType === "modify" );
            if ( _TYPE_INVITE ) if ( _ROLE_OWNER || _ROLE_ADMIN || _ROLE_RECRUITER ) return true;
            if ( _TYPE_ACCESS ) if ( _ROLE_OWNER || _ROLE_ADMIN || _ROLE_RECRUITER ) return true;
            if ( _TYPE_MODIFY ) if ( _ROLE_OWNER || _ROLE_ADMIN ) return true;
            if ( _ROLE_EDITOR ) return false;
        }

    }

    function _onAvatarChangeClick() {

        __dialogue.cropper("avatar", _onCropSuccess);

        function _onCropSuccess(_blob) {
            if ( _MODE_USER ) __api.userAvatar(__scope.userDatum.id, _blob)
                .success(_onUploadSuccess);
            if ( _MODE_GROUP ) __api.groupsAvatar(__scope.groupDatum.id, _blob)
                .success(_onUploadSuccess);
        }

        function _onUploadSuccess() {
            _readDatum();
            __dialogue.close();
        }

    }

    function _onCoverChangeClick() {

        __dialogue.cropper("cover", _onCropSuccess);

        function _onCropSuccess(_blob) {
            if ( _MODE_USER ) __api.blogCover(__scope.userDatum.blogId, _blob)
                .success(_onUploadSuccess);
            if ( _MODE_GROUP ) __api.groupsCover(__scope.groupDatum.id, _blob)
                .success(_onUploadSuccess);
        }

        function _onUploadSuccess() {
            _readDatum();
            __dialogue.close();
        }

    }

    function _onFriendAddClick() {

        __api.friendCreate(__scope.userDatum.id)
            .success(_onSuccess);

        function _onSuccess() {
            _readDatum();
            __friend.readList(__scope.accountDatum.id);
        }

    }

    function _onFriendRemoveClick() {

        __dialogue.confirm(__scope.lyric.DOCK_FRIEND_UNFRIEND_BUTTON, __scope.lyric.DOCK_FRIEND_UNFRIEND_ASKING, _onConfirm);

        function _onConfirm() {
            var _userFriendDatum = __friend.find(__scope.userDatum.id) || {} ;
            var _userFriendId = _userFriendDatum.id;
            __api.friendDelete(_userFriendId)
                .success(_onSuccess);
        }

        function _onSuccess() {
            _readDatum();
            __friend.readList(__scope.accountDatum.id);
        }

    }

    function _onJoinClick() {

        __api.groupUsersJoin(__scope.groupDatum.id)
            .success(_onSuccess);

        function _onSuccess() {
            if ( __scope.readGroup ) __scope.readGroup();
        }

    }

    function _readDatum() {

        if ( _MODE_USER ) if ( __scope.readAccount ) __scope.readAccount();
        if ( _MODE_GROUP ) if ( __scope.readGroup ) __scope.readGroup();

    }

}(window.allot));
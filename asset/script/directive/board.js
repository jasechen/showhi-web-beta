(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __path;
    var __api;
    var __wheel;
    var __dialogue;
    var _MODE_USER;
    var _MODE_GROUP;

    (function () {

        _boardDirective.$inject = ["$path", "$api", "$wheel", "$dialogue", "$TEMPLATE_PATH", "$FILE_CACHE_TIME"];
        __ngApp.directive("board", _boardDirective);

    }());

    function _boardDirective(_path, _api, _wheel, _dialogue, _TEMPLATE_PATH, _FILE_CACHE_TIME) {

        __path = _path;
        __api = _api;
        __wheel = _wheel;
        __dialogue = _dialogue;
        return {
                scope: false,
                restrict: "C",
                templateUrl: _TEMPLATE_PATH + "board.html" + _FILE_CACHE_TIME ,
                link: _boardLink
            };

    }

    function _boardLink(_scope, _element) {

        arguments[1] = _element[0];
        _defineVariable.apply(null, arguments);
        _defineFunction.apply(null, arguments);

    }

    function _defineVariable(_scope, _element) {

        _MODE_USER = _scope.MODE_USER = ( _scope.modeName === "User" );
        _MODE_GROUP = _scope.MODE_GROUP = ( _scope.modeName === "Group" );
        _scope.datum = _scope.item;
        _scope.datum.top = _element.offsetTop;

    }

    function _defineFunction(_scope) {

        _scope.getAvatarStyle = _getAvatarStyle.apply(null, arguments);
        _scope.getName = _getName.apply(null, arguments);
        _scope.getBoardCreatorUrl = _getBoardCreatorUrl.apply(null, arguments);
        _scope.getBoardPhotoUrl = _getBoardPhotoUrl;
        _scope.getPrivacyClass = _getPrivacyClass.apply(null, arguments);
        _scope.setVideoFile = _setVideoFile.apply(null, arguments);
        _scope.onExtraToggle = _onExtraToggle.apply(null, arguments);
        _scope.onModifyClick = _onModifyClick.apply(null, arguments);
        _scope.onDeleteClick = _onDeleteClick.apply(null, arguments);
        _scope.onSetPublicClick = _onSetPublicClick.apply(null, arguments);
        _scope.onSetPrivateClick = _onSetPrivateClick.apply(null, arguments);
        _scope.onSelectVideoClick = _onSelectVideoClick.apply(null, arguments);
        _scope.onPhotoClick = _onPhotoClick;
        _scope.onAttendPhotoClick = _onAttendPhotoClick.apply(null, arguments);
        _scope.onRemovePhotoClick = _onRemovePhotoClick.apply(null, arguments);
        _scope.onUpdateClick = _onUpdateClick.apply(null, arguments);
        _scope.onReplyClick = _onReplyClick.apply(null, arguments);
        _scope.onCommentsClick = _onCommentsClick.apply(null, arguments);
        __wheel.addEvent(_onWheel.apply(null, arguments));

    }

    function _getAvatarStyle(_scope) {

        var _datum = _scope.datum;
        return _process;

        function _process(_item) {
            var _target = _item || _datum ;
            var _url = "";
            var _defaultAvatarUrl = "/asset/image/dock-avatar-default.png";
            _target.creater = _target.creater || {} ;
            if ( _MODE_GROUP ) _url = _scope.groupDatum.avatarUrl || _defaultAvatarUrl ;
            else {
                if ( _target.creater.avatar ) {
                    if ( _target.creater.avatar_links ) _url = __path.PUBLIC + _target.creater.avatar_links[0] ;
                    else _url = __path.PUBLIC + "/gallery/blog_" + _target.parent_id + "/avatar/" + _target.creater.avatar ;
                } else _url = _defaultAvatarUrl;
            }
            if ( !/\.(jpg|jpeg|png|gif)$/.test(_url) ) _url += "_sq.png";
            return {
                    "background-image": "url(\"" + _url + "\")"
                };
        }

    }

    function _getName(_scope) {

        var _datum = _scope.datum;
        return _process;

        function _process(_item) {
            var _target = _item || _datum ;
            _target.creater = _target.creater || {} ;
            if ( _MODE_GROUP ) return _scope.groupDatum.title;
            return _target.creater.first_name + " " + _target.creater.last_name ;
        }

    }

    function _getBoardCreatorUrl(_scope) {

        return _process;

        function _process() {
            if ( _MODE_USER ) return "/user/" + _scope.datum.creater_id ;
            return "/group/" + _scope.datum.parent_id ;
        }

    }

    function _getBoardPhotoUrl(_photoItem) {

        var _url = ( _photoItem.links || [] )[0];
        _url = _url.replace(/\_q\.png$/, "_o.png").replace(/\_q\.jpg$/, "_o.jpg").replace(/\_q\.jpeg$/, "_o.jpeg");
        return __path.PUBLIC + _url ;

    }

    function _getPrivacyClass(_scope) {

        return _process;

        function _process() {
            var _className = "board-privacy-" + _scope.datum.privacy_status;
            var _obj = {};
            _obj[_className] = true;
            return _obj;
        }

    }

    function _setVideoFile(_scope, _element) {

        var _datum = _scope.datum;
        return _process;

        function _process() {
            if ( !(_datum.files) || !(_datum.files.length) ) return;
            if ( !( /video/.test(_datum.files[0].mime_type) ) ) return;
            var _videoBlockElement = _element.querySelector(".board-video");
            var _file = _datum.videoFile = _datum.files.shift();
            if ( !(_videoBlockElement) ) return;
            var _iframe = document.createElement("iframe");
            _iframe.src = _file.links[0];
            _videoBlockElement.insertBefore(_iframe, _videoBlockElement.firstChildren);
            _scope.isRead = true;
            __api.fileViewCount(_file.id);
        }

    }

    function _onExtraToggle(_scope) {

        return _process;

        function _process() {
            _scope.datum.isExtra = !(_scope.datum.isExtra);
        }

    }

    function _onModifyClick(_scope) {

        return _process;

        function _process() {
            _scope.datum.isModify = true;
            delete _scope.datum.isExtra;
            _scope.handleExtraId = 0;
        }

    }

    function _onDeleteClick(_scope) {

        return _process;

        function _process() {
            __dialogue.confirm(_scope.lyric.BOARD_EXTRA_DELETE_BUTTON, _scope.lyric.BOARD_EXTRA_DELETE_ASKING, _onConfirm);
        }

        function _onConfirm() {
            _deleteBoard(_scope);
        }

    }

    function _onSetPublicClick(_scope) {

        return _process;

        function _process() {
            __dialogue.confirm(_scope.lyric.BOARD_PUBLIC_BUTTON, _scope.lyric.BOARD_PUBLIC_ASKING, _onConfirm);
            delete _scope.datum.isExtra;
        }

        function _onConfirm() {
            _scope.datum.privacy_status = "public";
            _updateBoard(_scope);
        }

    }

    function _onSetPrivateClick(_scope) {

        return _process;

        function _process() {
            __dialogue.confirm(_scope.lyric.BOARD_PRIVATE_BUTTON, _scope.lyric.BOARD_PRIVATE_ASKING, _onConfirm);
            delete _scope.datum.isExtra;
        }

        function _onConfirm() {
            _scope.datum.privacy_status = "private";
            _updateBoard(_scope);
        }

    }

    function _onSelectVideoClick(_scope, _element) {

        var _videoInput;
        var _file;
        return _process;

        function _process() {
            _videoInput = document.createElement("input");
            _videoInput.type = "file";
            _videoInput.accept = "video/mp4";
            _videoInput.onchange = _onChange;
            _videoInput.click();
        }

        function _onChange() {
            _file = _videoInput.files[0];
            delete _scope.videoLocalUrl;
            _scope.videoFile = _scope.videoFile || {} ;
            _scope.isVideoWorking = true;
            _scope.$apply();
            _createFile(_scope, _videoInput, _onSuccess, null, _onMislay, _onFinish);
        }

        function _onSuccess(_response) {
            var URL = window.URL || window.webkitURL ;
            _videoInput.value = null;
            _scope.videoFile = {
                    id: _response.file_id,
                    link: _response.file_links[0]
                };
            _scope.videoLocalUrl = URL.createObjectURL(_file);
            var _iframe = _element.querySelector("iframe");
            var _parentElement = _iframe.parentNode;
            _parentElement.removeChild(_iframe);
            var _videoElement = document.createElement("video");
            _videoElement.src = _scope.videoLocalUrl;
            _videoElement.autoplay = true;
            _videoElement.controls = true;
            _parentElement.appendChild(_videoElement);
        }

        function _onMislay() {
            delete _scope.isVideoWorking;
        }

        function _onFinish() {
            delete _scope.isVideoWorking;
            _scope.$apply();
        }

    }

    function _onPhotoClick(_photoItem) {

        var _imageUrl = _getBoardPhotoUrl(_photoItem);
        __dialogue.lightbox(_imageUrl, null, _onSuccess);

        function _onSuccess() {
            __api.fileViewCount(_photoItem.id);
        }

    }

    function _onAttendPhotoClick(_scope) {

        var _fileInput;
        return _process;

        function _process() {
            _fileInput = _getFileInput();
            _fileInput.click();
        }

        function _getFileInput() {
            if ( !(_fileInput) ) {
                _fileInput = document.createElement("input");
                _fileInput.type = "file";
                _fileInput.accept = "image/x-png,image/jpeg";
                _fileInput.onchange = _onChange;
            }
            return _fileInput;
        }

        function _onChange() {
            _scope.datum.isPhotoWorking = true;
            _scope.$apply();
            _createFile(_scope, _fileInput, _onSuccess, null, _onMislay, _onFinish);
        }

        function _onSuccess(_response) {
            _scope.datum.files = _scope.datum.files || [] ;
            _scope.datum.files.unshift({
                    id: _response.file_id,
                    links: _response.file_links
                });
        }

        function _onMislay() {
            delete _scope.datum.isPhotoWorking;
        }

        function _onFinish() {
            delete _scope.datum.isPhotoWorking;
            _scope.$apply();
        }

    }

    function _onRemovePhotoClick(_scope) {

        var _fileItem;
        return _process;

        function _process(_item) {
            if ( !(_item) ) return;
            _fileItem = _item;
            __dialogue.confirm(_scope.lyric.BOARD_REMOVE_PHOTO_BUTTON, _scope.lyric.BOARD_REMOVE_PHOTO_ASKING, _onConfirm);
        }

        function _onConfirm() {
            var _fileList = _scope.datum.files;
            var _index = _fileList.length;
            var _itemInList;
            while ( _index-- > 0 ) {
                _itemInList = _fileList[_index];
                if ( _itemInList.id !== _fileItem.id ) continue;
                _fileList.splice(_index, 1);
            }
        }

    }

    function _onUpdateClick(_scope) {

        return _process;

        function _process() {
            _updateBoard(_scope);
        }

    }

    function _onReplyClick(_scope) {

        return _process;

        function _process() {
            _createComment(_scope);
        }

    }

    function _onCommentsClick(_scope) {

        return _process;

        function _process() {
            _fetchComments(_scope);
        }

    }

    function _onWheel(_scope, _element) {

        return _process;

        function _process() {
            if ( _scope.datum.isRead ) return;
            if ( !(_element.offsetTop) ) return;
            if ( __wheel.bottom < _element.offsetTop ) return;
            _scope.datum.isRead = true;
            if ( _scope.datum.fetchMore ) _scope.datum.fetchMore(true);
            if ( _MODE_GROUP ) _sendAccess(_scope);
            _scope.$apply();
        }

    }

    function _fetchComments(_scope) {

        __api.commentListByText(_scope.datum.text_id)
            .success(_onSuccess)
            .error(_onError)
            .finish(_onFinish);

        function _onSuccess(_response) {
            _scope.datum.comments = _response.comments;
        }

        function _onError() {
            _scope.datum.comments = [];
        }

        function _onFinish() {
            _scope.$apply();
        }

    }

    function _createComment(_scope) {

        __api.commentCreate(
                _scope.datum.replyText,
                _scope.datum.text_id,
                "text"
            )
            .success(_onSuccess);

        function _onSuccess() {
            delete _scope.datum.replyText;
            _fetchComments(_scope);
        }

    }

    function _updateBoard(_scope) {

        __api[ _MODE_USER ? "boardUpdate" : "boardGroupUpdate" ](
                _scope.datum.id,
                _scope.datum.text,
                _scope.datum.text_id,
                _scope.datum.privacy_status,
                null,
                _scope.videoFile || _getFileIdArray(),
                _MODE_USER ? null : _scope.groupDatum.id
            )
            .success(_onSuccess);

        function _onSuccess() {
            delete _scope.datum.isModify;
            _scope.$apply();
        }

        function _getFileIdArray() {
            var _files = _scope.datum.files || [] ;
            var _index = _files.length;
            var _array = [];
            while ( _index-- > 0 ) _array.unshift(_files[_index].id);
            return _array;
        }

    }

    function _deleteBoard(_scope) {

        __api[ _MODE_USER ? "boardDelete" : "boardGroupDelete" ](
                _scope.datum.id,
                _MODE_USER ? null : _scope.groupDatum.id
            )
            .success(_onSuccess);

        function _onSuccess() {
            _scope.datum.isDelete = true;
            _scope.$apply();
        }

    }

    function _createFile(_scope, _fileInput, _callback, _errorback, _mislayback, _finishback) {

        var _file = _fileInput.files[0];
        __api.fileCreate(
                _file,
                _scope.datum.parent_id,
                _scope.datum.parent_type
            )
            .success(_onSuccess)
            .mislay(_onMislay)
            .finish(_onFinish);

        function _onSuccess(_response) {
            if ( _callback ) _callback(_response);
        }

        function _onMislay() {
            if ( _mislayback ) _mislayback();
        }

        function _onFinish() {
            if ( _finishback ) _finishback();
        }

    }

    function _sendAccess(_scope) {

        __api.boardViewCount(_scope.datum.id);

    }

}(window.allot));
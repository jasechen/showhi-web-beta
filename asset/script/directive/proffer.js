(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __path;
    var __api;
    var __dialogue;
    var __documentPress;
    var _MODE_USER;
    var _MODE_GROUP;

    (function () {

        _profferController.$inject = ["$scope", "$path", "$api", "$dialogue", "$documentPress"];
        __ngApp.directive("proffer", _profferDirective);

    }());

    function _profferDirective() {

        return {
                scope: true,
                restrict: "C",
                controller: _profferController
            };

    }

    function _profferController(_scope, _path, _api, _dialogue, _documentPress) {

        __scope = _scope;
        __path = _path;
        __api = _api;
        __dialogue = _dialogue;
        __documentPress = _documentPress;
        _defineProcessVariables();
        _defineProcessEvents();

    }

    function _defineProcessVariables() {

        _MODE_USER = ( __scope.modeName === "User" );
        _MODE_GROUP = ( __scope.modeName === "Group" );
        __scope.creatorType = "member";

    }

    function _defineProcessEvents() {

        __scope.getTypeSelectClass = _getTypeSelectClass;
        __scope.getTypeSelectName = _getTypeSelectName;
        __scope.getArrangedQrcodeList = _getArrangedQrcodeList;
        __scope.getQRcodeUrl = _getQRcodeUrl;
        __scope.onCreatorSelectClick = _onCreatorSelectClick;
        __scope.onCreatorTargetClick = _onCreatorTargetClick;
        __scope.onQRCodeCreateClick = _onQRCodeCreateClick;
        __scope.onQRCodeNodeUpdateClick = _onQRCodeNodeUpdateClick;
        __scope.onQRCodeNodeDeleteClick = _onQRCodeNodeDeleteClick;
        __scope.onQRCodeNodeModifyClick = _onQRCodeNodeModifyClick;
        __scope.onQRCodeUserVerify = _onQRCodeUserVerify;

    }

    function _getTypeSelectClass() {

        var _type = __scope.creatorType || "member" ;
        var _className = "proffer-creator-target-" + _type ;
        var _responseObject = {};
        _responseObject[_className] = true;
        return _responseObject;

    }

    function _getTypeSelectName() {

        if ( __scope.creatorType === "school" ) return __scope.lyric.INVITE_TARGET_SCHOOL_DESCRIPTION;
        else if ( __scope.creatorType === "business" ) return __scope.lyric.INVITE_TARGET_BUSINESS_DESCRIPTION;
        else return __scope.lyric.INVITE_TARGET_STUDENT_DESCRIPTION;

    }

    function _getQRcodeUrl(_imageName) {

        var _imagePath = "/" + _imageName.replace(/\.(png)$/, "") + "_n.png" ;
        if ( _MODE_USER ) return __path.PUBLIC + "qrcode/blog_" + __scope.userDatum.blogId + _imagePath ;
        if ( _MODE_GROUP ) return __path.PUBLIC + "qrcode/group_" + __scope.groupDatum.id + _imagePath ;

    }

    function _onCreatorSelectClick(_event) {

        __scope.creatorIsTargetSelect = !(__scope.creatorIsTargetSelect);
        if ( __scope.creatorIsTargetSelect ) __documentPress.add(_onHandler, _event.target);
        __documentPress.ignore(document.querySelector(".proffer-creator-target-select"));

        function _onHandler() {
            __documentPress.reset();
            __scope.creatorIsTargetSelect = false;
            __scope.$apply();
        }

    }

    function _onCreatorTargetClick(_targetName) {

        if ( _targetName === "school" ) __scope.creatorType = "school";
        else if ( _targetName === "business" ) __scope.creatorType = "business";
        else __scope.creatorType = "member";
        __scope.creatorIsTargetSelect = false;
        __documentPress.reset();

    }

    function _getArrangedQrcodeList(_originalList) {

        var _index = ( _originalList || [] ).length;
        var _tempItem;
        while ( _index-- > 0 ) {
            _tempItem = _originalList[_index];
            _tempItem.expireDate = _getExpireDate(_tempItem.created_at, _tempItem.expire_time);
            _tempItem.isTypeSchool = _tempItem.type === "school" ;
            _tempItem.isTypeBusiness = _tempItem.type === "business" ;
        }
        return _originalList;

        function _getExpireDate(_createDate, _expireTime) {
            var _date = new Date(_createDate);
            _date.setSeconds( _date.getSeconds() + _expireTime );
            var _stringDate = _date.getFullYear() + "-" + ( _date.getMonth() + 1 ) + "-" + _date.getDate() ;
            var _stringTime = _date.getHours() + ":" + _date.getMinutes() + ":" + _date.getSeconds() ;
            return _stringDate + " " + _stringTime ;
        }

    }

    function _onQRCodeCreateClick() {

        if ( !__scope.creatorNode ) return;
        __api.qrcodesCreate(
                __scope.creatorType,
                _MODE_GROUP ? __scope.groupDatum.id : null ,
                _MODE_GROUP ? "group" : null
            )
            .success(_onCreateSuccess);

        function _onCreateSuccess(_response) {
            _updateQrcodeNote(_response, __scope.creatorNode, _onNoteSuccess);
        }

        function _onNoteSuccess() {
            __scope.creatorNode = "";
            _readQrcodeList();
        }

    }

    function _onQRCodeNodeUpdateClick(_item) {

        _updateQrcodeNote(_item, null, _onSuccess);

        function _onSuccess() {
            _readQrcodeList();
        }

    }

    function _onQRCodeNodeDeleteClick(_item) {

        __dialogue.confirm(__scope.lyric.INVITE_QRCODE_DELETE_BUTTON, __scope.lyric.INVITE_QRCODE_DELETE_ASKING, _onConfirm);

        function _onConfirm() {
            __api.qrcodesDelete(_item.id)
                .success(_onSuccess);
        }

        function _onSuccess() {
            _readQrcodeList();
        }

    }

    function _onQRCodeNodeModifyClick(_item) {

        if ( _item.isModify ) _item.note = _item.originNote;
        else _item.originNote = _item.note;
        _item.isModify = !_item.isModify;

    }

    function _onQRCodeUserVerify(_requestDatum) {

        __api.userQrcodeVerify(__scope.userDatum.id, _requestDatum.user_id)
            .success(_onSuccess);

        function _onSuccess() {
            _readQrcodeList();
        }

    }

    function _readQrcodeList() {

        if ( __scope.readQrcodeList ) __scope.readQrcodeList();

    }

    function _updateQrcodeNote(_item, _note, _callback, _errorback) {

        _note = _note || _item.note ;
        __api.qrcodesUpdatenote(_item.id, _note)
            .success(_onSuccess)
            .error(_onError)
            .mislay(_onError);

        function _onSuccess(_response) {
            if ( _callback ) _callback(_response);
        }

        function _onError() {
            if ( _errorback ) _errorback();
        }

    }

}(window.allot));
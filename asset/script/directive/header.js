(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __api;
    var __dialogue;
    var __account;
    var __user;
    var __documentPress;
    var _isSettingSelect;

    (function () {

        _headerDirective.$inject = ["$TEMPLATE_PATH", "$FILE_CACHE_TIME"];
        _headerController.$inject = ["$scope", "$api", "$dialogue", "$account", "$user", "$documentPress"];
        __ngApp.directive("header", _headerDirective);

    }());

    function _headerDirective(_TEMPLATE_PATH, _FILE_CACHE_TIME) {

        return {
                restrict: "E",
                templateUrl: _TEMPLATE_PATH + "header.html" + _FILE_CACHE_TIME ,
                controller: _headerController
            };

    }

    function _headerController(_scope, _api, _dialogue, _account, _user, _documentPress) {

        __scope = _scope;
        __api = _api;
        __dialogue = _dialogue;
        __account = _account;
        __user = _user;
        __documentPress = _documentPress;
        _defineVariable();
        _defineFunction();
        _readAccount(_onUserRead);

    }

    function _defineVariable() {

        __scope.isSettingSelect = _isSettingSelect = false;

    }

    function _defineFunction() {

        __scope.onSearchClick = _onSearchClick;
        __scope.onHeaderLoginClick = _onHeaderLoginClick;
        __scope.onHeaderLogoutClick = _onHeaderLogoutClick;
        __scope.onSettingHandlerPress = _onSettingHandlerPress;
        __scope.getDashboardLink = _getDashboardLink;
        __scope.getImageStyle = _getImageStyle;
        __scope.getLangpickerLink = _getLangpickerLink;
        __scope.confirmCreateGroupPrivacy = _confirmCreateGroupPrivacy;

    }

    function _readAccount(_callback) {

        __account.addCallback(_onRead);
        __account.read();

        function _onRead(_response) {
            var _responseDatum = _response.user || {} ;
            __scope.userDatum = __user.arrange(_responseDatum);
            if ( _callback ) _callback(__scope.userDatum);
        }

    }

    function _onUserRead(_responseDatum) {

        __scope.userDatum = _responseDatum;
        __scope.$apply();

    }

    function _onSearchClick(_event) {

        __documentPress.add(_onHandler, _event.target);
        __api.search(__scope.searchText)
            .success(_onSuccess)
            .finish(_finish);

        function _onSuccess(_response) {
            var _result = _arrageUsers( _response.users || [] );
            _result = _result.concat(_arrageGroups( _response.groups || [] ));
            __scope.searchResult = _result;
        }

        function _finish() {
            __scope.$apply();
        }

        function _arrageUsers(_responseList) {
            var _list = [];
            var _item;
            while ( _responseList.length ) {
                _item = _responseList.shift();
                _list.push({
                        id: _item.id,
                        type: "user",
                        name: ( _item.profile || {} ).first_name + " " + ( _item.profile || {} ).last_name
                    });
            }
            return _list;
        }

        function _arrageGroups(_responseList) {
            var _list = [];
            var _item;
            while ( _responseList.length ) {
                _item = _responseList.shift();
                _list.push({
                        id: _item.id,
                        type: "group",
                        name: _item.title
                    });
            }
            return _list;
        }

        function _onHandler() {
            __scope.searchResult = [];
            __documentPress.reset();
            __scope.$apply();
        }

    }

    function _onHeaderLoginClick() {

        __dialogue.login();

    }

    function _onHeaderLogoutClick() {

        __dialogue.confirm(__scope.lyric.HEADER_LOGOUT_BUTTON, __scope.lyric.HEADER_LOGOUT_ASKING, _onConfirm);

        function _onConfirm() {
            __account.logout();
        }

    }

    function _onSettingHandlerPress(_event) {

        _toggleSelect();
        if ( !_isSettingSelect ) __documentPress.reset();
        else __documentPress.add(_onHandler, _event.target);

        function _toggleSelect() {
            __scope.isSettingSelect = _isSettingSelect = !_isSettingSelect;
        }

        function _onHandler() {
            __documentPress.reset();
            _toggleSelect(false);
            __scope.$apply();
        }

    }

    function _getDashboardLink() {

        return __scope.isLogined ? "/" : "/home" ;

    }

    function _getImageStyle() {

        var _datum = __scope.userDatum || {} ;
        var _url = _datum.avatarUrl;
        if ( !(_url) ) return;
        if ( !/\.(jpg|jpeg|png|gif)$/.test(_url) ) _url += "_sq.png";
        return {
                "background-image": "url(\"" + _url + "\")"
            };

    }

    function _getLangpickerLink() {

        return __scope.isLogined ? "/langpicker" : "/langpicker-normal" ;

    }

    function _confirmCreateGroupPrivacy() {

        if ( __scope.userDatum ) {
            if ( __scope.userDatum.type === "school" ) return true;
            if ( __scope.userDatum.type === "business" ) return true;
        }

    }

}(window.allot));
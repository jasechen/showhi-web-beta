(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __location;
    var __routeParams;
    var __path;
    var __api;
    var __account;
    var __friend;
    var __dialogue;
    var _MODE_USER;
    var _MODE_GROUP;
    var _userId;
    var _groupId;

    (function () {

        _vitaeUserDirective.$inject = ["$TEMPLATE_PATH", "$FILE_CACHE_TIME"];
        _vitaeUserController.$inject = ["$scope", "$location", "$routeParams", "$path", "$api", "$account", "$friend", "$dialogue"];
        __ngApp.directive("vitae", _vitaeUserDirective);

    }());

    function _vitaeUserDirective(_TEMPLATE_PATH, _FILE_CACHE_TIME) {

        return {
                scope: true,
                restrict: "C",
                templateUrl: _TEMPLATE_PATH + "vitae.html" + _FILE_CACHE_TIME ,
                controller: _vitaeUserController
            };

    }

    function _vitaeUserController(_scope, _location, _routeParams, _path, _api, _account, _friend, _dialogue) {

        __scope = _scope;
        __location = _location;
        __routeParams = _routeParams;
        __path = _path;
        __api = _api;
        __account = _account;
        __friend = _friend;
        __dialogue = _dialogue;
        _defineVariable();
        _defineFunction();

    }

    function _defineVariable() {

        _MODE_USER = __scope.MODE_USER = ( __scope.modeName === "User" );
        _MODE_GROUP = __scope.MODE_GROUP = ( __scope.modeName === "Group" );
        if ( _MODE_USER ) {
            __scope.groupList = [];
            _userId  = __routeParams.userId;
        }
        if ( _MODE_GROUP ) _groupId = __routeParams.groupId || __routeParams.groupID ;

    }

    function _defineFunction() {

        __scope.getAvatarBackground = _getAvatarBackground;
        __scope.getFriendLink = _getFriendLink;
        __scope.getAppointLink = _getAppointLink;
        __scope.confirmPrivacy = _confirmPrivacy;
        __scope.onZoomClick = _onZoomClick;
        if ( _MODE_USER ) {
            if ( _userId ) _readFriendList();
            else __account.read(_onAccountRead);
        }

    }

    function _getAvatarBackground(_item) {

        if ( !(_item.avatarUrl) ) {
            if ( _item.avatar ) _item.avatarUrl = __path.PUBLIC + _item.avatar_links[0];
            else _item.avatarUrl = "/asset/image/dock-avatar-default.png";
        }
        return {
                "background-image": "url(" + _item.avatarUrl + ")"
            };

    }

    function _getFriendLink(_friendItem) {

        var _friendUserId = ( _userId === _friendItem.user_id ) ? _friendItem.friend_id : _friendItem.user_id ;
        return "/user/" + _friendUserId ;

    }

    function _getAppointLink(_memberItem) {

        if ( _memberItem === __scope.groupDatum.currentUser ) return "/appointmaker/" + _groupId ;
        return "/appoint/" + _groupId + "/" + _memberItem.userId ;

    }

    function _confirmPrivacy(_type) {

        var _TYPE_RECRUIT = ( _type === "recruit" );
        var _userRole = ( __scope.groupDatum.currentUser || {} ).role;
        if ( !(_userRole) ) return;
        if ( _userRole === "owner" ) if ( _TYPE_RECRUIT ) return true;
        if ( _userRole === "admin" ) if ( _TYPE_RECRUIT ) return true;
        if ( _userRole === "recruiter" ) if ( _TYPE_RECRUIT ) return true;

    }

    function _onZoomClick() {

        var _url;
        __api.meetingTemporary(_groupId, __scope.groupDatum.currentUser.user_id)
            .success(_onSuccess);
        __dialogue.countdown(__scope.lyric.ZOOM_CREATING_LETTER);

        function _onSuccess(_response) {
            _response = _response[0];
            _url = _response.start_url;
            __dialogue.confirm(__scope.lyric.ZOOM_TEMPORARY_BUTTON, __scope.lyric.ZOOM_TEMPORARY_ASKING, _onConfirm);
        }

        function _onConfirm() {
            window.open(_url, "_blank");
        }

    }

    function _readFriendList() {

        if ( !(_userId) ) return;
        __friend.readList(_userId)
            .then(_onFriednRead);

        function _onFriednRead(_responseList) {
            __scope.friends = _responseList;
            __scope.$apply();
        }

    }

    function _onAccountRead(_responseDatum) {

        _userId = ( _responseDatum.user || {} ).id;
        _readFriendList();

    }

}(window.allot));
(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __path;
    var __api;
    var __account;
    var __dialogue;
    var __friend;
    var _accountDatum;

    (function () {

        _notificationController.$inject = ["$scope", "$path", "$api", "$account", "$dialogue", "$friend"];
        _notificationDirective.$inject = ["$TEMPLATE_PATH", "$FILE_CACHE_TIME"];
        __ngApp.directive("notification", _notificationDirective);

    }());

    function _notificationDirective(_TEMPLATE_PATH, _FILE_CACHE_TIME) {

        return {
                scope: true,
                restrict: "C",
                templateUrl: _TEMPLATE_PATH + "notification.html" + _FILE_CACHE_TIME ,
                controller: _notificationController
            };

    }

    function _notificationController(_scope, _path, _api, _account, _dialogue, _friend) {

        __scope = _scope;
        __path = _path;
        __api = _api;
        __account = _account;
        __dialogue = _dialogue;
        __friend = _friend;
        _defineFunction();
        _readNoticication();
        _readFriend();

    }

    function _defineFunction() {

        __scope.onNotificationClick = _onNotificationClick;
        __scope.onAcceptMeetingClick = _onAcceptMeetingClick;
        __scope.onCancelMeetingClick = _onCancelMeetingClick;
        __scope.onConferenceClick = _onConferenceClick;

    }

    function _readNoticication() {

        _fetchNotification();

    }


    function _readFriend() {

        __account.read(_onAccountRead);

        function _onAccountRead(_responseDatum) {
            _accountDatum = _responseDatum.user;
            __friend.readList(_accountDatum.id)
                .then(_onRead);
        }

        function _onRead(_responseList) {
            var _list = _responseList;
            var _index = _list.length;
            var _notificationList = [];
            var _item;
            while ( _index-- > 0 ) {
                _item = _list[_index];
                if ( _item.user_id === _accountDatum.id ) continue;
                if ( _item.status !== "init" ) continue;
                _notificationList.unshift({
                        userId: _item.user_id,
                        content: _item.first_name + " " + _item.last_name,
                        created_at: _item.created_at,
                        userFriendId: _item.id,
                        type: "friend"
                    });
            }
            __scope.friendNotifications = _notificationList;
        }

    }

    function _onNotificationClick(_notificationItem) {

        if ( /group\sinvites\syou/.test(_notificationItem.content) ) __dialogue.confirm(_notificationItem.content, __scope.lyric.NOTIFICATION_GROUP_INVITE_ASKING, _onGroupConfirm);
        else if ( _notificationItem.type === "friend" ) __dialogue.confirm(_notificationItem.content, __scope.lyric.NOTIFICATION_FRIEND_INVITE_ASKING, _onFriendConfirm);
        else _updateNotificationRead(_notificationItem);

        function _onGroupConfirm() {
            __api.groupUsersSelfAllow(
                    _notificationItem.parent_id,
                    _accountDatum.id,
                    "enable"
                )
                .success(_onGroupSuccess);
        }

        function _onFriendConfirm() {
            __api.friendAgree(_notificationItem.userFriendId, "enable")
                .success(_onFriendSuccess);
        }

        function _onGroupSuccess() {
            _updateNotificationRead(_notificationItem);
        }

        function _onFriendSuccess() {
            _disableNotificationItem(_notificationItem);
        }

    }

    function _onAcceptMeetingClick(_item) {

        __dialogue.confirm(__scope.lyric.NOTIFICATION_MEETING_INVITE_BUTTON, __scope.lyric.NOTIFICATION_MEETING_INVITE_ASKING, _onConfirm);

        function _onConfirm() {
            _fetchMeeting(_item.parent_id, _onFetchSuccess, _onError);
        }

        function _onFetchSuccess(_response) {
            __api.meetingAgree(
                    _item.parent_id,
                    _response.meeting_date,
                    _response.meeting_time,
                    _response.meeting_summary
                )
                .success(_onAgreeSuccess)
                .error(_onError);
        }

        function _onAgreeSuccess() {
            _updateNotificationRead(_item, _fetchMeetingNotifications);
        }

        function _onError() {

        }

    }

    function _onCancelMeetingClick(_item) {

        var _reasonText;
        __dialogue.prompt(__scope.lyric.NOTIFICATION_MEETING_INVITE_CANCEL_BUTTON, __scope.lyric.NOTIFICATION_MEETING_INVITE_CANCEL_ASKING, _onConfirm, _onMislay);

        function _onConfirm(_response) {
            _reasonText = _response;
            _fetchMeeting(_item.parent_id, _onFetchSuccess, _onError);
        }

        function _onMislay() {

        }

        function _onFetchSuccess(_response) {
            __api.meetingDelete(
                    _item.parent_id,
                    _reasonText,
                    _response.meeting_date,
                    _response.meeting_time,
                    _response.meeting_summary
                )
                .success(_onDeleteSuccess)
                .error(_onError);
        }

        function _onDeleteSuccess() {
            _updateNotificationRead(_item, _fetchMeetingNotifications);
        }

        function _onError() {

        }

    }

    function _onConferenceClick(_notificationItem) {

        var _url;
        var _recruiterId;
        __api.meeting(_notificationItem.parent_id)
            .success(_onMeetingSuccess);

        function _onMeetingSuccess(_response) {
            _response = _response[0];
            _recruiterId = _response.owner_id;
            __api.meetingZoom(_notificationItem.parent_id, _response.owner_id)
                .success(_onZoomSuccess);
            __dialogue.countdown(__scope.lyric.ZOOM_CREATING_LETTER);
        }

        function _onZoomSuccess(_response) {
            _response = _response[0];
            _url = ( _notificationItem.receiver_id === _recruiterId ) ? _response.start_url : _response.join_url;
            __dialogue.confirm(__scope.lyric.ZOOM_TEMPORARY_BUTTON, __scope.lyric.ZOOM_TEMPORARY_ASKING, _onConfirm);
        }

        function _onConfirm() {
            window.open(_url, "_blank");
            _updateNotificationRead(_notificationItem);
        }

    }

    function _fetchNotification() {

        __api.noticeList("group", "init")
            .success(_onGroupSuccess);

        function _onGroupSuccess(_response) {
            __scope.groupNotifications = _arrangeNotificatinos("group", _response);
            _fetchMeetingNotifications();
        }

    }

    function _fetchMeetingNotifications() {

        __api.noticeList("meeting", "init")
            .success(_onMeetingSuccess);

        function _onMeetingSuccess(_response) {
            __scope.meetingNotifications = _arrangeNotificatinos("meeting", _response) || [] ;
            __api.noticeList("meeting_booker", "init")
                .success(_onBookerSuccess);
        }

        function _onBookerSuccess(_response) {
            __scope.meetingNotifications = ( __scope.meetingNotifications || [] ).concat( _arrangeNotificatinos("meeting", _response) || [] );
            __scope.$apply();
            _fetchConferenceNotifications();
        }

    }

    function _fetchConferenceNotifications() {

        __api.noticeList("meeting_cronjob", "init")
            .success(_onSuccess);

        function _onSuccess(_response) {
            __scope.conferenceNotifications = _arrangeNotificatinos("conference", _response);
            __scope.$apply();
        }

    }

    function _updateNotificationRead(_notificationItem, _callback) {

        __api.noticeRead(_notificationItem.id)
            .success(_onSuccess);

        function _onSuccess() {
            _disableNotificationItem(_notificationItem);
            if ( _callback ) _callback();
        }

    }

    function _fetchMeeting(_id, _callback, _errorback) {

        __api.meeting(_id)
            .success(_onSuccess)
            .error(_onError);

        function _onSuccess(_response) {
            _response = ( _response || [] )[0] || {} ;
            var _booker = ( _response.meeting_bookers || [] )[0];
            if ( _callback ) _callback(_booker);
        }

        function _onError() {
            if ( _errorback ) _errorback();
        }

    }

    function _arrangeNotificatinos(_type, _notificationList) {

        var _index = ( _notificationList || [] ).length;
        var _item;
        while ( _index-- > 0) {
            _item = _notificationList[_index];
            if ( _type === "group" ) {
                if ( _item.avatar ) _item.avatarUrl = __path.PUBLIC + "/gallery/group_" + _item.parent_id + "/avatar/" + _item.avatar + "_q.png" ;
                else _item.avatarUrl = "/asset/image/dock-avatar-default.png";
            }
            if ( _type === "meeting" ) {
                if ( /Make\sappointment\swith/.test(_item.content) ) _item.isAsking = true;
                _notificationList[_index].content = _item.content.replace(/<br>\s?/g, "\n");
                _item.avatarUrl = _item.avatar_links[0];
                if ( _item.avatarUrl ) _item.avatarUrl = __path.PUBLIC + _item.avatarUrl ;
                else _item.avatarUrl = "/asset/image/dock-avatar-default.png";
            }
            if ( _type === "conference" ) {
                if ( _item.content.indexOf("please click here") >= 0 ) _item.isNow = true;
            }
        }
        return _notificationList;

    }

    function _disableNotificationItem(_notificationItem) {

        _notificationItem.isRemove = true;
        __scope.$apply();

    }

}(window.allot));
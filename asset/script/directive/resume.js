(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __util;
    var __api;
    var __translator;
    var _languageTerm;

    (function () {

        _resumeDirective.$inject = ["$TEMPLATE_PATH", "$FILE_CACHE_TIME"];
        _resumeController.$inject = ["$scope", "$util", "$api", "$translator"];
        __ngApp.directive("resume", _resumeDirective);

    }());

    function _resumeDirective(_TEMPLATE_PATH, _FILE_CACHE_TIME) {

        return {
                scope: true,
                restrict: "C",
                templateUrl: _TEMPLATE_PATH + "resume.html" + _FILE_CACHE_TIME ,
                controller: _resumeController
            };

    }

    function _resumeController(_scope, _util, _api, _translator) {

        __scope = _scope;
        __util = _util;
        __api = _api;
        __translator = _translator;
        _defineVariable();
        _defineFunction();
        _readAccount();

    }

    function _defineVariable() {

        __scope.isModify = false;
        __scope.modifiedUserDatum = {};
        __scope.genderArray = {
                male: __scope.lyric.GENDER_MALE,
                female: __scope.lyric.GENDER_FEMALE,
                others: __scope.lyric.GENDER_OTHER
            };
        __scope.YearArray = __util.getIntegerArray(1970, ( (new Date()).getFullYear() - 10 ) );
        __scope.MonthArray = __util.getIntegerArray(1, 12);
        __scope.DateArray = __util.getIntegerArray(1, 31);
        __scope.educationTypeDictionary = {
                high_school: __scope.lyric.EDUCATION_HIGHSCHOOL,
                collage: __scope.lyric.EDUCATION_UNIVERSITY,
                graduate_school: __scope.lyric.EDUCATION_GRADUATE
            };
        __scope.petDictionary = {
                dog: "Dog",
                cat: "Cat",
                others: "Others"
            };
        _languageTerm = __translator.getLanguageTerm(__scope.language);

    }

    function _defineFunction() {

        __scope.confirmEducationType = _confirmEducationType;
        __scope.toggleModify = _toggleModify;

    }

    function _readAccount() {

        __scope.$watch("userDatum", _onWatch);

        function _onWatch() {
            if ( !( __scope.userDatum || {} ).id  ) return;
            _updateUserDatum();
            _readLanguageList();
            if ( false ) _readCountryArray();
            if ( false ) _readInterestArray();
            __api.user(__scope.userDatum.introducerId)
                .success(_onIntroducerRead);
        }

        function _updateUserDatum() {
            var _birthSplit;
            if ( !__scope.userDatum.gender ) delete __scope.userDatum.gender;
            if ( __scope.userDatum.birth && /^\d{4}([\-\.\/]\d{1,2}){2}$/.test(__scope.userDatum.birth) ) {
                _birthSplit = __scope.userDatum.birth.split(/[\-\.\/]/g);
                __scope.userDatum.birthYear = parseInt(_birthSplit.shift());
                __scope.userDatum.birthMonth = parseInt(_birthSplit.shift());
                __scope.userDatum.birthDate = parseInt(_birthSplit.shift());
            }
        }

        function _onIntroducerRead(_response) {
            var _responseProfile = ( _response.user || {} ).profile;
            __scope.introducerName = _responseProfile.first_name + " " + _responseProfile.last_name ;
            __scope.$apply();
        }

    }

    function _readLanguageList() {

        __api.language(_languageTerm)
            .success(_onSuccess);

        function _onSuccess(_response) {
            var _languageList = __scope.languageList = _response.languages;
            var _index = _languageList.length;
            var _item;
            __scope.languageDictionary = {};
            while ( _index-- > 0 ) {
                _item = _languageList[_index];
                __scope.languageDictionary[_item.code] = _item.language_name;
                if ( ( __scope.userDatum.languages || [] ).indexOf(_item.code) < 0 ) continue;
                _item.check = true;
            }
            __scope.$apply();
        }

    }

    function _readCountryArray() {

        __api.country(_languageTerm)
            .success(_onSuccess);

        function _onSuccess(_response) {
            __scope.countryArray = _response.countries;
        }

    }

    function _readInterestArray() {

        __api.interest(_languageTerm)
            .success(_onSuccess);

        function _onSuccess(_response) {
            __scope.interestArray = _response.interests;
        }

    }

    function _confirmEducationType() {

        if ( /collage|graduate_school/.test(__scope.modifiedUserDatum.educationType) ) return true;

    }

    function _toggleModify() {

        if ( __scope.isModify ) return _updateUserDatum();
        __scope.modifiedUserDatum = JSON.parse(JSON.stringify(__scope.userDatum));
        __scope.isModify = true;

    }

    function _updateUserDatum() {

        _updateProfile(_onSuccess);

        function _onSuccess() {
            __scope.isModify = false;
            __scope.readAccount();
        }

    }

    function _updateProfile(_callback) {

        __scope.modifiedUserDatum.birth = _getFormalBirth();
        __scope.modifiedUserDatum.website = _getFormalWebsite();
        __scope.modifiedUserDatum.languages = _getFormalLanguages();
        __api.userProfile(
                __scope.modifiedUserDatum.id,
                __scope.modifiedUserDatum.firstName,
                __scope.modifiedUserDatum.lastName,
                __scope.modifiedUserDatum.nickName,
                __scope.modifiedUserDatum.gender,
                __scope.modifiedUserDatum.birth,
                null,
                __scope.modifiedUserDatum.intro,
                null,
                __scope.modifiedUserDatum.languages
            )
            .success(_onSuccess);

        function _onSuccess() {
            if ( __scope.modifiedUserDatum.educationSchool ) _updateEducation(_callback);
            else if ( _callback ) _callback();
        }

        function _getFormalBirth() {
            var _birthYear = __scope.modifiedUserDatum.birthYear;
            var _birthMonth = __scope.modifiedUserDatum.birthMonth;
            var _birthDate = __scope.modifiedUserDatum.birthDate;
            if ( !_birthYear ||  !_birthMonth || !_birthDate ) return;
            if ( _birthMonth < 10 ) _birthMonth = "0" + _birthMonth ;
            if ( _birthDate < 10 ) _birthDate = "0" + _birthDate ;
            return _birthYear + "-" + _birthMonth + "-" + _birthDate ;
        }

        function _getFormalWebsite() {
            if ( !__scope.modifiedUserDatum.website ) return;
            if ( /^https?\:\/{2}/.test(__scope.modifiedUserDatum.website) ) return __scope.modifiedUserDatum.website;
            return "http://" + __scope.modifiedUserDatum.website ;
        }

        function _getFormalLanguages() {
            var _array = [];
            var _key;
            var _item;
            for ( _key in __scope.languageList ) {
                _item = __scope.languageList[_key];
                if ( !_item.check ) continue;
                _array.push(_item.code);
            }
            return _array;
        }

    }

    function _updateEducation(_callback) {

        var _IS_UPDATE = !!( __scope.userDatum.userEducation );
        var _parameters = [
                __scope.modifiedUserDatum.educationSchool,
                __scope.modifiedUserDatum.educationType,
                [ _confirmEducationType() ? __scope.modifiedUserDatum.educationMajor : null ]
            ];
        if ( _IS_UPDATE ) _parameters.unshift(__scope.userDatum.userEducation.id);
        __api[ _IS_UPDATE ? "userEducationUpdate" : "userEducationCreate" ].apply(null, _parameters)
            .success(_onSuccess);

        function _onSuccess() {
            if ( _callback ) _callback();
        }

    }

}(window.allot));
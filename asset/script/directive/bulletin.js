(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __path;
    var __api;
    var __account;
    var __user;
    var __dialogue;
    var __documentPress;
    var _videoInput;
    var _photoInput;
    var _MODE_USER;
    var _MODE_GROUP;

    (function () {

        _bulletinDirective.$inject = ["$TEMPLATE_PATH", "$FILE_CACHE_TIME"];
        _bulletinController.$inject = ["$scope", "$path", "$api", "$account", "$user", "$dialogue", "$documentPress"];
        __ngApp.directive("bulletin", _bulletinDirective);

    }());

    function _bulletinDirective(_TEMPLATE_PATH, _FILE_CACHE_TIME) {

        return {
                scope: true,
                restrict: "C",
                templateUrl: _TEMPLATE_PATH + "bulletin.html" + _FILE_CACHE_TIME ,
                controller: _bulletinController
            };

    }

    function _bulletinController(_scope, _path, _api, _account, _user, _dialogue, _documentPress) {

        __scope = _scope;
        __path = _path;
        __api = _api;
        __account = _account;
        __user = _user;
        __dialogue = _dialogue;
        __documentPress = _documentPress;
        _defineVariable();
        _defineFunction();
        _readAccount();

    }

    function _defineVariable() {

        _MODE_USER = __scope.MODE_USER = ( __scope.modeName === "User" );
        _MODE_GROUP = __scope.MODE_GROUP = ( __scope.modeName === "Group" );
        __scope.posterDatum = {};

    }

    function _defineFunction() {

        __scope.getPosterAvatar = _getPosterAvatar;
        __scope.getPosterPhotoItemStyle = _getPosterPhotoItemStyle;
        __scope.confirmPosterPrivacy = _confirmPosterPrivacy;
        __scope.setPhotoChange = _setPhotoChange;
        __scope.onPosterVideoClick = _onPosterVideoClick;
        __scope.onPosterImageClick = _onPosterImageClick;
        __scope.onPhotoRemoveClick = _onPhotoRemoveClick;
        __scope.onPrivacyToggle = _onPrivacyToggle;
        __scope.onPrivacyClick = _onPrivacyClick;
        __scope.onPostClick = _onPostClick;

    }

    function _readAccount() {

        __account.addCallback(_onRead);
        __account.read();

        function _onRead(_response) {
            var _responseDatum = _response.user || {} ;
            __scope.accountDatum = __user.arrange(_responseDatum);
        }

    }

    function _getPosterAvatar() {

        var _datum = __scope.accountDatum;
        if ( _MODE_GROUP ) {
            if ( __scope.groupDatum.currentUser && ( __scope.groupDatum.currentUser.role !== "member" ) ) _datum = __scope.groupDatum;
        }
        var _url = ( _datum || {} ).avatarUrl;
        if ( !(_url) ) return;
        if ( !/\.(jpg|jpeg|png|gif)$/.test(_url) ) _url += "_o.png";
        return {
                "background-image": "url(\"" + _url + "\")"
            };

    }

    function _getPosterPhotoItemStyle(_photoItem) {

        return {
                "background-image": "url(" + __path.PUBLIC + _photoItem + ")"
            };

    }

    function _confirmPosterPrivacy() {

        if ( _MODE_USER ) {
            if ( ( __scope.userDatum || {} ).id === ( __scope.accountDatum || {} ).id ) return true;
        } if ( _MODE_GROUP ) {
            var _currentUser = __scope.groupDatum.currentUser;
            if ( !(_currentUser) ) return;
            if ( /^(owner|admin|recruiter|editor)$/.test(_currentUser.role) ) return true;
        }

    }

    function _setPhotoChange() {

        _photoInput = document.querySelector(".bulletin-poster-options-photo + input[type=file]");
        _photoInput.addEventListener("change", _onPosterPhotoChange);

    }

    function _onPosterVideoClick() {

        if ( !(_videoInput) ) {
            _videoInput = document.createElement("input");
            _videoInput.type = "file";
            _videoInput.accept = "video/mp4";
            _videoInput.onchange = _onPosterVideoChange;
        }
        _videoInput.click();

    }

    function _onPosterImageClick() {

        _photoInput.click();

    }

    function _onPhotoRemoveClick(_fileItem) {

        __dialogue.confirm(__scope.lyric.BULLETIN_POSTER_REMOVE_PHOTO_BUTTON, __scope.lyric.BULLETIN_POSTER_REMOVE_PHOTO_ASKING, _onConfirm);

        function _onConfirm() {
            var _fileList = __scope.posterDatum.fileList;
            var _index = _fileList.length;
            var _itemInList;
            while ( _index-- > 0 ) {
                _itemInList = _fileList[_index];
                if ( _itemInList.id !== _fileItem.id ) continue;
                _fileList.splice(_index, 1);
            }
        }

    }

    function _onPosterVideoChange() {

        var _file = _videoInput.files[0];
        delete __scope.posterDatum.videoLocalUrl;
        __scope.posterDatum.videoFile = __scope.posterDatum.videoFile || {} ;
        __scope.posterDatum.isVideoWorking = true;
        __scope.$apply();
        _createFile(_file, _onSuccess, null, _onMislay, _onFinish);

        function _onSuccess(_response) {
            var URL = window.URL || window.webkitURL ;
            _videoInput.value = null;
            __scope.posterDatum.videoFile = {
                    id: _response.file_id,
                    link: _response.file_links[0]
                };
            __scope.posterDatum.videoLocalUrl = URL.createObjectURL(_file);
        }

        function _onMislay() {
            delete __scope.posterDatum.isVideoWorking;
        }

        function _onFinish() {
            delete __scope.posterDatum.isVideoWorking;
            __scope.$apply();
        }

    }

    function _onPosterPhotoChange() {

        __scope.posterDatum.fileList = __scope.posterDatum.fileList || [] ;
        __scope.posterDatum.isPhotoWorking = true;
        __scope.$apply();
        _createFile(_photoInput.files[0], _onSuccess, null, _onMislay, _onFinish);

        function _onSuccess(_response) {
            _photoInput.value = null;
            __scope.posterDatum.fileList.unshift({
                    id: _response.file_id,
                    link: _response.file_links[0]
                });
        }

        function _onMislay() {
            delete __scope.posterDatum.isPhotoWorking;
        }

        function _onFinish() {
            delete __scope.posterDatum.isPhotoWorking;
            __scope.$apply();
        }

    }

    function _onPrivacyToggle(_event) {

        _toggleSelect();
        if ( !(__scope.posterDatum.isPrivacySelect) ) __documentPress.reset();
        else __documentPress.add(_onHandler, _event.target);

        function _toggleSelect() {
            __scope.posterDatum.isPrivacySelect = !(__scope.posterDatum.isPrivacySelect);
        }

        function _onHandler() {
            __documentPress.reset();
            _toggleSelect(false);
            __scope.$apply();
        }

    }

    function _onPrivacyClick(_privacyType) {

        if ( !/^(public|private)$/.test(_privacyType) ) return;
        __scope.posterDatum.privacy = _privacyType;

    }

    function _onPostClick() {

        _createBoard();

    }

    function _readDatum() {

        if ( _MODE_USER ) if ( __scope.readAccount ) __scope.readAccount();
        if ( _MODE_GROUP ) if ( __scope.readGroup ) __scope.readGroup();

    }

    function _createBoard() {

        if ( !( __scope.posterDatum.content || __scope.posterDatum.videoFile || ( __scope.posterDatum.fileList || [] ).length ) ) return _onMislay();
        __api[ _MODE_USER ? "boardCreate" : "boardGroupCreate" ](
                _MODE_USER ? __scope.userDatum.blogId : __scope.groupDatum.id ,
                _MODE_USER ? "blog" : "group" ,
                __scope.posterDatum.content,
                __scope.posterDatum.privacy,
                null,
                __scope.posterDatum.videoFile || _getFileIdArray()
            )
            .success(_onSuccess)
            .mislay(_onMislay);

        function _onSuccess() {
            __scope.posterDatum = {};
            _readDatum();
        }

        function _onMislay() {
            __dialogue.alert(__scope.lyric.BULLETIN_POSTER_EMPTY);
        }

        function _getFileIdArray() {
            var _list = __scope.posterDatum.fileList || [] ;
            var _index = _list.length;
            var _array = [];
            while ( _index-- > 0 ) _array.unshift(_list[_index].id);
            return _array;
        }

    }

    function _createFile(_file, _callback, _errorback, _mislayback, _finishback) {

        __api.fileCreate(
                _file,
                _MODE_USER ? __scope.userDatum.blogId : __scope.groupDatum.id,
                _MODE_USER ? "blog" : "group"
            )
            .success(_onSuccess)
            .mislay(_onMislay)
            .finish(_onFinish);

        function _onSuccess(_response) {
            if ( _callback ) _callback(_response);
        }

        function _onMislay() {
            if ( _mislayback ) _mislayback();
        }

        function _onFinish() {
            if ( _finishback ) _finishback();
        }

    }

}(window.allot));
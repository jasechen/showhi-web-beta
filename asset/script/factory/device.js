(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var _code;
    var _os;
    var _type;
    var _language;

    (function () {

        __ngApp.factory("$device", _deviceFactory);

    }());

    function _deviceFactory() {

        _os = _getOS();
        _type = _getType();
        _language = _getLanguage();
        return {
                code: _code,
                os: _os,
                type: _type,
                language: _language
            };

    }

    function _getOS() {

        var _nAgent = navigator.userAgent;
        var _osList = [{
                name: "ios",
                regex: /iP(ad|od|hone)/
            }, {
                name: "android",
                regex: /Android/
            }, {
                name: "macos",
                regex: /Mac/
            }, {
                name: "win",
                regex: /Win/
            }];
        var _tempOS;
        while ( _osList.length ) {
            _tempOS = _osList.shift();
            if ( _tempOS.regex.test(_nAgent) ) return _tempOS.name;
        }

    }

    function _getType() {

        var _nVersion = navigator.appVersion;
        var _typeList = [{
                name: "pad",
                regex: /iPad/
            }, {
                name: "phone",
                regex: /Mobile|mini|Fennec|Android|iP(od|hone)/
            }];
        var _tempType;
        while ( _typeList.length ) {
            _tempType = _typeList.shift();
            if ( _tempType.regex.test(_nVersion) ) return _tempType.name;
        }
        return "desktop";

    }

    function _getLanguage() {

        var _nLanguage = navigator.language.toLowerCase();
        var _languageList = [{
                name: "zh_cn",
                regex: /zh-cn/
            }, {
                name: "zh_tw",
                regex: /zh/
            }, {
                name: "jp",
                regex: /jp/
            }];
        var _tempLanguage;
        while ( _languageList.length ) {
            _tempLanguage = _languageList.shift();
            if ( _tempLanguage.regex.test(_nLanguage) ) return _tempLanguage.name;
        }
        return "en_us";

    }

}(window.allot));
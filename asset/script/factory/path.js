(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __config = __allot("config");
    var _serverDict;
    var _typeDict;
    var _serverName;
    var _apiPath;
    var _publicPath;

    (function () {

        _setDefaultData();
        __ngApp.factory("$path", _pathFactory);

    }());

    function _pathFactory() {

        return {
                API: _apiPath || _getAPIPath(),
                PUBLIC: _publicPath || _getPublicPath(),
                params: _getParams()
            };

    }

    function _setDefaultData() {

        _serverDict = {
                RELEASE: "RELEASE",
                STAGING: "STAGING",
                DEVELOP: "DEVELOP"
            };
        _typeDict = {
                API: "API",
                PUBLIC: "PUBLIC"
            };

    }

    function _getAPIPath() {

        if ( _apiPath ) return _apiPath;
        _apiPath = _getJudgedPath(_typeDict.API);
        return _apiPath;

    }

    function _getPublicPath() {

        if ( _publicPath ) return _publicPath;
        _publicPath = _getJudgedPath(_typeDict.PUBLIC);
        return _publicPath;

    }

    function _getParams() {

        return window.location.href.replace(/.+\?/g, "");

    }

    function _getJudgedPath(_type) {

        var _typeName = "_" + _type + "_" + "PATH";
        var _path;
        _serverName = _getServerName();
        _path = __config[ _serverName + _typeName ];
        if ( _path ) return _path;
        return "";

    }

    function _getServerName() {

        if ( _serverName ) return _serverName;
        var _host = window.location.host;
        var _regexHostPath = new RegExp("HOST_PATH", "g");
        var _key;
        var _tempValue;
        for ( _key in __config ) {
            _tempValue = __config[_key];
            if ( typeof(_tempValue) !== "string" ) continue;
            if ( !_key.match(_regexHostPath) ) continue;
            if ( _host !== _tempValue.replace(/\//g, "") ) continue;
            return _key.replace(_regexHostPath, "").replace(/_$/g, "");
        }

    }

}(window.allot));
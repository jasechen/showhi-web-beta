(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __path;
    var __api;
    var __account;
    var _datum;

    (function () {

        _userFactory.$inject = ["$path", "$api", "$account"];
        __ngApp.factory("$user", _userFactory);

    }());

    function _userFactory(_path, _api, _account) {

        __path = _path;
        __api = _api;
        __account = _account;
        _defineVariable();
        return {
                read: _read,
                readBlog: _readBlog,
                arrange: _getArrangedUserDatum
            };

    }

    function _defineVariable() {

        _datum = {};

    }

    function _read(_id, _callback, _errorback, _finishback) {

        if ( _callback ) if ( _datum ) _callback(_datum);
        _fetchUser(_id, _callback, _errorback, _finishback);

    }

    function _readBlog(_callback, _errorback, _finishback) {

        _fetchBlog(_callback, _errorback, _finishback);

    }

    function _fetchUser(_id, _callback, _errorback, _finishback) {

        if ( _id ) __api.user(_id)
            .success(_onSuccess)
            .finish(_onFinish);
        else __account.read(_onSuccess, null, _onFinish);

        function _onSuccess(_response) {
            _datum = _getArrangedUserDatum(_response.user);
            if ( _callback ) _callback(_datum);
            _fetchBlog(_callback, _errorback, _finishback);
        }

        function _onFinish() {
            if ( _finishback ) _finishback();
        }

    }

    function _fetchBlog(_callback, _errorback, _finishback) {

        __api.blog(_datum.blogId)
            .success(_onSuccess)
            .finish(_onFinish);

        function _onSuccess(_response) {
            var _responseBlog = _response.blog;
            _arrangeBlogDatum(_responseBlog);
            if ( _callback ) _callback(_datum);
            _fetchGroups(_callback, _errorback, _finishback);
            _fetchEducation(_callback, _errorback, _finishback);
        }

        function _onFinish() {
            if ( _finishback ) _finishback();
        }

    }

    function _fetchGroups(_callback, _errorback, _finishback) {

        __api.groupsByUser(_datum.id)
            .success(_onSuccess)
            .finish(_onFinish);

        function _onSuccess(_response) {
            _datum.groups = _getArragngedAvatar( _response || [] );
            if ( _callback ) _callback(_datum);
        }

        function _onFinish() {
            if ( _finishback ) _finishback();
        }

        function _getArragngedAvatar(_groupArray) {
            var _index = _groupArray.length;
            var _item;
            while ( _index-- > 0 ) {
                _item = _groupArray[_index];
                if ( !_item.avatar ) _item.avatarUrl = "/asset/image/dock-avatar-default.png";
                else _item.avatarUrl = __path.PUBLIC + "/gallery/group_" + _item.id + "/avatar/" + _item.avatar + "_sq.png" ;
            }
            return _groupArray;
        }

    }

    function _fetchEducation(_callback, _errorback, _finishback) {

        __api.userEducation(_datum.id)
            .success(_onSuccess)
            .error(_onError)
            .finish(_onFinish);

        function _onSuccess(_response) {
            var _userEducationDatum = ( _response.user_educations || [] )[0];
            _datum.userEducation = _userEducationDatum;
            _datum.educationType = _userEducationDatum.type;
            _datum.educationSchool = _userEducationDatum.school;
            _datum.educationMajor = _userEducationDatum.concentrations[0];
            _datum.education = _datum.educationSchool;
            if ( _datum.educationMajor ) _datum.education += " - " + _datum.educationMajor;
            if ( _callback ) _callback(_datum);
        }

        function _onError() {
            _datum.educationType = "graduate_school";
        }

        function _onFinish() {
            if ( _finishback ) _finishback();
        }

    }

    function _getArrangedUserDatum(_requestDatum) {

        var _userDatum = {};
        var _profileDatum = _requestDatum.profile;
        _userDatum.id = _profileDatum.user_id;
        _userDatum.type = _requestDatum.type;
        _userDatum.firstName = _profileDatum.first_name;
        _userDatum.lastName = _profileDatum.last_name;
        _userDatum.name = _userDatum.firstName + " " + _userDatum.lastName;
        _userDatum.nickName = _profileDatum.nickname;
        _userDatum.intro = _profileDatum.intro;
        _userDatum.gender = _profileDatum.gender;
        _userDatum.birth = _profileDatum.birth;
        _userDatum.mail = _profileDatum.email;
        _userDatum.phone = _profileDatum.mobile_phone;
        _userDatum.city = _profileDatum.city;
        _userDatum.website = _profileDatum.website;
        _userDatum.languages = _profileDatum.languages;
        _userDatum.interests = _profileDatum.interests;
        _userDatum.visits = _profileDatum.visits;
        _userDatum.pets = _profileDatum.pets;
        _userDatum.introducerId = _requestDatum.introducer_id;
        _userDatum.blogId = _requestDatum.blog_id;
        _userDatum.avatar = _profileDatum.avatar;
        _userDatum.avatarPath = _userDatum.blogId ? "/gallery/blog_" + _userDatum.blogId + "/avatar/" : "" ;
        _userDatum.avatarUrl = _getAvatarUrl();
        _userDatum.coverUrl = _datum.coverUrl;
        return _userDatum;

        function _getAvatarUrl() {
            if ( !( _userDatum.avatarPath && _userDatum.avatar ) ) return "/asset/image/dock-avatar-default.png";
            return __path.PUBLIC + _userDatum.avatarPath + _userDatum.avatar;
        }

    }

    function _arrangeBlogDatum(_responseBlog) {

        var _coverPath = _datum.coverPath = "/gallery/blog_" + _responseBlog.id + "/cover/" ;
        if ( _coverPath && _responseBlog.cover ) _datum.coverUrl = __path.PUBLIC + _coverPath + _responseBlog.cover ;
        else _datum.coverUrl = "/asset/image/dock-cover-background.jpg";

    }

}(window.allot));
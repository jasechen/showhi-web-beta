(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var _callbackAry = [];
    var _ignoreElementAry = [];

    (function () {

        __ngApp.factory("$documentPress", _documentPressFactory);

    }());

    function _documentPressFactory() {

        _reset();
        _setDocumentEvent();
        return {
                add: _addCallback,
                ignore: _addIgnoreElement,
                reset: _reset,
                callbackArray: _callbackAry,
                ignoreArray: _ignoreElementAry
            };

    }

    function _setDocumentEvent() {

        document.addEventListener("click", _onPress, false);

        function _onPress(_event) {
            var _elementIndex = _ignoreElementAry.length;
            var _element;
            var _callbackIndex = _callbackAry.length;
            var _callback;
            while ( _elementIndex-- > 0 ) {
                _element = _ignoreElementAry[_elementIndex];
                if ( _element === _event.target ) return;
            }
            while ( _callbackIndex-- > 0 ) {
                _callback = _callbackAry[_callbackIndex];
                if ( typeof(_callback) === "function" ) _callback();
            }
        }

    }

    function _addCallback(_callback) {

        var _elementArray = Array.prototype.slice.call(arguments, 1);
        if ( _callback ) _callbackAry.push(_callback);
        while ( _elementArray.length ) _addIgnoreElement(_elementArray.shift());

    }

    function _addIgnoreElement(_element) {

        if ( !_element ) return;
        _ignoreElementAry.push(_element);
        var _childArray = Array.prototype.slice.call(_element.children);
        while ( _childArray.length ) _addIgnoreElement(_childArray.shift());

    }

    function _reset() {

        _callbackAry = [];
        _ignoreElementAry = [];

    }

}(window.allot));
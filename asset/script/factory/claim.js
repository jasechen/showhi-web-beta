/*global File */
(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __path;
    var __util;
    var __NECESSARY_API_KEY;
    var __OPTIONAL_API_KEY;
    var _successCallback = {};
    var _errorCallback = {};
    var _mislayResponse = {};
    var _finishCallback = {};
    var _header;
    var _annex;

    (function () {

        _claimFactory.$inject = ["$path", "$util", "$NECESSARY_API_KEY", "$OPTIONAL_API_KEY"];
        __ngApp.factory("$claim", _claimFactory);

    }());

    function _claimFactory(_path, _util, _NECESSARY_API_KEY, _OPTIONAL_API_KEY) {

        __path = _path;
        __util = _util;
        __NECESSARY_API_KEY = _NECESSARY_API_KEY;
        __OPTIONAL_API_KEY = _OPTIONAL_API_KEY;
        _header = {};
        return {
                get: _claim("GET"),
                post: _claim("POST"),
                put: _claim("PUT"),
                delete: _claim("DELETE"),
                header: _header,
                clearHeaderTemp: _cleanHeader
            };

    }

    function _claim(_restfullType) {

        _restfullType = _restfullType || "POST" ;
        return _process;

        function _process(_code, _data) {
            _data = _data || {} ;
            var _requestName = _code + JSON.stringify(_data).replace(/[\"\']/g, "") ;
            _refreshResponses(_requestName);
            _updateAnnexData(_data);
            if ( _conformParameters(_code, _data, _requestName) ) _callAPI(_code, _data, _requestName, _restfullType);
            return _getResult(_requestName);
        }

        function _refreshResponses(_requestName) {
            _successCallback[_requestName] = null;
            _errorCallback[_requestName] = null;
            _mislayResponse[_requestName] = null;
        }

    }

    function _updateAnnexData(_data) {

        var _tempValue;
        __util.forEach(_annex, _onForEach);

        function _onForEach(_key, _val) {
            _tempValue = _data[_key];
            if ( !_tempValue || _tempValue === __OPTIONAL_API_KEY ) _data[_key] = _val;
        }

    }

    function _conformParameters(_code, _data, _requestName) {

        var _nowKey;
        var _tempValue;
        var _mislayList = [];
        for (_nowKey in _data) {
            _tempValue = _data[_nowKey];
            switch ( true ) {
                case !_tempValue && ( _tempValue !== 0 ) && ( _tempValue !== false ) :
                case _tempValue === __NECESSARY_API_KEY:
                    _mislayList.push(_nowKey);
                    break;
                case _tempValue === __OPTIONAL_API_KEY:
                    delete _data[_nowKey];
                    break;
            }
        }
        if ( !_mislayList.length ) return true;
        _mislayResponse[_requestName] = {
                miss: _mislayList
            };
        return false;

    }

    function _callAPI(_url, _datum, _requestName, _restfullType) {

        var _xhttp = new XMLHttpRequest();
        var _dataForRequest;
        var _isUrlenForm;
        var _apiURL = "";
        _replaceURLQuoter();
        _dataForRequest = _getRequestData(_datum);
        _isUrlenForm = ( typeof(_dataForRequest) === "string" );
        _apiURL += ( _url.match(/^(http|\/)/g) ) ? "" : __path.API ;
        _apiURL += _url;
        _apiURL += ( _restfullType === "GET" ) ? _catchTime() : "" ;
        _xhttp.onreadystatechange = _onReadyStateChange;
        _xhttp.open(_restfullType, _apiURL, true);
        if ( _isUrlenForm ) _xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
        if ( !/\.json($|\??)/.test(_url) ) {
            if ( _header.session ) _xhttp.setRequestHeader("Session", _header.session);
            if ( _header.token ) _xhttp.setRequestHeader("Token", _header.token);
        }
        _xhttp.send(_dataForRequest);

        function _replaceURLQuoter() {
            var _quoterArray = _url.match(/\{[\w\d_-]*\}/g) || [] ;
            var _quoter;
            var _temp;
            while ( _quoterArray.length ) {
                _quoter = _quoterArray.shift();
                _temp = _datum[_quoter];
                if ( _temp === null || _temp === undefined  ) break;
                _url = _url.replace(_quoter, _temp);
                delete _datum[_quoter];
            }
        }

        function _onReadyStateChange() {
            try {
                if ( _xhttp.readyState !== 4 ) return;
                var _response = JSON.parse(_xhttp.responseText.replace(/\n/g, "\\n"));
                if ( _xhttp.status === 200 ) _onAPISuccess(_response);
                else _onAPIError(_response);
                _onAPIFinish();
            } catch (_error) {
                _onAPIError({
                        comment: _error.message
                    }, _requestName);
                _onAPIFinish();
                throw _error;
            }
        }

        function _onAPISuccess(_response) {
            var _isJSONFile = /\.json\??/.test(_requestName);
            if ( _response.status === "success" || _isJSONFile ) {
                if ( !_successCallback[_requestName] ) return;
                if ( !(_isJSONFile) ) _response = _response.data;
                _successCallback[_requestName](_response);
            } else _onAPIError(_response, _requestName);
        }

        function _onAPIError(_response) {
            if ( !_errorCallback[_requestName] ) return;
            _errorCallback[_requestName](_response);
        }

        function _onAPIFinish() {
            if ( !_finishCallback[_requestName] ) return;
            _finishCallback[_requestName]("FINISH");
            delete _finishCallback[_requestName];
        }

    }

    function _catchTime() {

        return "?v=" + ( new Date() ).getTime() ;

    }

    function _getRequestData(_data) {

        var _isFileForm = _conformDataFile(_data);
        var _requestData = ( _isFileForm ) ? new FormData() : "";
        __util.forEach(_data, _onForEach);
        return _requestData;

        function _onForEach(_key, _val) {
            if ( _isFileForm ) _requestData.append(_key, _val);
            else _requestData += ( _requestData.length ? "&" : "" ) + _getJudgedValue(_key, _val);
        }

        function _conformDataFile(_data) {
            var _nowKey;
            for (_nowKey in _data) {
                if ( _data[_nowKey] instanceof(Blob) ) return true;
                if ( _data[_nowKey] instanceof(File) ) return true;
            }
            return false;
        }

    }

    function _getJudgedValue(_keyName, _val) {

        if ( ( _val === null ) || ( typeof(_val) !== "object" ) ) {
            _val = String( _val || "" ).replace(/\&/g, "%26");
            return _keyName + "=" + _val ;
        }
        return _getObjectDatum(_val, _keyName);

    }

    function _getObjectDatum(_collect, _parentName) {

        var _result = "";
        var _isFirst = true;
        _parentName = _parentName || "";
        __util.forEach(_collect, _onForEach);
        return _result;

        function _onForEach(_key, _val) {
            _result += ( _isFirst ? "" : "&" ) + _getJudgedValue( _parentName + "%5B" + _key + "%5D" , _val);
            if ( _isFirst ) _isFirst = null;
        }

    }

    function _getResult(_requestName) {

        return {
                success: _setSuccess,
                error: _setError,
                mislay: _setMislay,
                finish: _setFinish
            };

        function _setSuccess(_callback) {

            _successCallback[_requestName] = _callback;
            return _getResult(_requestName);

        }

        function _setError(_callback) {

            _errorCallback[_requestName] = _callback;
            return _getResult(_requestName);

        }

        function _setMislay(_callback) {

            if ( _callback && _mislayResponse[_requestName] ) _callback(_mislayResponse[_requestName]);
            return _getResult(_requestName);

        }

        function _setFinish(_callback) {

            _finishCallback[_requestName] = _callback;
            return _getResult(_requestName);

        }

    }

    function _cleanHeader() {

        if ( !arguments.length ) _header = {};
        else {
            var _arguments = Array.prototype.slice.call(arguments);
            while ( _arguments.length ) delete _header[_arguments.shift()];
        }

    }

}(window.allot));
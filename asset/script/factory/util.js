(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");

    (function () {

        __ngApp.factory("$util", _utilFactory);

    }());

    function _utilFactory() {

        return {
                forEach: _forEach,
                aB: _changeAB(false),
                bA: _changeAB(true),
                getExpireDate: _getExpireDate,
                getFormalDate: _getFormalDate,
                getIntegerArray: _getIntegerArray
            };

    }

    function _forEach(_collection, _callback) {

        if ( !_collection ) return;
        if ( Object.prototype.toString.call(_collection) === "[object Array]" ) _collection.forEach(_onForEachArray);
        else Object.keys(_collection).forEach(_onForEachObject);

        function _onForEachArray(_value, _index) {
            _callback(_index, _value);
        }

        function _onForEachObject(_key) {
            _callback(_key, _collection[_key]);
        }

    }

    function _changeAB(_isToB) {

        var _ST_STR_1 = "maPLe";
        var _ST_STR_2 = "jAy";
        var _ND_STR_1 = "nG39f";
        var _ND_STR_2 = "b3j9F=";
        return _present;

        function _present(_value) {
            if ( !_value ) return;
            try {
                if ( !_isToB ) return encodeURIComponent( _ND_STR_1 + window.btoa(_ST_STR_1 + encodeURIComponent(_value) + _ST_STR_2) + _ND_STR_2 );
                else return decodeURIComponent(window.atob(decodeURIComponent(_value).replace(_ND_STR_1, "").replace(new RegExp(_ND_STR_2 + "$"), "")).replace(_ST_STR_1, "").replace(new RegExp(_ST_STR_2 + "$"), ""));
            } catch ( _error ) {
                return _value;
            }
        }

    }

    function _getExpireDate(_createDate, _expireTime) {

        var _date = new Date(_createDate);
        _date.setSeconds( _date.getSeconds() + _expireTime );
        return _getFormalDate(_date, true);

    }

    function _getFormalDate(_date, _withTime) {

        _date = _date || new Date() ;
        var _stringDate = _date.getFullYear() + "-" + ( _date.getMonth() + 1 ) + "-" + _date.getDate() ;
        var _stringTime = _date.getHours() + ":" + _date.getMinutes() + ":" + _date.getSeconds() ;
        _stringDate = _stringDate.replace(/\-(\d)((?=[\-])|$)/g, "-0$1");
        return _stringDate + ( _withTime ? " " + _stringTime : "" ) ;

    }

    function _getIntegerArray(_firstInteger, _lastInteger) {

        var _index = _lastInteger;
        var _array = [];
        if ( !Number.isInteger(_firstInteger) ) _firstInteger = 0;
        if ( !Number.isInteger(_lastInteger) ) _lastInteger = 0;
        while ( _index >= _firstInteger ) _array.unshift(_index--);
        return _array;

    }

}(window.allot));
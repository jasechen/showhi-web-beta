(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __interval;
    var _intervalMissions;
    var _missionsCount;

    (function () {

        _callIntervalSecondsFactory.$inject = ["$interval"];
        __ngApp.factory("$interim", _callIntervalSecondsFactory);

    }());

    function _callIntervalSecondsFactory(_interval) {

        __interval = _interval;
        _intervalMissions = [];
        _missionsCount = 0;
        workInterval();
        return {
                set: _setIntervalSeconds,
                remove: _removeIntervalMissionByID
            };

    }

    function workInterval() {

        __interval(_doIntervalMissions, 1000);

    }

    function _doIntervalMissions() {

        var _nowNum = _intervalMissions.length;
        while ( _nowNum-- > 0 ) {
            _intervalMissions[_nowNum].timeout--;
            if ( _intervalMissions[_nowNum].timeout > 0 ) continue;
            _intervalMissions[_nowNum].timeout = _intervalMissions[_nowNum].seconds;
            _intervalMissions[_nowNum].content();
            if ( _intervalMissions[_nowNum].max >= 0 ) _intervalMissions[_nowNum].max--;
            if ( _intervalMissions[_nowNum].max === 0 ) _intervalMissions.splice(_nowNum, 1);
        }

    }

    function _setIntervalSeconds(_callback, _seconds, _max) {

        var _mission = {
                id : _missionsCount,
                content : _callback,
                seconds: _seconds,
                timeout: _seconds,
                max: _max || -1
            };
        _missionsCount++;
        _intervalMissions.push(_mission);
        return _mission.id;

    }

    function _removeIntervalMissionByID(_missionID) {

        var _nowNum = _intervalMissions.length;
        while ( _nowNum-- > 0 ) {
            if ( _missionID !== _intervalMissions[_nowNum].id ) continue;
            _intervalMissions.splice(_nowNum, 1);
            return;
        }

    }

}(window.allot));
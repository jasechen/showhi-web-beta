(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __rootScope;
    var __path;
    var __api;
    var __account;
    var _datum;
    var _accountDatum;

    (function () {

        _groupFactory.$inject = ["$rootScope", "$path", "$api", "$account"];
        __ngApp.factory("$group", _groupFactory);

    }());

    function _groupFactory(_rootScope, _path, _api, _account) {

        __rootScope = _rootScope;
        __path = _path;
        __api = _api;
        __account = _account;
        return {
                read: _read
            };

    }

    function _read(_id, _callback) {

        if ( _datum && ( _id === _datum.id ) ) _callback(_datum);
        if ( !(_id) ) return;
        __api.groups(_id)
            .success(_onSuccess)
            .finish(_onFinish);

        function _onSuccess(_response) {
            var _responseDatum = _response[0];
            var _originDatum = _datum || {} ;
            if ( !(_responseDatum) ) return;
            _datum = _responseDatum;
            if ( _originDatum.currentUser ) _datum.currentUser = _originDatum.currentUser;
            if ( _originDatum.members ) _datum.members = _originDatum.members;
            if ( _originDatum.recruiters ) _datum.recruiters = _originDatum.recruiters;
            _arrangeDatum(_datum);
            _readAccount(_callback);
            if ( _callback ) _callback(_datum);
        }

        function _arrangeDatum(_datum) {
            var _avatarPath = _datum.avatarPath = "/gallery/group_" + _datum.id + "/avatar/" ;
            var _coverPath = _datum.coverPath = "/gallery/group_" + _datum.id + "/cover/" ;
            if ( !( _avatarPath && _datum.avatar ) ) _datum.avatarUrl = "/asset/image/dock-avatar-default.png";
            else _datum.avatarUrl = __path.PUBLIC + _avatarPath + _datum.avatar ;
            if ( !( _coverPath && _datum.cover ) ) _datum.coverUrl = "/asset/image/dock-cover-background.jpg";
            else _datum.coverUrl = __path.PUBLIC + _coverPath + _datum.cover ;
            _datum.privacy = _datum.privacy_status;
        }

        function _onFinish() {
            __rootScope.groupDatum = _datum;
            __rootScope.$apply();
        }

    }

    function _readAccount(_callback) {

        __account.read(_onRead);

        function _onRead(_response) {
            _accountDatum = _response.user || {} ;
            if ( _accountDatum.id === _datum.owner_id ) _datum.isOwned = true;
            _readMembers(_callback);
        }

    }

    function _readMembers(_callback) {

        __api.groupUsersMembers(_datum.id)
            .success(_onSuccess)
            .finish(_onFinish);

        function _onSuccess(_response) {
            var _responseMembers = _getArragedMembers(_response);
            _datum.members = _responseMembers;
            _datum.recruiters = _getArrangedRecruiters(_responseMembers);
            if ( _callback ) _callback(_datum);
        }

        function _onFinish() {
            __rootScope.$apply();
        }

        function _getArragedMembers(_responseMembers) {
            var _index = _responseMembers.length;
            var _item;
            var _tempPath;
            delete _datum.currentUser;
            while ( _index-- > 0 ) {
                _item = _responseMembers[_index];
                _item.userId = _item.user_id;
                if ( ( _item.userId === _accountDatum.id ) && ( _item.status !== "disable" ) ) _datum.currentUser = _item;
                _item.name = _item.first_name + " " + _item.last_name ;
                _tempPath = ( _item.avatar_links || [] )[0];
                if ( _tempPath ) _item.avatarUrl = __path.PUBLIC + _tempPath ;
                else _item.avatarUrl = "/asset/image/dock-avatar-default.png";
            }
            return _responseMembers;
        }

        function _getArrangedRecruiters(_memberArray){
            var _index = _memberArray.length;
            var _item;
            var _targetArray = [];
            while ( _index-- > 0 ) {
                _item = _memberArray[_index];
                if ( _item.status !== "enable" ) continue;
                if ( !(/^(admin|recruiter)$/.test(_item.role)) && !(_item.meeting_enable) ) continue;
                _targetArray.push(_item);
            }
            return _targetArray;
        }

    }

}(window.allot));
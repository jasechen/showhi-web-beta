(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __rootScope;
    var __api;
    var _listDictionary;
    var _fetchCallbackDisctionary;

    (function () {

        _friendFactory.$inject = ["$rootScope", "$api"];
        __ngApp.factory("$friend", _friendFactory);

    }());

    function _friendFactory(_rootScope, _api) {

        __rootScope = _rootScope;
        __api = _api;
        _defineVariable();
        return {
                readList: _processReadList,
                match: _processMatch,
                find: _processFind
            };

    }

    function _defineVariable() {

        _listDictionary = {};
        _fetchCallbackDisctionary = {};

    }

    function _processReadList(_id) {

        return _fetchList(_id);

    }

    function _processMatch(_userId, _friendId) {

        if ( !( _userId || _friendId ) ) return;
        if ( (_listDictionary[_userId]) ) return _findFriendInList();

        function _findFriendInList() {
            var _list = _listDictionary[_userId];
            var _index = _list.length;
            var _item;
            while ( _index-- > 0 ) {
                _item = _list[_index];
                if ( _item.friend_id === _friendId ) return true;
                if ( _item.user_id === _friendId ) return true;
            }
        }

    }

    function _processFind(_userId) {

        if ( (_listDictionary[_userId]) ) return _findFriendInList();

        function _findFriendInList() {
            var _list = _listDictionary[_userId];
            var _index = _list.length;
            var _item;
            while ( _index-- > 0 ) {
                _item = _list[_index];
                if ( _item.friend_id === _userId ) return _item;
                if ( _item.user_id === _userId ) return _item;
            }
        }

    }

    function _fetchList(_id) {

        _inspectCallbackArray();
        if ( !(_fetchCallbackDisctionary[_id].length) ) _processFetchList();
        return {
                then: _processThen(_id)
            };

        function _inspectCallbackArray() {
            _fetchCallbackDisctionary[_id] = _fetchCallbackDisctionary[_id] || [] ;
        }

        function _processFetchList() {
            _listDictionary[_id] = [];
            __api.friendList("enable", _id)
                .success(_onSuccess(_id))
                .finish(_onEnableFinish(_id));
        }

        function _onSuccess(_id) {
            return function (_response) {
                    _listDictionary[_id] = _listDictionary[_id].concat(_response);
                };
        }

        function _onEnableFinish(_id) {
            return function () {
                    __api.friendList("init", _id)
                        .success(_onSuccess(_id))
                        .finish(_onInitFinish(_id));
                };
        }

        function _onInitFinish(_id) {
            return function () {
                    var _fetchCallbackArray = _fetchCallbackDisctionary[_id];
                    var _callback;
                    while ( _fetchCallbackArray.length ) {
                        _callback = _fetchCallbackArray.shift();
                        if ( typeof(_callback) === "function" ) _callback(_listDictionary[_id]);
                    }
                    __rootScope.$apply();
                };
        }

        function _processThen(_id) {
            return function (_callback) {
                    var _fetchCallbackArray = _fetchCallbackDisctionary[_id];
                    _fetchCallbackArray.unshift(_callback);
                };
        }

    }

}(window.allot));
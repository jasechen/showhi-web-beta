(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __config = __allot("config");
    var __device;
    var __claim;
    var __api;
    var __storage;
    var _STORAGE_SESSION_KEY;
    var _STORAGE_DEVICECODE_KEY;

    (function () {

        _sessionFactory.$inject = ["$device", "$claim", "$api", "$storage"];
        __ngApp.factory("$session", _sessionFactory);

    }());

    function _sessionFactory(_device, _claim, _api, _storage) {

        __device = _device;
        __claim = _claim;
        __api = _api;
        __storage = _storage;
        _STORAGE_SESSION_KEY = __config.COOKIE_NAME + "-usersession" ;
        _STORAGE_DEVICECODE_KEY = __config.COOKIE_NAME + "-devicecode" ;
        return {
                read: _read,
                create: _create
            };

    }

    function _read(_callback, _initback, _outback, _errorback, _finishback) {

        var _session = __storage.getItem(_STORAGE_SESSION_KEY);
        var _deviceCode = __storage.getItem(_STORAGE_DEVICECODE_KEY);
        __claim.header.session = _session;
        if ( _deviceCode ) __device.code = _deviceCode;
        if ( _session ) __api.session(_session)
            .success(_onSuccess)
            .error(_onError)
            .finish(_onFinish);
        else if ( _initback ) _initback();

        function _onSuccess(_response) {
            if ( _response.status !== "login" ) {
                if ( _outback ) _outback(_session);
            } else if ( _callback ) _callback(_session);
        }

        function _onError(_response) {
            if ( _errorback ) _errorback(_response);
        }

        function _onFinish() {
            if ( _finishback ) _finishback();
        }

    }

    function _create(_callback, _errorback, _finishback) {

        var _deviceCode = __device.code;
        var _deviceOS = __device.os;
        var _deviceType = __device.type;
        var _deviceLanguage = __device.language;
        if ( _deviceLanguage === "en_us" ) _deviceLanguage = "en";
        else if ( _deviceLanguage === "zh_tw" ) _deviceLanguage = "zh-tw";
        else if ( _deviceLanguage === "zh_cn" ) _deviceLanguage = "zh-cn";
        __api.sessionInit(_deviceCode, _deviceOS, _deviceType, _deviceLanguage)
            .success(_onSuccess)
            .error(_onError)
            .mislay(_onError)
            .finish(_onFinish);

        function _onSuccess(_response) {
            var _session = _response.session;
            var _deviceCode = _response.device_code;
            __claim.header.session = _session;
            __storage.setItem(_STORAGE_SESSION_KEY, _session);
            __storage.setItem(_STORAGE_DEVICECODE_KEY, _deviceCode);
            if ( _callback ) _callback();
        }

        function _onError() {

            if ( _errorback ) _errorback();

        }

        function _onFinish() {

            if ( _finishback ) _finishback();

        }

    }

}(window.allot));
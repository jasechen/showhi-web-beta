(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __templateRequest;
    var __compile;
    var __TEMPLATE_PATH;
    var __FILE_CACHE_TIME;
    var _dialogueNav = document.getElementById("DialogueNav");

    (function () {

        _dialogueFactory.$inject = ["$rootScope", "$templateRequest", "$compile", "$TEMPLATE_PATH", "$FILE_CACHE_TIME"];
        __ngApp.factory("$dialogue", _dialogueFactory);

    }());

    function _dialogueFactory(_rootScope, _templateRequest, _compile, _TEMPLATE_PATH, _FILE_CACHE_TIME) {

        __scope = _rootScope.$new();
        __templateRequest = _templateRequest;
        __compile = _compile;
        __TEMPLATE_PATH = _TEMPLATE_PATH;
        __FILE_CACHE_TIME = _FILE_CACHE_TIME;

        return {
                login: _login,
                alert: _alert,
                confirm: _confirm,
                prompt: _prompt,
                lightbox: _lightbox,
                countdown: _countdown,
                cropper: _cropper,
                close: _closeDialogue
            };

    }

    function _login() {

        _readTemplate("login.html");

    }

    function _alert(_description, _confirmback, _callback, _errorback) {

        __scope.alertDescription = _description || "" ;
        __scope.onConfirm = _onConfirm;
        _readTemplate("alert.html", _onSuccess, _onError);

        function _onConfirm() {
            if ( _confirmback ) _confirmback();
            _closeDialogue();
        }

        function _onSuccess() {
            if ( _callback ) _callback();
        }

        function _onError() {
            if ( _errorback ) _errorback();
        }

    }

    function _confirm(_title, _description, _confirmback, _denyback, _callback, _errorback) {

        __scope.confirmTitle = _title || "" ;
        __scope.confirmDescription = _description || "" ;
        __scope.onConfirm = _onConfirm;
        __scope.onDeny = _onDeny;
        _readTemplate("confirm.html", _onSuccess, _onError);

        function _onConfirm() {
            if ( _confirmback ) _confirmback();
            _closeDialogue();
        }

        function _onDeny() {
            if ( _denyback ) _denyback();
            _closeDialogue();
        }

        function _onSuccess() {
            if ( _callback ) _callback();
        }

        function _onError() {
            if ( _errorback ) _errorback();
        }

    }

    function _prompt(_title, _description, _confirmback, _mislayback, _denyback, _callback, _errorback) {

        __scope.promptTitle = _title || "" ;
        __scope.promptDescription = _description || "" ;
        __scope.promptInput = "";
        __scope.onConfirm = _onConfirm;
        __scope.onDeny = _onDeny;
        _readTemplate("prompt.html", _onSuccess, _onError);

        function _onConfirm() {
            if ( __scope.promptInput ) {
                if ( _confirmback ) _confirmback(__scope.promptInput);
                _closeDialogue();
            } else if ( _mislayback ) _mislayback();
        }

        function _onDeny() {
            if ( _denyback ) _denyback();
            _closeDialogue();
        }

        function _onSuccess() {
            if ( _callback ) _callback();
        }

        function _onError() {
            if ( _errorback ) _errorback();
        }

    }

    function _lightbox(_imageUrl, _denyback, _callback, _errorback) {

        if ( !(_imageUrl) ) return;
        __scope.getLightboxImage = _getLightboxImage;
        __scope.onEixtClick = _closeDialogue;
        _readTemplate("lightbox.html", _onSuccess, _onError);

        function _onSuccess() {
            __scope.lightboxImage = _imageUrl;
            if ( _callback ) _callback();
        }

        function _onError() {
            if ( _errorback ) _errorback();
        }

    }

    function _getLightboxImage() {

        return {
                "background-image": "url(" + __scope.lightboxImage + ")"
            };

    }

    function _countdown(_description, _denyback, _callback, _errorback) {

        __scope.alertDescription = _description || "" ;
        __scope.onDeny = _onDeny;
        _readTemplate("countdown.html", _onSuccess, _onError);

        function _onDeny() {
            if ( _denyback ) _denyback();
            _closeDialogue();
        }

        function _onSuccess() {
            if ( _callback ) _callback();
        }

        function _onError() {
            if ( _errorback ) _errorback();
        }

    }

    function _cropper(_type, _callback) {

        __scope.cropperType = _type;
        __scope.cropperCallback = _callback;
        _readTemplate("cropper.html");

    }

    function _readTemplate(_templateFileName, _callback) {

        __templateRequest( __TEMPLATE_PATH + _templateFileName + __FILE_CACHE_TIME ).then(_onSuccess);

        function _onSuccess(_template) {
            _dialogueNav.innerHTML = _template;
            __compile(_dialogueNav)(__scope);
            if ( _callback ) _callback();
            _workDialogue();
        }

        function _workDialogue() {
            _dialogueNav.classList.add("work");
            document.documentElement.classList.add("freeze");
            _dialogueNav.addEventListener("mousedown", _onDialougueClick);
        }

        function _onDialougueClick(_event) {
            if ( _event.target !== _dialogueNav ) return;
            _closeDialogue();
            _dialogueNav.removeEventListener("mousedown", _onDialougueClick);
        }

    }

    function _closeDialogue() {

        _dialogueNav.classList.remove("work");
        document.documentElement.classList.remove("freeze");

    }

}(window.allot));
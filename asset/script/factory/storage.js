(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __util;
    var _onlyCookie = false;
    var _INVALID_EXPIRE = "=;expires=" + ( new Date(1000) ).toGMTString() ;

    (function () {

        _storageFactory.$inject = ["$util"];
        __ngApp.factory("$storage", _storageFactory);

    }());

    function _storageFactory(_util) {

        __util = _util;
        try {
            window.localStorage.setItem("TEST_STORAGE_PRIVACY", "TEST_STORAGE_PRIVACY");
            window.localStorage.removeItem("TEST_STORAGE_PRIVACY", "TEST_STORAGE_PRIVACY");
        } catch (_error) {
            _onlyCookie = true;
        }
        return {
                getItem: _getItem,
                setItem: _setItem,
                removeItem: _removeItem,
                clear: _clear
            };

    }

    function _getItem(_key) {

        var _value;
        _key = __util.aB(_key);
        if ( _onlyCookie ) {
            var _split = ( "; " + document.cookie ).split( "; " + _key + "=" );
            if ( _split.length === 2 ) _value = _split.pop().split(";").shift();
        } else _value = window.localStorage.getItem(_key);
        if ( _value ) return __util.bA(_value);

    }

    function _setItem(_key, _value) {

        _key = __util.aB(_key);
        _value = __util.aB(_value);
        if ( _onlyCookie ) {
            if ( _value === null || _value === undefined ) return;
            document.cookie = _key + "=" + _value ;
        } else window.localStorage.setItem(_key, _value);

    }

    function _removeItem(_key) {

        _key = __util.aB(_key);
        if ( _onlyCookie ) {
            document.cookie = _key + _INVALID_EXPIRE ;
        } else window.localStorage.removeItem(_key);

    }

    function _clear() {

        if ( _onlyCookie ) {
            var _cookieArray = Array.prototype.slice.call(( "; " + document.cookie ).split( "; " ), 1);
            while ( _cookieArray.length ) _removeItem(_cookieArray.shift().replace(/\=.*/, ""));
        } else window.localStorage.clear();

    }

}(window.allot));
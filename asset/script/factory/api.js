(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");

    (function () {

        _apiFactory.$inject = ["$claim", "$OPTIONAL_API_KEY"];
        __ngApp.factory("$api", _apiFactory);

    }());

    function _apiFactory(_claim, _OPTIONAL_API_KEY) {

        return {
                session: _session,
                sessionInit: _sessionInit,
                login: _login,
                logout: _logout,
                register: _register,
                user: _user,
                userToken: _userToken,
                userAvatar: _userAvatar,
                userProfile: _userProfile,
                userEmailNotify: _userEmailNotify,
                userEmailVerify: _userEmailVerify,
                userMobileNotify: _userMobileNotify,
                userMobileVerify: _userMobileVerify,
                userQrcodeVerify: _userQrcodeVerify,
                userEducation: _userEducation,
                userEducationCreate: _userEducationCreate,
                userEducationUpdate: _userEducationUpdate,
                blog: _blog,
                blogCover: _blogCover,
                boardGroup: _boardGroup,
                boardFetch: _boardFetch,
                boardBlog: _boardBlog,
                boardCreate: _boardCreate,
                boardGroupCreate: _boardGroupCreate,
                boardUpdate: _boardUpdate,
                boardGroupUpdate: _boardGroupUpdate,
                boardDelete: _boardDelete,
                boardGroupDelete: _boardGroupDelete,
                boardViewCount: _boardViewCount,
                boardGroupViewStatistic: _boardGroupViewStatistic,
                commentCreate: _commentCreate,
                commentListByText: _commentListByText,
                groups: _groups,
                groupsList: _groupsList,
                groupsByUser: _groupsByUser,
                groupsMyOwnList: _groupsMyOwnList,
                groupsMyJoinList: _groupsMyJoinList,
                groupsCreate: _groupsCreate,
                groupsUpdate: _groupsUpdate,
                groupsAvatar: _groupsAvatar,
                groupsCover: _groupsCover,
                groupSetWindower: _groupSetWindower,
                groupUsers: _groupUsers,
                groupUsersMembers: _groupUsersMembers,
                groupUsersJoin: _groupUsersJoin,
                groupUsersAllow: _groupUsersAllow,
                groupUsersSelfAllow: _groupUsersSelfAllow,
                groupUsersRole: _groupUsersRole,
                groupUsersDelete: _groupUsersDelete,
                groupUsersLeave: _groupUsersLeave,
                groupUsersMeetingEnable: _groupUsersMeetingEnable,
                groupUsersGetTimeSetting: _groupUsersGetTimeSetting,
                groupUsersTimeSetting: _groupUsersTimeSetting,
                groupUsersAvailableDates: _groupUsersAvailableDates,
                meeting: _meeting,
                meetingCreate: _meetingCreate,
                meetingAgree: _meetingAgree,
                meetingDelete: _meetingDelete,
                meetingCancel: _meetingCancel,
                meetingZoom: _meetingZoom,
                meetingTemporary: _meetingTemporary,
                friendList: _friendList,
                friendSelfList: _friendSelfList,
                friendCreate: _friendCreate,
                friendAgree: _friendAgree,
                friendDelete: _friendDelete,
                qrcodes: _qrcodes,
                qrcodesCheck: _qrcodesCheck,
                qrcodesList: _qrcodesList,
                qrcodesListOfGroup: _qrcodesListOfGroup,
                qrcodesCreate: _qrcodesCreate,
                qrcodesUpdatenote: _qrcodesUpdatenote,
                qrcodesDelete: _qrcodesDelete,
                noticeList: _noticeList,
                noticeRead: _noticeRead,
                fileCreate: _fileCreate,
                fileViewCount: _fileViewCount,
                fileGroupViewFileStatistic: _fileGroupViewFileStatistic,
                search: _search,
                localeCreate: _localeCreate,
                localeUpdate: _localeUpdate,
                localesLang: _localesLang,
                localeFetchAll: _localeFetchAll,
                localeJSON: _localeJSON,
                language: _language,
                country: _country,
                countryCallings: _countryCallings,
                interest: _interest
            };

        function _session() {
            return _claim.get("session/{code}", {
                    "{code}": arguments[0]
                });
        }

        function _sessionInit() {
            return _claim.post("session/init", {
                    device_code: arguments[0] || _OPTIONAL_API_KEY ,
                    device_os: arguments[1] || _OPTIONAL_API_KEY ,
                    device_type: arguments[2] || _OPTIONAL_API_KEY ,
                    device_lang: arguments[3] || _OPTIONAL_API_KEY
                });
        }

        function _login() {
            return _claim.post("login", {
                    account: arguments[0],
                    password: arguments[1],
                });
        }

        function _logout() {
            return _claim.post("logout", {
                });
        }

        function _register() {
            return _claim.post("register", {
                    account: arguments[0],
                    password: arguments[1],
                    password_repeat: arguments[2],
                    first_name: arguments[3],
                    last_name: arguments[4],
                    mobile_country_code: arguments[5],
                    mobile_phone: arguments[6],
                    qrcode_id: arguments[7],
                    introducer_id: arguments[8] || _OPTIONAL_API_KEY
                });
        }

        function _user() {
            return _claim.get("user/{id}", {
                    "{id}": arguments[0]
                });
        }

        function _userToken() {
            return _claim.get("user/token/{token}", {
                    "{token}": arguments[0]
                });
        }

        function _userAvatar() {
            return _claim.post("user/{id}/avatar", {
                    "{id}": arguments[0],
                    avatar: arguments[1],
                    _method: "PUT"
                });
        }

        function _userProfile() {
            return _claim.put("user/{id}/profile", {
                    "{id}": arguments[0],
                    first_name: arguments[1] || _OPTIONAL_API_KEY ,
                    last_name: arguments[2] || _OPTIONAL_API_KEY ,
                    nickname: arguments[3] || _OPTIONAL_API_KEY ,
                    gender: arguments[4] || _OPTIONAL_API_KEY ,
                    birth: arguments[5] || _OPTIONAL_API_KEY ,
                    city: arguments[6] || _OPTIONAL_API_KEY ,
                    intro: arguments[7] || _OPTIONAL_API_KEY ,
                    website: arguments[8] || _OPTIONAL_API_KEY ,
                    languages: arguments[9] || _OPTIONAL_API_KEY ,
                    interests: arguments[10] || _OPTIONAL_API_KEY ,
                    countries: arguments[11] || _OPTIONAL_API_KEY ,
                    pets: arguments[12] || _OPTIONAL_API_KEY
                });
        }

        function _userEmailNotify() {
            return _claim.get("user/{id}/email/notify", {
                    "{id}": arguments[0]
                });
        }

        function _userEmailVerify() {
            return _claim.put("user/{id}/email/verify", {
                    "{id}": arguments[0],
                    check_code: arguments[1]
                });
        }

        function _userMobileNotify() {
            return _claim.get("user/{id}/mobile/notify", {
                    "{id}": arguments[0]
                });
        }

        function _userMobileVerify() {
            return _claim.put("user/{id}/mobile/verify", {
                    "{id}": arguments[0],
                    check_code: arguments[1]
                });
        }

        function _userQrcodeVerify() {
            return _claim.put("user/{id}/qrcode/verify", {
                    "{id}": arguments[0],
                    user_id: arguments[1]
                });
        }

        function _userEducation() {
            return _claim.get("user/{id}/educations", {
                    "{id}": arguments[0]
                });
        }

        function _userEducationCreate() {
            return _claim.post("user/education", {
                    school: arguments[0],
                    type: arguments[1] || _OPTIONAL_API_KEY,
                    concentrations: arguments[2] || _OPTIONAL_API_KEY
                });
        }

        function _userEducationUpdate() {
            return _claim.put("user/education/{id}", {
                    "{id}": arguments[0],
                    school: arguments[1],
                    type: arguments[2] || _OPTIONAL_API_KEY,
                    concentrations: arguments[3] || _OPTIONAL_API_KEY
                });
        }

        function _blog() {
            return _claim.get("blog/{id}", {
                    "{id}": arguments[0]
                });
        }

        function _blogCover() {
            return _claim.post("blog/{id}/cover", {
                    "{id}": arguments[0],
                    cover: arguments[1],
                    _method: "PUT"
                });
        }

        function _boardBlog() {
            return _claim.get("board/blog/{id}/{page}", {
                    "{id}": arguments[0],
                    "{page}": arguments[1]
                });
        }

        function _boardGroup() {
            return _claim.get("board/group/{id}/{page}", {
                    "{id}": arguments[0],
                    "{page}": arguments[1]
                });
        }

        function _boardFetch() {
            return _claim.get("board/{id}", {
                    "{id}": arguments[0]
                });
        }

        function _boardCreate() {
            return _claim.post("board", {
                    parent_id: arguments[0],
                    parent_type: arguments[1] || _OPTIONAL_API_KEY,
                    content: arguments[2] || _OPTIONAL_API_KEY,
                    privacy_status: arguments[3] || _OPTIONAL_API_KEY,
                    status: arguments[4] || _OPTIONAL_API_KEY,
                    file_ids: arguments[5] || _OPTIONAL_API_KEY,
                });
        }

        function _boardGroupCreate() {
            return _claim.post("groups/board", {
                    parent_id: arguments[0],
                    parent_type: arguments[1] || _OPTIONAL_API_KEY,
                    content: arguments[2] || _OPTIONAL_API_KEY,
                    privacy_status: arguments[3] || _OPTIONAL_API_KEY,
                    status: arguments[4] || _OPTIONAL_API_KEY,
                    file_ids: arguments[5] || _OPTIONAL_API_KEY,
                });
        }

        function _boardUpdate() {
            return _claim.put("board/{id}", {
                    "{id}": arguments[0],
                    content: arguments[1] || _OPTIONAL_API_KEY,
                    text_id: arguments[2] || _OPTIONAL_API_KEY,
                    privacy_status: arguments[3] || _OPTIONAL_API_KEY,
                    status: arguments[4] || _OPTIONAL_API_KEY,
                    file_ids: arguments[5] || _OPTIONAL_API_KEY,
                });
        }

        function _boardGroupUpdate() {
            return _claim.put("groups/{group_id}/board/{id}", {
                    "{id}": arguments[0],
                    content: arguments[1] || _OPTIONAL_API_KEY,
                    text_id: arguments[2] || _OPTIONAL_API_KEY,
                    privacy_status: arguments[3] || _OPTIONAL_API_KEY,
                    status: arguments[4] || _OPTIONAL_API_KEY,
                    file_ids: arguments[5] || _OPTIONAL_API_KEY,
                    "{group_id}": arguments[6] || _OPTIONAL_API_KEY
                });
        }

        function _boardDelete() {
            return _claim.delete("board/{id}", {
                    "{id}": arguments[0]
                });
        }

        function _boardGroupDelete() {
            return _claim.delete("groups/{group_id}/board/{id}", {
                    "{id}": arguments[0],
                    "{group_id}": arguments[1]
                });
        }

        function _boardViewCount() {
            return _claim.put("board/{id}/view/count", {
                    "{id}": arguments[0]
                });
        }

        function _boardGroupViewStatistic() {
            return _claim.get("board/group/{groupId}/statistic/view/{page}", {
                    "{groupId}": arguments[0],
                    "{page}": arguments[1]
                });
        }

        function _commentCreate() {
            return _claim.post("comment", {
                    content: arguments[0],
                    parent_id: arguments[1],
                    parent_type: arguments[2] || _OPTIONAL_API_KEY,
                    file_ids: arguments[3] || _OPTIONAL_API_KEY
                });
        }

        function _commentListByText() {
            return _claim.get("comment/text/{id}", {
                    "{id}": arguments[0]
                });
        }

        function _groups() {
            return _claim.get("groups/{id}", {
                    "{id}": arguments[0]
                });
        }

        function _groupsList() {
            return _claim.get("groups", {
                });
        }

        function _groupsByUser() {
            return _claim.get("groups/{id}/getByUserId", {
                    "{id}": arguments[0]
                });
        }

        function _groupsMyOwnList() {
            return _claim.get("groups/myOwnList", {
                });
        }

        function _groupsMyJoinList() {
            return _claim.get("groups/myAddList", {
                });
        }

        function _groupsCreate() {
            return _claim.post("groups", {
                    type: arguments[0],
                    title: arguments[1],
                    tag: arguments[2],
                    description: arguments[3] || _OPTIONAL_API_KEY
                });
        }

        function _groupsUpdate() {
            return _claim.put("groups/{id}", {
                    "{id}": arguments[0],
                    title: arguments[1] || _OPTIONAL_API_KEY,
                    description: arguments[2] || _OPTIONAL_API_KEY,
                    status: arguments[3] || _OPTIONAL_API_KEY,
                    privacy_status: arguments[4] || _OPTIONAL_API_KEY,
                    type: arguments[5] || _OPTIONAL_API_KEY,
                    owner_id: arguments[6] || _OPTIONAL_API_KEY
                });
        }

        function _groupsAvatar() {
            return _claim.post("groups/{id}/avatar", {
                    "{id}": arguments[0],
                    avatar: arguments[1],
                    _method: "PUT",
                });
        }

        function _groupsCover() {
            return _claim.post("groups/{id}/cover", {
                    "{id}": arguments[0],
                    cover: arguments[1],
                    _method: "PUT",
                });
        }

        function _groupSetWindower() {
            return _claim.put("groups/{id}/setOwnerWindower", {
                    "{id}": arguments[0],
                    windower: arguments[1]
                });
        }

        function _groupUsers() {
            return _claim.post("groupUsers", {
                    groupId: arguments[0],
                    account: arguments[1]
                });
        }

        function _groupUsersMembers() {
            return _claim.get("groupUsers/{id}/list", {
                    "{id}": arguments[0]
                });
        }

        function _groupUsersJoin() {
            return _claim.post("groupUsers/join", {
                    groupId: arguments[0]
                });
        }

        function _groupUsersAllow() {
            return _claim.put("groupUsers/{groupId}/allow/{id}", {
                    "{groupId}": arguments[0],
                    "{id}": arguments[1],
                    status: arguments[2]
                });
        }

        function _groupUsersSelfAllow() {
            return _claim.put("groupUsers/{groupId}/selfAgree/{id}", {
                    "{groupId}": arguments[0],
                    "{id}": arguments[1],
                    status: arguments[2]
                });
        }

        function _groupUsersRole() {
            return _claim.put("groupUsers/{groupId}/role/{id}", {
                    "{groupId}": arguments[0],
                    "{id}": arguments[1],
                    role: arguments[2]
                });
        }

        function _groupUsersDelete() {
            return _claim.delete("groupUsers/{groupId}/delete/{id}", {
                    "{groupId}": arguments[0],
                    "{id}": arguments[1]
                });
        }

        function _groupUsersLeave() {
            return _claim.delete("groupUsers/{groupId}/selfDelete/{id}", {
                    "{groupId}": arguments[0],
                    "{id}": arguments[1]
                });
        }

        function _groupUsersMeetingEnable() {
            return _claim.put("groupUsers/{groupId}/meetingEnable/{id}", {
                    "{groupId}": arguments[0],
                    "{id}": arguments[1],
                    meeting_enable: arguments[2]
                });
        }

        function _groupUsersGetTimeSetting() {
            return _claim.get("groupUsers/{groupId}/getTimeSetting", {
                    "{groupId}": arguments[0]
                });
        }

        function _groupUsersTimeSetting() {
            return _claim.put("groupUsers/{groupId}/timeSetting", {
                    "{groupId}": arguments[0],
                    start_date: arguments[1],
                    end_date: arguments[2],
                    sun: arguments[3],
                    mon: arguments[4],
                    tue: arguments[5],
                    wed: arguments[6],
                    thu: arguments[7],
                    fri: arguments[8],
                    sat: arguments[9],
                });
        }

        function _groupUsersAvailableDates() {
            return _claim.get("groupUsers/{groupId}/available/{userId}/day/{day}", {
                    "{groupId}": arguments[0],
                    "{userId}": arguments[1],
                    "{day}": arguments[2]
                });
        }

        function _meeting() {
            return _claim.get("meetings/{id}", {
                    "{id}": arguments[0]
                });
        }

        function _meetingCreate() {
            return _claim.post("meetings", {
                    group_id: arguments[0],
                    user_id: arguments[1],
                    meeting_date: arguments[2],
                    meeting_time: arguments[3],
                    meeting_summary: arguments[4]
                });
        }

        function _meetingAgree() {
            return _claim.put("meetings/{id}/agree", {
                    "{id}": arguments[0],
                    meeting_date: arguments[1],
                    meeting_time: arguments[2],
                    meeting_summary: arguments[3]
                });
        }

        function _meetingDelete() {
            return _claim.delete("meetings/{id}", {
                    "{id}": arguments[0],
                    cancel_note: arguments[1],
                    meeting_date: arguments[2],
                    meeting_time: arguments[3],
                    meeting_summary: arguments[4]
                });
        }

        function _meetingCancel() {
            return _claim.delete("meetings/{id}/cancelDelete", {
                    "{id}": arguments[0],
                    cancel_note: arguments[1],
                    meeting_date: arguments[2],
                    meeting_time: arguments[3],
                    meeting_summary: arguments[4]
                });
        }

        function _meetingZoom() {
            return _claim.post("meetings/zoomCreate", {
                    meeting_id: arguments[0],
                    user_id: arguments[1]
                });
        }

        function _meetingTemporary() {
            return _claim.post("meetings/tmpZoomCreate", {
                    group_id: arguments[0],
                    user_id: arguments[1]
                });
        }

        function _friendList() {
            return _claim.get("userFriends/{status}/getByUserId/{userId}", {
                    "{status}": arguments[0],
                    "{userId}": arguments[1]
                });
        }

        function _friendSelfList() {
            return _claim.get("userFriends/{status}/list", {
                    "{status}": arguments[0]
                });
        }

        function _friendCreate() {
            return _claim.post("userFriends", {
                    friend_id: arguments[0]
                });
        }

        function _friendAgree() {
            return _claim.put("userFriends/{id}/agreeFriends", {
                    "{id}": arguments[0],
                    status: arguments[1]
                });
        }

        function _friendDelete() {
            return _claim.delete("userFriends/{id}", {
                    "{id}": arguments[0]
                });
        }

        function _qrcodes() {
            return _claim.get("qrcodes/{id}", {
                    "{id}": arguments[0]
                });
        }

        function _qrcodesCheck() {
            return _claim.get("qrcodes/{id}/check", {
                    "{id}": arguments[0]
                });
        }

        function _qrcodesList() {
            return _claim.get("qrcodes", {
                });
        }

        function _qrcodesListOfGroup() {
            return _claim.get("qrcodes/{groupId}/groupList", {
                    "{groupId}": arguments[0]
                });
        }

        function _qrcodesCreate() {
            return _claim.post("qrcodes", {
                    type: arguments[0],
                    parentId: arguments[1] || _OPTIONAL_API_KEY ,
                    parentType: arguments[2] || _OPTIONAL_API_KEY
                });
        }

        function _qrcodesUpdatenote() {
            return _claim.put("qrcodes/{id}/updatenote", {
                    "{id}": arguments[0],
                    note: arguments[1]
                });
        }

        function _qrcodesDelete() {
            return _claim.delete("qrcodes/{id}", {
                    "{id}": arguments[0]
                });
        }

        function _noticeList() {
            return _claim.get("notices/{parentType}/{status}/list", {
                    "{parentType}": arguments[0],
                    "{status}": arguments[1]
                });
        }

        function _noticeRead() {
            return _claim.put("notices/{id}/readed", {
                    "{id}": arguments[0]
                });
        }

        function _fileCreate() {
            return _claim.post("file", {
                    file: arguments[0],
                    grand_id: arguments[1],
                    grand_type: arguments[2] || _OPTIONAL_API_KEY,
                    parent_id: arguments[3] || _OPTIONAL_API_KEY,
                    parent_type: arguments[4] || _OPTIONAL_API_KEY,
                    privacy_status: arguments[5] || _OPTIONAL_API_KEY,
                    status: arguments[6] || _OPTIONAL_API_KEY
                });
        }

        function _fileViewCount() {
            return _claim.put("file/{id}/view/count", {
                    "{id}": arguments[0]
                });
        }

        function _fileGroupViewFileStatistic() {
            return _claim.get("file/group/{groupId}/statistic/{type}/view/{page}", {
                    "{groupId}": arguments[0],
                    "{type}": arguments[1],
                    "{page}": arguments[2]
                });
        }

        function _search() {
            return _claim.get("search/{term}", {
                    "{term}": arguments[0]
                });
        }

        function _localeCreate() {
            return _claim.post("locales", {
                    code: arguments[0],
                    lang_en: arguments[1],
                    content_en: arguments[2],
                    lang_tw: arguments[3],
                    content_tw: arguments[4],
                    lang_cn: arguments[5],
                    content_cn: arguments[6],
                    note: arguments[7] || _OPTIONAL_API_KEY
                });
        }

        function _localeUpdate() {
            return _claim.put("locales/{id}", {
                    "{id}": arguments[0],
                    code: arguments[1],
                    lang_en: arguments[2],
                    content_en: arguments[3],
                    lang_tw: arguments[4],
                    content_tw: arguments[5],
                    lang_cn: arguments[6],
                    content_cn: arguments[7],
                    note: arguments[8] || _OPTIONAL_API_KEY
                });
        }

        function _localesLang() {
            return _claim.get("locales/lang/{lang}", {
                    "{lang}": arguments[0]
                });
        }

        function _localeFetchAll() {
            return _claim.get("locales/all", {
                });
        }

        function _localeJSON() {
            return _claim.get("asset/json/locales_{lang}.json", {
                    "{lang}": arguments[0]
                });
        }

        function _language() {
            return _claim.get("language/{lang}", {
                    "{lang}": arguments[0]
                });
        }

        function _country() {
            return _claim.get("country/{lang}", {
                    "{lang}": arguments[0]
                });
        }

        function _countryCallings() {
            return _claim.get("country/callings/{lang}", {
                    "{lang}": arguments[0]
                });
        }

        function _interest() {
            return _claim.get("interest/{lang}", {
                    "{lang}": arguments[0]
                });
        }

    }

}(window.allot));
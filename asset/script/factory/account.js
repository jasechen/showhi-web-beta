(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __config = __allot("config");
    var __rootScope;
    var __location;
    var __path;
    var __claim;
    var __api;
    var __storage;
    var _token;
    var _datum;
    var _isUserReading;
    var _callbackReadArray;
    var _callbackStaticReadArray;
    var _STORAGE_TOKEN_KEY;

    (function () {

        _accountFactory.$inject = ["$rootScope", "$location", "$path", "$claim", "$api", "$storage"];
        __ngApp.factory("$account", _accountFactory);

    }());

    function _accountFactory(_rootScope, _location, _path, _claim, _api, _storage) {

        __rootScope = _rootScope;
        __location = _location;
        __path = _path;
        __claim = _claim;
        __api = _api;
        __storage = _storage;
        _defineVariable();
        return {
                read: _read,
                addCallback: _addCallback,
                login: _login,
                logout: _logout,
                getToken: _getToken,
                setToken: _setToken,
                confirmToken: _confirmToken
            };

    }

    function _defineVariable() {

        _STORAGE_TOKEN_KEY = __config.COOKIE_NAME + "-token" ;
        _isUserReading = false;
        _token = __claim.header.token = __storage.getItem(_STORAGE_TOKEN_KEY);
        _callbackReadArray = [];
        _callbackStaticReadArray = [];

    }

    function _read(_callback) {

        if ( _callback ) {
            if ( _datum ) _callback(_datum);
            _callbackReadArray.push(_callback);
        }
        if ( _isUserReading ) return;
        _isUserReading = true;
        _fetchAccount();

    }

    function _addCallback(_function) {

        _callbackStaticReadArray.push(_function);

    }

    function _login(_account, _password, _callback, _errorback, _finishback) {

        __api.login(_account, _password)
            .success(_onSuccess)
            .error(_onError)
            .mislay(_onError)
            .finish(_onFinish);

        function _onSuccess(_response) {
            _token = __claim.header.token = _response.token;
            __storage.setItem(_STORAGE_TOKEN_KEY, _token);
            if ( _callback ) _callback();
            __location.path("/");
            __rootScope.$apply();
        }

        function _onError(_response) {
            var _responseToken;
            if ( ( _response.comment || "" ).indexOf("user status") >= 0 ) {
                _responseToken = ( _response.data || {} ).token;
                if ( _responseToken ) {
                    _token = _responseToken;
                    return _fetchAccount(_onRead);
                }
            }
            if ( _errorback ) _errorback(_response);
        }

        function _onRead(_response) {
            var _responseDatum = _response.user;
            if ( _callback ) _callback();
            __location.path( "/verify/" + _responseDatum.id );
            __rootScope.$apply();
        }

        function _onFinish() {
            if ( _finishback ) _finishback();
        }

    }

    function _logout() {

        __api.logout()
            .success(_onSuccess);

        function _onSuccess() {
            __rootScope.userDatum = _datum = null;
            _token = null;
            delete __claim.header.token;
            __storage.removeItem(_STORAGE_TOKEN_KEY);
            __rootScope.isLogined = false;
            __location.path("/home");
            __rootScope.$apply();
        }

    }

    function _getToken() {

        return _token;

    }

    function _setToken(_tokenValue) {

        if ( _tokenValue ) _token = _tokenValue;

    }

    function _confirmToken() {

        return !!_token ;

    }

    function _fetchAccount(_callback) {

        __api.userToken(_token)
           .success(_onSuccess)
           .error(_onError)
           .mislay(_onMislay)
           .finish(_onFinish);

        function _onSuccess(_response) {
            _datum = _response;
            _processCallbackRead();
            if ( _callback ) _callback(_datum);
        }

        function _onError() {

        }

        function _onMislay() {
            _isUserReading = false;
        }

        function _onFinish() {
            _isUserReading = false;
            __rootScope.$apply();
        }

    }

    function _processCallbackRead() {

        var _index = _callbackStaticReadArray.length;
        var _function;
        while ( _index-- > 0 ) {
            _function = _callbackStaticReadArray[_index];
            if ( _function ) _function(_datum);
        }
        while ( _callbackReadArray.length ) {
            _function = _callbackReadArray.shift();
            if ( _function ) _function(_datum);
        }

    }

}(window.allot));
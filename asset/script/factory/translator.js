(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __config = __allot("config");
    var __rootScope;
    var __device;
    var __claim;
    var __api;
    var __storage;
    var __FILE_CACHE_TIME;
    var _language;
    var _ZH_TW = "zh_tw";
    var _ZH_CN = "zh_cn";
    var _EN_US = "en_us";
    var _LANGUAGE_DICT;
    var _JSON_LANGUAGE_DICT;
    var _STORAGE_KEY;

    (function () {

        _translatorFactory.$inject = ["$rootScope", "$device", "$claim", "$api", "$storage", "$FILE_CACHE_TIME"];
        __ngApp.factory("$translator", _translatorFactory);

    }());

    function _translatorFactory(_rootScope, _device, _claim, _api, _storage, _FILE_CACHE_TIME) {

        __rootScope = _rootScope;
        __device = _device;
        __claim = _claim;
        __api = _api;
        __storage = _storage;
        __FILE_CACHE_TIME = _FILE_CACHE_TIME;
        _STORAGE_KEY = __config.COOKIE_NAME + "-language" ;
        __rootScope.lyric = {};
        _defineLanguageDict();
        _defineLanguage();
        return {
                read: _read,
                set: _setTranslator,
                languageDict: _LANGUAGE_DICT,
                getLanguageTerm: _getLanguageTerm
            };

    }

    function _defineLanguageDict() {

        _LANGUAGE_DICT = {};
        _LANGUAGE_DICT[_EN_US] = "English";
        _LANGUAGE_DICT[_ZH_TW] = "中文(繁)";
        _LANGUAGE_DICT[_ZH_CN] = "中文(简)";
        _JSON_LANGUAGE_DICT = {};
        _JSON_LANGUAGE_DICT[_EN_US] = "en";
        _JSON_LANGUAGE_DICT[_ZH_TW] = "tw";
        _JSON_LANGUAGE_DICT[_ZH_CN] = "cn";

    }

    function _defineLanguage() {

        var _currentLanguage = __storage.getItem(_STORAGE_KEY);
        _language = _EN_US;
        if ( _currentLanguage ) _updateScopeLanguage(_currentLanguage);
        else {
            _language = __device.language;
            _updateStorageLanguage();
        }

    }

    function _read() {

        __api.localesLang(_getLanguageTerm(_language))
            .success(_onSuccess);

        function _onSuccess(_response) {
            var _responseLocales = _response.locales;
            var _languageDictionary = {};
            var _item;
            while ( _responseLocales.length ) {
                _item = _responseLocales.shift();
                if ( /^country\_name\_/.test(_item.code) ) continue;
                _languageDictionary[_item.code] = _item.contents[0].content;
            }
            __rootScope.languageDictionary = _languageDictionary;
            __rootScope.$apply();
        }

    }

    function _setTranslator(_targetLanguage, _callback, _errorback) {

        var _languageCode;
        _updateScopeLanguage(_targetLanguage);
        if ( _language === _ZH_TW ) _languageCode = _ZH_TW;
        else if ( _language === _ZH_CN ) _languageCode = _ZH_CN;
        else if ( _language === _EN_US ) _languageCode = _EN_US;
        if ( !_languageCode ) return;
        _updateStorageLanguage();
        __api.localeJSON(_JSON_LANGUAGE_DICT[_languageCode])
            .success(_onSuccess)
            .error(_onError);

        function _onSuccess(_response) {
            __rootScope.lyric = _response;
            if ( _callback ) _callback(_response);
            __rootScope.$apply();
        }

        function _onError() {
            if ( _errorback ) _errorback();
        }

    }

    function _getLanguageTerm(_language) {

        if ( _language === _ZH_TW ) return "tw";
        else if ( _language === _ZH_CN ) return "cn";
        else if ( _language === _EN_US ) return "en";

    }

    function _updateScopeLanguage(_targetLanguage) {

        if ( _targetLanguage ) _language = _targetLanguage;
        __rootScope.language = _language;

    }

    function _updateStorageLanguage() {

        __storage.setItem(_STORAGE_KEY, _language);

    }

}(window.allot));
(function (__allot) {

    "use strict";

    var _ngApp = __allot("ng");
    var __api;
    var __dialogue;
    var __scope;
    var _imageInp;
    var _cropContrainer;
    var _frameElement;
    var _imgElement;
    var _originalScale;
    var _notRatio;
    var _frameDatum;

    (function () {

        _cropperDirective.$inject = ["$api", "$dialogue"];
        _ngApp.directive("cropper", _cropperDirective);

    }());

    function _cropperDirective(_api, _dialogue) {

        __api = _api;
        __dialogue = _dialogue;
        return {
                restrict: "C",
                scope: false,
                link: _cropperController
            };

    }

    function _cropperController(_scope) {

        __scope = _scope;
        _defineVars();
        _defineEvents();

    }

    function _defineVars() {

        _imageInp = document.getElementById("ImageInp");
        _cropContrainer = document.getElementById("CropContainer");
        _frameElement = document.getElementById("CropFrame");
        _imgElement = document.getElementById("BaseImg");
        _originalScale = 1;
        _notRatio = false;
        _frameDatum = {};
        __scope.cropperType = __scope.cropperType || "avatar" ;
        if ( __scope.cropperType === "cover" ) {
            __scope.width = 915;
            __scope.height = 336;
        } else {
            __scope.width = 300;
            __scope.height = 300;
        }
        __scope.ratio = __scope.width / __scope.height ;

    }

    function _defineEvents() {

        _imageInp.addEventListener("change", _onInput);
        _frameElement.addEventListener("mousedown", _onFrameEvent);
        Array.prototype.slice.call(document.getElementsByClassName("crop-frontier")).forEach(_setFrontierEvent);
        __scope.onFileClick = _onFileClick;
        __scope.onCancelClick = _onCancelClick;
        __scope.onUploadClick = _onUploadClick;

    }

    function _onFileClick() {

        var _uploadInput = document.querySelector("nav#DialogueNav section.cropper input[type=file]");
        _uploadInput.click();

    }

    function _onCancelClick() {

        __dialogue.close();

    }

    function _onUploadClick() {

        var _dataForCropper = {
                left: _frameDatum.left,
                top: _frameDatum.top,
                right: _frameDatum.right,
                bottom: _frameDatum.bottom,
                width: __scope.width,
                height: __scope.height,
                scale: _originalScale
            };
        _cropClass(_imageInp.files[0], _dataForCropper, _onCropSuccess);

        function _onCropSuccess(_croppedCan) {
            var _dataBlob = _dataURItoBlob(_croppedCan.toDataURL());
            if ( __scope.cropperCallback ) __scope.cropperCallback(_dataBlob);
        }

        function _dataURItoBlob(_dataURI) {
            var _byteString;
            if ( _dataURI.split(",")[0].indexOf("base64") >= 0 ) _byteString = window.atob(_dataURI.split(",")[1]);
            else _byteString = window.unescape(_dataURI.split(",")[1]);
            var mimeString = _dataURI.split(",")[0].split(":")[1].split(";")[0];
            var _unitArray = new Uint8Array(_byteString.length);
            var _index = 0;
            while ( _index < _byteString.length ) _unitArray[_index] = _byteString.charCodeAt( _index++ );
            return new Blob([_unitArray], {
                    type: mimeString
                });
        }

    }

    function _onInput() {

        var _reader = new FileReader();
        _reader.onload = _imageIsLoaded;
        _reader.readAsDataURL(_imageInp.files[0]);

        function _imageIsLoaded(_event) {
            _imgElement.onload = _imgLoaded;
            _imgElement.src = URL.createObjectURL(_imageInp.files[0]);
            _cropContrainer.style.backgroundImage = "url(" + _event.target.result + ")";
            _cropContrainer.classList.add("work");
        }

        function _imgLoaded() {

            var _isWidther = ( _imgElement.width > _imgElement.height ) && ( _imgElement.width > _imgElement.height * __scope.ratio ) ;
            var _tempOffset;
            if ( _isWidther ) _tempOffset = Math.floor( Math.abs( ( _imgElement.width - ( _imgElement.height * __scope.ratio ) ) * 0.5 ) );
            else _tempOffset = Math.floor( Math.abs( ( _imgElement.height - ( _imgElement.width / __scope.ratio ) ) * 0.5 ) );
            _originalScale = _imgElement.height / _imgElement.naturalHeight ;
            if ( _isWidther ) {
                _frameDatum.top = 0;
                _frameDatum.bottom = _imgElement.height;
                _frameDatum.left = _tempOffset;
                _frameDatum.right = _imgElement.width - _tempOffset ;
            } else {
                _frameDatum.top = _tempOffset;
                _frameDatum.bottom = _imgElement.height - _tempOffset ;
                _frameDatum.left = 0;
                _frameDatum.right = _imgElement.width;
            }
            _updateFrame();
        }

    }

    function _getAxis(_mouseX, _mouseY) {

        var _frameWidth = _frameDatum.right - _frameDatum.left ;
        var _frameHeight = _frameDatum.bottom - _frameDatum.top ;
        var _axis = {
                x: _frameWidth * 0.5 + _frameDatum.left ,
                y: _frameHeight * 0.5 + _frameDatum.top
            };
        _axis.originX = _axis.x;
        _axis.originY = _axis.y;
        _axis.offsetX = _mouseX - _axis.x ;
        _axis.offsetY = _mouseY - _axis.y ;
        return _axis;

    }

    function _onFrameEvent(_event) {

        if ( _event.target !== _frameElement ) return;
        var _originMouseX = _event.clientX || _event.pageX ;
        var _originMouseY = _event.clientY || _event.pageY ;
        var _frameWidth = _frameDatum.right - _frameDatum.left ;
        var _frameHeight = _frameDatum.bottom - _frameDatum.top ;
        var _axis = _getAxis(_originMouseX, _originMouseY);
        var _mouseX;
        var _mouseY;
        document.addEventListener("mousemove", _onMove);
        document.addEventListener("mouseup", _onUp);

        function _onMove(_event) {
            _mouseX = ( _event.clientX || _event.pageX ) - _axis.offsetX ;
            _mouseY = ( _event.clientY || _event.pageY ) - _axis.offsetY ;
            _axis.x = _mouseX - _axis.originX + _axis.x ;
            _axis.y = _mouseY - _axis.originY + _axis.y ;
            if ( _axis.x - _frameWidth * 0.5 < 0 ) _axis.x = _frameWidth * 0.5 ;
            if ( _axis.x + _frameWidth * 0.5 > _imgElement.offsetWidth ) _axis.x = _imgElement.offsetWidth - _frameWidth * 0.5 ;
            if ( _axis.y - _frameHeight * 0.5 < 0 ) _axis.y = _frameHeight * 0.5 ;
            if ( _axis.y + _frameHeight * 0.5 > _imgElement.offsetHeight ) _axis.y = _imgElement.offsetHeight - _frameHeight * 0.5 ;
            _frameDatum.top = _axis.y - _frameHeight * 0.5 ;
            _frameDatum.bottom = _axis.y + _frameHeight * 0.5 ;
            _frameDatum.left = _axis.x - _frameWidth * 0.5 ;
            _frameDatum.right = _axis.x + _frameWidth * 0.5 ;
            _axis.originX = _axis.x;
            _axis.originY = _axis.y;
            _updateFrame();
        }

        function _onUp() {
            document.removeEventListener("mousemove", _onMove);
            document.removeEventListener("mouseup", _onUp);
        }

    }

    function _setFrontierEvent(_tmpFrontier) {

        _tmpFrontier.addEventListener("mousedown", _onFrontier);

    }

    function _onFrontier(_event) {

        var _frontier = _event.currentTarget || _event.target ;
        var _axial = _frontier.id.replace("Crop", "").toLowerCase();
        var _mouseX;
        var _mouseY;
        var _axis;
        document.addEventListener("mousemove", _onMove);
        document.addEventListener("mouseup", _onUp);

        function _onMove(_event) {
            _mouseX = ( _event.clientX || _event.pageX ) - _cropContrainer.offsetLeft ;
            _mouseY = ( _event.clientY || _event.pageY ) - _cropContrainer.offsetTop ;
            _axis = _getAxis(_mouseX, _mouseY);
            if ( _axial.indexOf("top") >= 0 || _axial.indexOf("bottom") >= 0 ) {
                if ( _axis.offsetY < 0 ) _axis.offsetY = _axis.offsetY * -1 ;
                if ( _axis.y - _axis.offsetY < 0 ) _axis.offsetY = _axis.y;
                if ( _axis.y + _axis.offsetY > _imgElement.height ) _axis.offsetY = _imgElement.height - _axis.y ;
                _axis.offsetX = _axis.offsetY * __scope.ratio ;
            }
            if ( _axial.indexOf("left") >= 0 || _axial.indexOf("right") >= 0 ) {
                if ( _axis.offsetX < 0 ) _axis.offsetX = _axis.offsetX * -1 ;
                if ( _axis.x - _axis.offsetX < 0 ) _axis.offsetX = _axis.x;
                if ( _axis.x + _axis.offsetX > _imgElement.width ) _axis.offsetX = _imgElement.width - _axis.x ;
                _axis.offsetY = _axis.offsetX / __scope.ratio ;
            }
            _frameDatum.top = _axis.y - _axis.offsetY ;
            _frameDatum.bottom = _axis.y + _axis.offsetY ;
            _frameDatum.left = _axis.x - _axis.offsetX ;
            _frameDatum.right = _axis.x + _axis.offsetX ;
            _updateFrame();
        }

        function _onUp() {
            document.removeEventListener("mousemove", _onMove);
            document.removeEventListener("mouseup", _onUp);
        }

    }

    function _updateFrame() {

        var _frameStyle = document.getElementById("CropFrame").style;
        _frameStyle.top = _frameDatum.top + "px" ;
        _frameStyle.height = ( _frameDatum.bottom - _frameDatum.top ) + "px" ;
        _frameStyle.left = _frameDatum.left + "px" ;
        _frameStyle.width = ( _frameDatum.right - _frameDatum.left ) + "px" ;

    }

    function _cropClass(_imageFile, _sizeDatum, _callback) {

        var _canvas = document.createElement("canvas");
        var _context = _canvas.getContext("2d");
        var _imgEmt = new Image();
        _sizeDatum.scale = _sizeDatum.scale || 1 ;
        _sizeDatum.left = ( _sizeDatum.left || 0 ) / _sizeDatum.scale ;
        _sizeDatum.top = ( _sizeDatum.top || 0 ) / _sizeDatum.scale ;
        _imgEmt.addEventListener("load", _onImgLoaded);
        _imgEmt.src = URL.createObjectURL(_imageFile);

        function _onImgLoaded() {
            _sizeDatum.right = ( _sizeDatum.right || _imgEmt.width ) / _sizeDatum.scale ;
            _sizeDatum.bottom = ( _sizeDatum.bottom || _imgEmt.height ) / _sizeDatum.scale ;
            _sizeDatum.width = _sizeDatum.width || _imgEmt.width ;
            _sizeDatum.height = _sizeDatum.height || _imgEmt.height ;
            var _clipWidth = _sizeDatum.right - _sizeDatum.left ;
            var _clipHeight = _sizeDatum.bottom - _sizeDatum.top ;
            _canvas.width = _sizeDatum.width;
            _canvas.height = _sizeDatum.height;
            _context.fillStyle = "white";
            _context.fillRect(0, 0, _canvas.width, _canvas.height);
            _context.drawImage(_imgEmt, _sizeDatum.left, _sizeDatum.top, _clipWidth, _clipHeight, 0, 0, _sizeDatum.width, _sizeDatum.height);
            if ( _callback ) _callback(_canvas);
        }

    }

}(window.allot));
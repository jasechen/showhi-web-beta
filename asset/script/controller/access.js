(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __routeParams;
    var __path;
    var __util;
    var __api;
    var __group;
    var __dialogue;
    var _groupId;

    (function () {

        _accessController.$inject = ["$scope", "$routeParams", "$path", "$util", "$api", "$group", "$dialogue"];
        __ngApp.controller("AccessController", _accessController);

    }());

    function _accessController(_scope, _routeParams, _path, _util, _api, _group, _dialogue) {

        __scope = _scope;
        __routeParams = _routeParams;
        __path = _path;
        __util = _util;
        __api = _api;
        __group = _group;
        __dialogue = _dialogue;
        _defineVariable();
        _defineFunction();
        _readGroup();

    }

    function _defineVariable() {

        __scope.modeName = "Group";
        __scope.groupDatum = {};
        __scope.accessType = null;
        __scope.accessLabelArray = [];
        __scope.accessDataList = [];
        _groupId = __routeParams.groupId;

    }

    function _defineFunction() {

        __scope.getAccessName = _getAccessName;
        __scope.getAccessClass = _getAccessClass;
        __scope.confirmValueType = _confirmValueType;
        __scope.onBoardAccessClick = _onBoardAccessClick;
        __scope.onPhotoAccessClick = _onPhotoAccessClick;
        __scope.onVideoAccessClick = _onVideoAccessClick;

    }

    function _readGroup() {

        __group.read(_groupId, _onRead);

        function _onRead(_responseDatum) {
            __scope.groupDatum = _responseDatum;
        }

    }

    function _getAccessClass() {

        if ( !(__scope.accessType) ) return;
        var _obj = {};
        _obj[ "access-type-" + __scope.accessType ] = true;
        return _obj;

    }

    function _getAccessName() {

        if ( __scope.accessType === "board" ) return __scope.lyric.REPORT_BOARD_BUTTON;
        if ( __scope.accessType === "photo" ) return __scope.lyric.REPORT_PHOTO_BUTTON;
        if ( __scope.accessType === "video" ) return __scope.lyric.REPORT_VIDEO_BUTTON;

    }

    function _confirmValueType(_type, _value) {

        var _TYPE_TEXT = ( _type === "text" );
        var _TYPE_PHOTO = ( _type === "photo" );
        var _TYPE_VIDEO = ( _type === "video" );
        if ( /(jpg|jpeg|png)$/.test(_value) ) return _TYPE_PHOTO;
        if ( /https?\:\/{2}|((mp4)$)/.test(_value) ) return _TYPE_VIDEO;
        return _TYPE_TEXT;

    }

    function _onBoardAccessClick() {

        __api.boardGroupViewStatistic(_groupId, 1)
            .success(_onSuccess);

        function _onSuccess(_response) {
            var _responseBoards = _response.boards;
            var _list = [];
            var _index = _responseBoards.length;
            var _item;
            while ( _index-- > 0 ) {
                _item = _responseBoards[_index];
                _list.unshift({
                        postTime: _item.created_at,
                        content: _item.text,
                        count: _item.num_views
                    });
            }
            __scope.accessType = "board";
            __scope.accessLabelArray = [
                    __scope.lyric.ACCESS_TIME_LABEL,
                    __scope.lyric.ACCESS_CONTENT_LABEL,
                    __scope.lyric.ACCESS_COUNT_LABEL
                ];
            __scope.accessDataList = _list;
            __scope.$apply();
        }

    }

    function _onPhotoAccessClick() {

        __api.fileGroupViewFileStatistic(_groupId, "image", 1)
            .success(_onSuccess);

        function _onSuccess(_response) {
            var _responseFiles = _response.files;
            var _list = [];
            var _index = _responseFiles.length;
            var _item;
            while ( _index-- > 0 ) {
                _item = _responseFiles[_index];
                _list.unshift({
                        postTime: _item.created_at,
                        content: __path.PUBLIC + _item.links[0] ,
                        count: _item.num_views
                    });
            }
            __scope.accessType = "photo";
            __scope.accessLabelArray = [
                    __scope.lyric.ACCESS_TIME_LABEL,
                    __scope.lyric.ACCESS_CONTENT_LABEL,
                    __scope.lyric.ACCESS_COUNT_LABEL
                ];
            __scope.accessDataList = _list;
            __scope.$apply();
        }

    }

    function _onVideoAccessClick() {

        __api.fileGroupViewFileStatistic(_groupId, "video", 1)
            .success(_onSuccess);

        function _onSuccess(_response) {
            var _responseFiles = _response.files;
            var _list = [];
            var _index = _responseFiles.length;
            var _item;
            while ( _index-- > 0 ) {
                _item = _responseFiles[_index];
                _list.unshift({
                        postTime: _item.created_at,
                        content: _item.links[0],
                        count: _item.num_views
                    });
            }
            __scope.accessType = "video";
            __scope.accessLabelArray = [
                    __scope.lyric.ACCESS_TIME_LABEL,
                    __scope.lyric.ACCESS_CONTENT_LABEL,
                    __scope.lyric.ACCESS_COUNT_LABEL
                ];
            __scope.accessDataList = _list;
            __scope.$apply();
        }

    }

}(window.allot));
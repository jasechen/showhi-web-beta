(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __api;
    var __dialogue;

    (function () {

        _pediaController.$inject = ["$scope", "$api", "$dialogue"];
        __ngApp.controller("PediaController", _pediaController);

    }());

    function _pediaController(_scope, _api, _dialogue) {

        __scope = _scope;
        __api = _api;
        __dialogue = _dialogue;
        _defineVariable();
        _defineFunction();
        _readLanguages();

    }

    function _defineVariable() {

        __scope.pediaList = [];
        __scope.newable = {};
        __scope.isEdit = (/\/r\/s\/n\/pedia\/edit/.test(window.location.href));

    }

    function _defineFunction() {

        __scope.onTextareaChange = _onTextareaChange;
        __scope.onNewableClick = _onNewableClick;
        __scope.onUpdateClick = _onUpdateClick;

    }

    function _readLanguages() {

        __api.localeFetchAll()
            .success(_onSuccess);

        function _onSuccess(_response) {
            __scope.pediaList = _getArranged(_response.locales);
            __scope.$apply();
        }

        function _getArranged(_datum) {
            var _list = [];
            var _key;
            var _item;
            for ( _key in _datum ) {
                _item = _datum[_key];
                if ( /^(country|language|interest|pet|gender)\_name\_/.test(_item.code) ) delete _datum[_key];
                else _list.push(_item);
            }
            return _list;
        }

    }

    function _onTextareaChange(_item) {

        _item.isModify = true;

    }

    function _onNewableClick() {

        __scope.newable.isWorking = true;
        __api.localeCreate(
                __scope.newable.code,
                "en",
                __scope.newable.en,
                "tw",
                __scope.newable.tw,
                "cn",
                __scope.newable.cn
            )
            .success(_onSuccess)
            .error(_onError);

        function _onSuccess() {
            __scope.newable = {};
            _readLanguages();
        }

        function _onError() {
            delete __scope.newable.isWorking;
            __dialogue.alert("Role error");
        }

    }

    function _onUpdateClick(_item) {

        _item.isWorking = true;
        __api.localeUpdate(
                _item.id,
                _item.code,
                "en",
                _item.content.en,
                "tw",
                _item.content.tw,
                "cn",
                _item.content.cn
            )
            .success(_onSuccess)
            .error(_onError)
            .mislay(_onMislay);

        function _onSuccess() {
            _readLanguages();
        }

        function _onError() {
            delete _item.isWorking;
            __dialogue.alert("Role error");
        }

        function _onMislay() {
            delete _item.isWorking;
            __dialogue.alert("Content mislay");
        }

    }

}(window.allot));
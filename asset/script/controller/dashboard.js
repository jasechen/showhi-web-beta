(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __routeParams;
    var __api;
    var __account;
    var __user;
    var _userId;
    var _isBoardsReading;
    var _currentPage;

    (function () {

        _dashboardController.$inject = ["$scope", "$routeParams", "$api", "$account", "$user"];
        __ngApp.controller("DashboardController", _dashboardController);

    }());

    function _dashboardController(_scope, _routeParams, _api, _account, _user) {

        __scope = _scope;
        __routeParams = _routeParams;
        __api = _api;
        __account = _account;
        __user = _user;
        _defineVariable();
        _defineFunction();
        _readUser();

    }

    function _defineVariable() {

        __scope.modeName = "User";
        __scope.userDatum = {};
        _userId = __routeParams.userId;

    }

    function _defineFunction() {

        __scope.readAccount = _readUser;

    }

    function _readUser() {

        __user.read(_userId, _onUserRead, null, _onFinish);

        function _onUserRead(_responseDatum) {
            __scope.userDatum = _responseDatum;
            _fetchBoards();
        }

        function _onFinish() {
            __scope.$apply();
        }

    }

    function _fetchBoards(_isMorePage) {

        if ( _isBoardsReading ) return;
        _isBoardsReading = true;
        if ( !(_isMorePage) ) _currentPage = 1;
        __api.boardBlog(__scope.userDatum.blogId, _currentPage++)
            .success(_onSuccess)
            .mislay(_onMislay)
            .finish(_onFinish);

        function _onSuccess(_response) {
            var _boardList = _response.boards;
            if ( !(_isMorePage) ) __scope.boardList = [];
            __scope.boardList = __scope.boardList.concat(_boardList);
            _setEvent(__scope.boardList);
        }

        function _onMislay() {
            _isBoardsReading = false;
        }

        function _onFinish() {
            _isBoardsReading = false;
            __scope.$apply();
        }

        function _setEvent(_boardList) {
            var _index = _boardList.length;
            var _board;
            var _isLastSet;
            var _MAX_BOARD_NUM = 15;
            if ( _index < _MAX_BOARD_NUM ) return;
            while ( _index-- > 0 ) {
                _board = _boardList[_index];
                if ( _isLastSet ) delete _board.fetchMore;
                else {
                    _board.fetchMore = _fetchBoards;
                    _isLastSet = true;
                }
            }
        }

    }

}(window.allot));
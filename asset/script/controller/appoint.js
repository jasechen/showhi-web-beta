(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __location;
    var __routeParams;
    var __util;
    var __api;
    var __user;
    var __group;
    var __dialogue;
    var _groupId;
    var _memberId;

    (function () {

        _appointController.$inject = ["$scope", "$location", "$routeParams", "$util", "$api", "$user", "$group", "$dialogue"];
        __ngApp.controller("AppointController", _appointController);

    }());

    function _appointController(_scope, _location, _routeParams, _util, _api, _user, _group, _dialogue) {

        __scope = _scope;
        __location = _location;
        __routeParams = _routeParams;
        __util = _util;
        __api = _api;
        __user = _user;
        __group = _group;
        __dialogue = _dialogue;
        _defineVariable();
        _defineFunction();
        _readGroup();
        _readMember();
        _readAppoint();

    }

    function _defineVariable() {

        __scope.modeName = "Group";
        __scope.groupDatum = {};
        __scope.appointDate = new Date();
        __scope.availableDates = [];
        __scope.appointTime = "";
        __scope.appointDescription = "";
        _groupId = __routeParams.groupId;
        _memberId = __routeParams.memberId;

    }

    function _defineFunction() {

        __scope.getFormalTime = _getFormalTime;
        __scope.onDateChange = _onDateChange;
        __scope.onTimeClick = _onTimeClick;
        __scope.onConfirmClick = _onConfirmClick;

    }

    function _readGroup() {

        __group.read(_groupId, _onRead);

        function _onRead(_responseDatum) {
            __scope.groupDatum = _responseDatum;
        }

    }

    function _readMember() {

        __user.read(_memberId, _onUserRead, null, _onFinish);

        function _onUserRead(_responseDatum) {
            __scope.memberDatum = _responseDatum;
            _responseDatum.avatarUrl = ( _responseDatum.avatarUrl || "" ).replace(/(\d)$/, "$1_q.png");
        }

        function _onFinish() {
            __scope.$apply();
        }

    }

    function _readAppoint() {

        _fetchAppoint();

    }

    function _fetchAppoint() {

        __api.groupUsersAvailableDates(
                _groupId,
                _memberId,
                __util.getFormalDate(__scope.appointDate)
            )
            .success(_onSuccess)
            .error(_onError)
            .finish(_onFinish);

        function _onSuccess(_response) {
            __scope.availableDates = _response;
        }

        function _onError() {
            __scope.availableDates = [];
        }

        function _onFinish() {
            __scope.$apply();
        }

    }

    function _getFormalTime(_time) {

        return _time.replace(/\B(?=\d{2}$)/, ":");

    }

    function _onDateChange() {

        _fetchAppoint();
        __scope.appointTime = "";

    }

    function _onTimeClick(_timeItem) {

        __scope.appointTime = _timeItem;

    }

    function _onConfirmClick() {

        __api.meetingCreate(
                _groupId,
                _memberId,
                __util.getFormalDate(__scope.appointDate),
                __scope.appointTime,
                __scope.appointDescription
            )
            .success(_onSuccess)
            .error(_onError)
            .mislay(_onMislay);

        function _onSuccess() {
            __dialogue.alert(__scope.lyric.ALERT_SUCCESS, _onConfirm);
        }

        function _onError() {
            __dialogue.alert(__scope.lyric.ALERT_ERROR);
        }

        function _onMislay() {
            __dialogue.alert(__scope.lyric.APPOINT_EMPTY);
        }

        function _onConfirm() {
            __location.path( "/group/" + _groupId );
        }

    }

}(window.allot));
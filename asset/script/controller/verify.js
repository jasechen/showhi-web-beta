(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __api;
    var _userID;

    (function () {

        _verifyController.$inject = ["$scope", "$routeParams", "$api"];
        __ngApp.controller("VerifyController", _verifyController);

    }());

    function _verifyController(_scope, _routeParams, _api) {

        __scope = _scope;
        __api = _api;
        _userID = _routeParams.userID;
        _defineEvent();
        _readAccount();

    }

    function _defineEvent() {

        __scope.sendVerifyMail = _sendVerifyMail;
        __scope.sendVerifyPhone = _sendVerifyPhone;
        __scope.processMail = _processMail;
        __scope.processPhone = _processPhone;

    }

    function _readAccount() {

        __api.user(_userID)
            .success(_onSuccess);

        function _onSuccess(_response) {
            var _responseUser = _response.user;
            __scope.mailStatus = ( _responseUser.email_status === "checked" ) ;
            __scope.phoneStatus = ( _responseUser.mobile_status === "checked" ) ;
            __scope.recruiterStatus = ( _responseUser.recruiter_status === "checked" ) ;
            __scope.$apply();
        }

    }

    function _sendVerifyMail() {

        __api.userEmailNotify(_userID)
            .success(_onSuccess);

        function _onSuccess() {

        }

    }

    function _sendVerifyPhone() {

        __api.userMobileNotify(_userID)
            .success(_onSuccess);

        function _onSuccess() {

        }

    }

    function _processMail() {

        __api.userEmailVerify(_userID, __scope.mailVerifyCode)
            .success(_onSuccess);

        function _onSuccess() {
            _readAccount();
        }

    }

    function _processPhone() {

        __api.userMobileVerify(_userID, __scope.phoneVerifyCode)
            .success(_onSuccess);

        function _onSuccess() {
            _readAccount();
        }

    }

}(window.allot));
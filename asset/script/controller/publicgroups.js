(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __path;
    var __api;
    var __account;

    (function () {

        _publicgroupsController.$inject = ["$scope", "$path", "$api", "$account"];
        __ngApp.controller("PublicgroupsController", _publicgroupsController);

    }());

    function _publicgroupsController(_scope, _path, _api, _account) {

        __scope = _scope;
        __path = _path;
        __api = _api;
        __account = _account;
        _defineVariable();
        _defineFunction();
        _readGroups();

    }

    function _defineVariable() {

        __scope.groupFilter = {
                country: "us"
            };
        __scope.countryDictionary = {
                us: "Schools in the U.S.",
                cn: "Schools in China"
            };

    }

    function _defineFunction() {

        __scope.getLink = _getLink;

    }

    function _readGroups() {

        __api.groupsList()
            .success(_onSuccess);

        function _onSuccess(_response) {
            __scope.groupList = _response || [] ;
            _addAvatarPath();
            __scope.$apply();
        }

        function _addAvatarPath() {
            var _index = __scope.groupList.length;
            var _item;
            while ( _index-- > 0 ) {
                _item = __scope.groupList[_index];
                if ( !_item.avatar ) _item.avatarUrl = "/asset/image/dock-avatar-default.png";
                else _item.avatarUrl = __path.PUBLIC + "/gallery/group_" + _item.id + "/avatar/" + _item.avatar + "_o.png" ;
            }
        }

    }

    function _getLink(_item) {

        if ( __scope.isLogined ) return "/group/" + _item.id ;

    }

}(window.allot));
(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __location;
    var __routeParams;
    var __api;
    var __group;

    (function () {

        _groupController.$inject = ["$scope", "$location", "$routeParams", "$path", "$api", "$group"];
        __ngApp.controller("GroupeditController", _groupController);

    }());

    function _groupController(_scope, _location, _routeParams, _path, _api, _group) {

        __scope = _scope;
        __location = _location;
        __routeParams = _routeParams;
        __api = _api;
        __group = _group;
        _defineVariable();
        _defineFunction();
        _readGroup();

    }

    function _defineVariable() {

        __scope.modeName = "Group";
        __scope.groupDatum = {};
        __scope.modifiedUserDatum = {};
        __scope.privacyList = [{
                    id: "public",
                    name: __scope.lyric.GROUPEDIT_PRIVACY_PUBLIC_BUTTON,
                    description: __scope.lyric.GROUPEDIT_PRIVACY_PUBLIC_DESCRIPTION
                }, {
                    id: "private",
                    name: __scope.lyric.GROUPEDIT_PRIVACY_PRIVATE_BUTTON,
                    description: __scope.lyric.GROUPEDIT_PRIVACY_PRIVATE_DESCRIPTION
            }];
        __scope.statusList = [{
                    id: "enable",
                    name: __scope.lyric.GROUPEDIT_STATUS_ENABLE_BUTTON,
                    description: __scope.lyric.GROUPEDIT_STATUS_ENABLE_DESCRIPTION
                }, {
                    id: "disable",
                    name: __scope.lyric.GROUPEDIT_STATUS_DISABLE_BUTTON,
                    description: __scope.lyric.GROUPEDIT_STATUS_DISABLE_DESCRIPTION
            }];

    }

    function _defineFunction() {

        __scope.readGroup = _readGroup;
        __scope.onUpdateClick = _onUpdateClick;

    }

    function _readGroup() {

        var _id = __routeParams.groupID;
        if ( _id ) __group.read(_id, _onRead);

        function _onRead(_responseDatum) {
            __scope.groupDatum = _responseDatum;
            __scope.modifiedUserDatum = JSON.parse(JSON.stringify(_responseDatum));
        }

    }

    function _onUpdateClick() {

        var _datum = __scope.modifiedUserDatum;
        __api.groupsUpdate(
                _datum.id,
                _datum.title,
                _datum.description,
                _datum.status,
                _datum.privacy
            )
            .success(_onSuccess)
            .finish(_onFinish);

        function _onSuccess() {
            __location.path( "/group/" + _datum.id );
        }

        function _onFinish() {
            __scope.$apply();
        }

    }

}(window.allot));
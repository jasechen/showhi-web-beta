(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __location;
    var __util;
    var __api;
    var __account;
    var __dialogue;
    var _qrcodeID;

    (function () {

        _registerController.$inject = ["$scope", "$routeParams", "$location", "$util", "$api", "$account", "$dialogue"];
        __ngApp.controller("RegisterController", _registerController);

    }());

    function _registerController(_scope, _routeParams, _location, _util, _api, _account, _dialogue) {

        __scope = _scope;
        __location = _location;
        __util = _util;
        __api = _api;
        __account = _account;
        __dialogue = _dialogue;
        __scope.qrcodeID = _qrcodeID = _routeParams.qrcodeID;
        __scope.onProcessClick = _onProcessClick;
        __scope.countrycode = "1";
        if ( __scope.language === "zh_tw" ) __scope.countrycode = "886";
        else if ( __scope.language === "zh_cn" ) __scope.countrycode = "86";
        _readQRCodeData();
        _readCountrycode();

    }

    function _readQRCodeData() {

        if ( !_qrcodeID ) return;
        __api.qrcodesCheck(_qrcodeID)
            .success(_onQRCodeCheckSuccess)
            .error(_onQRCodeCheckError);

        function _onQRCodeCheckSuccess(_response) {
            if ( !_response ) _onQRCodeCheckError();
            else __api.qrcodes(_qrcodeID)
                .success(_onQRCodeSuccess)
                .error(_onError)
                .mislay(_onError);
        }

        function _onQRCodeCheckError() {
            __location.path("/home");
            __dialogue.alert(__scope.lyric.REGISTER_QRCODE_EXPIRED);
            __scope.$apply();
        }

        function _onQRCodeSuccess(_response) {
            var _datum = _response[0] || {} ;
            __scope.introducerID = _datum.creater_id;
            __scope.expireTime = __util.getExpireDate(_datum.created_at, _datum.expire_time);
            __api.user(__scope.introducerID)
                .success(_onIntroduceSuccess);
        }

        function _onIntroduceSuccess(_response) {
            var _responseProfile = ( _response.user || {} ).profile;
            __scope.introducerName = _responseProfile.first_name + _responseProfile.last_name ;
            __scope.$apply();
        }

        function _onError() {

        }

    }

    function _readCountrycode() {

        var _langName = "en";
        if ( __scope.language === "zh_tw" ) _langName = "tw";
        else if ( __scope.language === "zh_cn" ) _langName = "cn";
        __api.countryCallings(_langName)
            .success(_onCountrySuccess);

        function _onCountrySuccess(_response) {
            __scope.countrycodeArray = _response.callings;
            __scope.$apply();
        }

    }

    function _onProcessClick() {

        __scope.phone = ( __scope.phone || "" ).replace(/^0/, "");
        __scope.isProcessWorking = true;
        __api.register(
                __scope.account,
                __scope.password,
                __scope.rePassword,
                __scope.firstName,
                __scope.lastName,
                __scope.countrycode,
                __scope.phone,
                __scope.qrcodeID,
                __scope.introducerID
            )
            .success(_onSuccess)
            .error(_onError)
            .mislay(_onMislay)
            .finish(_onFinish);

        function _onSuccess(_response) {
            var _responseToken = _response.token;
            __account.setToken(_responseToken);
            __account.read(_onUserRead);
        }

        function _onUserRead(_response) {
            var _responseDatum = _response.user || {} ;
            __location.path( "/verify/" + _responseDatum.id );
            __scope.$apply();
        }

        function _onError() {
            __scope.isPrecessError = true;
        }

        function _onMislay() {
            __scope.isPrecessError = true;
            __scope.isProcessWorking = false;
        }

        function _onFinish() {
            __scope.isProcessWorking = false;
            __scope.$apply();
        }

    }

}(window.allot));
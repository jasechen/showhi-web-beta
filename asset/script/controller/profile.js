(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __routeParams;
    var __account;
    var __user;
    var _userId;

    (function () {

        _profileController.$inject = ["$scope", "$routeParams", "$account", "$user"];
        __ngApp.controller("ProfileController", _profileController);

    }());

    function _profileController(_scope, _routeParams, _account, _user) {

        __scope = _scope;
        __routeParams = _routeParams;
        __account = _account;
        __user = _user;
        _defineVariable();
        _defineFunction();
        _readAccount();
        _readUser();

    }

    function _defineVariable() {

        __scope.modeName = "User";
        __scope.userDatum = {};
        _userId = __routeParams.userId;

    }

    function _defineFunction() {

        __scope.readAccount = _readUser;

    }

    function _readAccount() {

        __account.addCallback(_onRead);
        __account.read();

        function _onRead(_response) {
            __scope.accountDatum = _response.user || {} ;
        }

    }

    function _readUser() {

        __user.read(_userId, _onUserRead, null, _onFinish);

        function _onUserRead(_responseDatum) {
            __scope.userDatum = _responseDatum;
        }

        function _onFinish() {
            __scope.$apply();
        }

    }

}(window.allot));
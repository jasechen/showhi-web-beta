(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __api;
    var __user;

    (function () {

        _inviteController.$inject = ["$scope", "$api", "$user"];
        __ngApp.controller("InviteController", _inviteController);

    }());

    function _inviteController(_scope, _api, _user) {

        __scope = _scope;
        __api = _api;
        __user = _user;
        _defineVariable();
        _defineFunction();
        _readAccount();
        _readQrcodeList();

    }

    function _defineVariable() {

        __scope.modeName = "User";
        __scope.userDatum = {};

    }

    function _defineFunction() {

        __scope.readQrcodeList = _readQrcodeList;

    }

    function _readAccount() {

        __user.read(null, _onUserRead, null, _onFinish);

        function _onUserRead(_responseDatum) {
            __scope.userDatum = _responseDatum;
        }

        function _onFinish() {
            __scope.$apply();
        }

    }

    function _readQrcodeList() {

        _fetchQrcodeList();

    }

    function _fetchQrcodeList() {

        __api.qrcodesList()
            .success(_onSuccess)
            .finish(_onFinish);

        function _onSuccess(_response) {
            __scope.qrcodeList = _response;
        }

        function _onFinish() {
            __scope.$apply();
        }

    }

}(window.allot));
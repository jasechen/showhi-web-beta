(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;

    (function () {

        _homeController.$inject = ["$scope"];
        __ngApp.controller("HomeController", _homeController);

    }());

    function _homeController(_scope) {

        __scope = _scope;
        _setCover();
        _setVideo();
        _setNews();
        _setSchools();

    }

    function _setCover() {

        var _datum = __scope.homeCoverDatum = [{
                image: "home-cover-picture1.jpg",
                textCode: "HOME_COVER_TMP_1"
            }, {
                image: "home-cover-picture2.jpg",
                textCode: "HOME_COVER_TMP_2"
            }, {
                image: "home-cover-picture3.jpg",
                textCode: "HOME_COVER_TMP_3"
            }];
        var _index = 0;
        __scope.getCoverContentStyle = _getContentStyle;
        __scope.getCoverItemStyle = _getItemStyle;
        __scope.getCoverMenuItemClass = _getMenuItemClass;
        __scope.onCoverPrev = _onPrev;
        __scope.onCoverNext = _onNext;
        __scope.onCoverMenuIndex = _onMenuIndex;

        function _getContentStyle() {
            return {
                    "width": ( _datum.length * 100 ) + "%",
                    "margin-left": -( _index * 100 ) + "%"
                };
        }

        function _getItemStyle() {
            return {
                    "width": ( 100 / _datum.length ) + "%"
                };
        }

        function _getMenuItemClass(_targetIndex) {
            return {
                    "work": _targetIndex === _index
                };
        }

        function _onPrev() {
            if ( _index > 0 ) _index--;
            else _index = _datum.length - 1 ;
        }

        function _onNext() {
            if ( _index < _datum.length - 1 ) _index++;
            else _index = 0;
        }

        function _onMenuIndex(_targetIndex) {
            _index = _targetIndex;
        }

    }

    function _setVideo() {

        var _MAX_NUM_IN_PAGE = 3;
        var _datum = __scope.homeVideoDatum = [{
                image: "home-video-picture1.jpg",
                text: "一分鐘視頻了解我們"
            }, {
                image: "home-video-picture2.jpg",
                text: "兩分鐘視頻了解我們"
            }, {
                image: "home-video-picture3.jpg",
                text: "三分鐘視頻了解我們"
            }, {
                image: "home-video-picture1.jpg",
                text: "四分鐘視頻了解我們"
            }, {
                image: "home-video-picture2.jpg",
                text: "五分鐘視頻了解我們"
            }, {
                image: "home-video-picture3.jpg",
                text: "六分鐘視頻了解我們"
            }];
        __scope.getVideoPageRepeatNum = _getPageRepeatNum;

        function _getPageRepeatNum() {
            var _pageNum = parseInt( _datum.length / _MAX_NUM_IN_PAGE ) + ( ( _datum.length % _MAX_NUM_IN_PAGE ) ? 1 : 0 ) ;
            return new Array(_pageNum);
        }

    }

    function _setNews() {

        var _datum = __scope.homeNewsDatum = [{
                text: "熱門招生中 George Mason Univers"
            }, {
                text: "熱門招生中 University of Wiscon"
            }, {
                text: "熱門招生中 Ball State Universit"
            }, {
                text: "熱門招生中 北京師範大學"
            }, {
                text: "熱門招生中 北京交通大學"
            }];

    }

    function _setSchools() {

        var _datum = __scope.homeSchoolsDatum = [{
                image: "home-schools-tmp1.png",
                text: "University of Wiscon"
            }, {
                image: "home-schools-tmp2.png",
                text: "George Mason University"
            }, {
                image: "home-schools-tmp3.png",
                text: "北京師範大學"
            }, {
                image: "home-schools-tmp4.png",
                text: "北京交通大學"
            }, {
                image: "home-schools-tmp5.png",
                text: "中國傳媒大學"
            }, {
                image: "home-schools-tmp6.png",
                text: "四川師範大學"
            }, {
                image: "home-schools-tmp7.png",
                text: "溫州醫科大學"
            }, {
                image: "home-schools-tmp8.png",
                text: "淮陰師範學院"
            }, {
                image: "home-schools-tmp9.png",
                text: "哈爾濱師範大學"
            }, {
                image: "home-schools-tmp10.png",
                text: "東北農業大學"
            }];

    }

}(window.allot));
(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __routeParams;
    var __api;
    var __account;
    var __user;
    var __group;
    var _isBoardsReading;
    var _currentPage;

    (function () {

        _groupController.$inject = ["$scope", "$routeParams", "$path", "$api", "$account", "$user", "$group"];
        __ngApp.controller("GroupController", _groupController);

    }());

    function _groupController(_scope, _routeParams, _path, _api, _account, _user, _group) {

        __scope = _scope;
        __routeParams = _routeParams;
        __api = _api;
        __account = _account;
        __user = _user;
        __group = _group;
        _defineVariable();
        _defineFunction();
        _readGroup();

    }

    function _defineVariable() {

        __scope.modeName = "Group";
        __scope.groupDatum = {};

    }

    function _defineFunction() {

        __scope.readGroup = _readGroup;

    }

    function _readGroup() {

        var _id = __routeParams.groupID;
        if ( _id ) __group.read(_id, _onRead);

        function _onRead(_responseDatum) {
            __scope.groupDatum = _responseDatum;
            _fetchBoards();
        }

    }

    function _fetchBoards(_isMorePage) {

        if ( _isBoardsReading ) return;
        _isBoardsReading = true;
        if ( !(_isMorePage) ) _currentPage = 1;
        __api.boardGroup(__scope.groupDatum.id, _currentPage++)
            .success(_onSuccess)
            .mislay(_onMislay)
            .finish(_onFinish);

        function _onSuccess(_response) {
            var _boardList = _response.boards;
            if ( !(_isMorePage) ) __scope.boardList = [];
            __scope.boardList = __scope.boardList.concat(_boardList);
            _setEvent(__scope.boardList);
        }

        function _onMislay() {
            _isBoardsReading = false;
        }

        function _onFinish() {
            _isBoardsReading = false;
            __scope.$apply();
        }

        function _setEvent(_boardList) {
            var _index = _boardList.length;
            var _board;
            var _isLastSet;
            var _MAX_BOARD_NUM = 15;
            if ( _index < _MAX_BOARD_NUM ) return;
            while ( _index-- > 0 ) {
                _board = _boardList[_index];
                if ( _isLastSet ) delete _board.fetchMore;
                else {
                    _board.fetchMore = _fetchBoards;
                    _isLastSet = true;
                }
            }
        }

    }

}(window.allot));
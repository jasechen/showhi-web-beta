(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __routeParams;
    var __api;
    var __user;
    var __group;
    var _groupId;

    (function () {

        _groupinviteController.$inject = ["$scope", "$routeParams", "$api", "$user", "$group"];
        __ngApp.controller("GroupinviteController", _groupinviteController);

    }());

    function _groupinviteController(_scope, _routeParams, _api, _user, _group) {

        __scope = _scope;
        __routeParams = _routeParams;
        __api = _api;
        __user = _user;
        __group = _group;
        _defineVariable();
        _defineFunction();
        _readAccount();
        _readGroup();
        _readQrcodeList();

    }

    function _defineVariable() {

        __scope.modeName = "Group";
        __scope.groupDatum = {};
        _groupId = __routeParams.groupId;

    }

    function _defineFunction() {

        __scope.readQrcodeList = _readQrcodeList;

    }

    function _readAccount() {

        __user.read(null, _onUserRead, null, _onFinish);

        function _onUserRead(_responseDatum) {
            __scope.accountDatum = _responseDatum;
        }

        function _onFinish() {
            __scope.$apply();
        }

    }

    function _readGroup() {

        __group.read(_groupId, _onRead);

        function _onRead(_responseDatum) {
            __scope.groupDatum = _responseDatum;
        }

    }

    function _readQrcodeList() {

        _fetchQrcodeList();

    }

    function _fetchQrcodeList() {

        __api.qrcodesListOfGroup(_groupId)
            .success(_onSuccess)
            .finish(_onFinish);

        function _onSuccess(_response) {
            __scope.qrcodeList = _response;
        }

        function _onFinish() {
            __scope.$apply();
        }

    }

}(window.allot));
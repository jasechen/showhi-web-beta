(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __routeParams;
    var __util;
    var __api;
    var __group;
    var __dialogue;
    var _groupId;

    (function () {

        _appointmakerController.$inject = ["$scope", "$routeParams", "$util", "$api", "$group", "$dialogue"];
        __ngApp.controller("AppointmakerController", _appointmakerController);

    }());

    function _appointmakerController(_scope, _routeParams, _util, _api, _group, _dialogue) {

        __scope = _scope;
        __routeParams = _routeParams;
        __util = _util;
        __api = _api;
        __group = _group;
        __dialogue = _dialogue;
        _defineVariable();
        _defineFunction();
        _readGroup();
        _readAppointment();

    }

    function _defineVariable() {

        __scope.modeName = "Group";
        __scope.groupDatum = {};
        __scope.isDayTime = true;
        __scope.dayArray = [0, 1, 2, 3, 4, 5, 6];
        __scope.daytimeArray = _getTimeArray("daytime");
        __scope.nighttimeArray = _getTimeArray("nighttime");
        __scope.makerBeginDate = new Date();
        __scope.makerEndDate = new Date();
        __scope.dayNameArray = [
                "Sun.",
                "Mon.",
                "Tue.",
                "Wed.",
                "Thu.",
                "Fri.",
                "Sat."
            ];
        __scope.appointTimeList = _getAppointTimeList();
        _groupId = __routeParams.groupId;

    }

    function _defineFunction() {

        __scope.getFormalTime = _getFormalTime;
        __scope.onTypeClick = _onTypeClick;
        __scope.onTimeClick = _onTimeClick;
        __scope.onConfirmClick = _onConfirmClick;

    }

    function _readGroup() {

        __group.read(_groupId, _onRead);

        function _onRead(_responseDatum) {
            __scope.groupDatum = _responseDatum;
        }

    }

    function _readAppointment() {

        __api.groupUsersGetTimeSetting(_groupId)
            .success(_onSuccess);

        function _onSuccess(_response) {
            _response = _response[0] || {} ;
            __scope.makerBeginDate = new Date(_response.start_date);
            __scope.makerEndDate = new Date(_response.end_date);
            __scope.appointTimeList[0] = _getTimeDictionary(_response.sun);
            __scope.appointTimeList[1] = _getTimeDictionary(_response.mon);
            __scope.appointTimeList[2] = _getTimeDictionary(_response.tue);
            __scope.appointTimeList[3] = _getTimeDictionary(_response.wed);
            __scope.appointTimeList[4] = _getTimeDictionary(_response.thu);
            __scope.appointTimeList[5] = _getTimeDictionary(_response.fri);
            __scope.appointTimeList[6] = _getTimeDictionary(_response.sat);
            __scope.$apply();
        }

        function _getTimeDictionary(_array) {
            var _dictionary = {};
            _array = _array || [] ;
            while ( _array.length ) _dictionary[_array.shift()] = true;
            return _dictionary;
        }

    }

    function _getTimeArray(_type) {

        var _array = [];
        var _BEGIN_TIME = ( _type === "nighttime" ) ? 18 : 6 ;
        var _index = 12;
        var _keyName;
        while ( _index-- > 0 ) {
            _keyName = _BEGIN_TIME + _index ;
            if ( _keyName >= 24 ) _keyName = _keyName - 24 ;
            if ( _keyName < 10 ) _keyName = "0" + _keyName ;
            _array.unshift( _keyName + "30" );
            _array.unshift( _keyName + "00" );
        }
        return _array;

    }

    function _getAppointTimeList() {

        var _list = [];
        var _index = __scope.dayArray.length;
        while ( _index-- > 0 ) _list.push({});
        return _list;

    }

    function _getFormalTime(_time) {

        return _time.replace(/\B(?=\d{2}$)/, ":");

    }

    function _onTypeClick(_type) {

        __scope.isDayTime = ( _type === "daytime" );

    }

    function _onTimeClick(_index, _time) {

        var _timeDictionary = __scope.appointTimeList[_index];
        if ( _timeDictionary[_time] ) delete _timeDictionary[_time];
        else _timeDictionary[_time] = true;

    }

    function _onConfirmClick() {

        __api.groupUsersTimeSetting(
                _groupId,
                __util.getFormalDate(__scope.makerBeginDate),
                __util.getFormalDate(__scope.makerEndDate),
                Object.keys( __scope.appointTimeList[0] || {} ),
                Object.keys( __scope.appointTimeList[1] || {} ),
                Object.keys( __scope.appointTimeList[2] || {} ),
                Object.keys( __scope.appointTimeList[3] || {} ),
                Object.keys( __scope.appointTimeList[4] || {} ),
                Object.keys( __scope.appointTimeList[5] || {} ),
                Object.keys( __scope.appointTimeList[6] || {} )
            )
            .success(_onSuccess);

        function _onSuccess() {
            __dialogue.alert(__scope.lyric.ALERT_SAVED);
        }

    }

}(window.allot));
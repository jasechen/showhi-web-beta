(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __location;
    var __api;
    var __user;

    (function () {

        _newgroupController.$inject = ["$scope", "$location", "$api", "$user"];
        __ngApp.controller("NewgroupController", _newgroupController);

    }());

    function _newgroupController(_scope, _location, _api, _user) {

        __scope = _scope;
        __location = _location;
        __api = _api;
        __user = _user;
        _defineVariable();
        _defineFunction();
        _readAccount();

    }

    function _defineVariable() {

        __scope.modeName = "User";
        __scope.userDatum = {};
        __scope.newgroupDatum = {};
        __scope.newgroupDatum.type = "business";
        __scope.typeDictionary = {
                business: __scope.lyric.NEWGROUP_TYPE_BUSINESS
            };

    }

    function _defineFunction() {

        __scope.readAccount = _readAccount;
        __scope.onSubmitClick = onSubmitClick;

    }

    function _readAccount() {

        __user.read(null, _onUserRead, null, _onFinish);

        function _onUserRead(_responseDatum) {
            __scope.userDatum = _responseDatum;
            if ( __scope.userDatum.type !== "school" ) return;
            __scope.typeDictionary.school = __scope.lyric.NEWGROUP_TYPE_SCHOOL;
            __scope.typeDictionary.dept = __scope.lyric.NEWGROUP_TYPE_DEPT;
            __scope.typeDictionary.community = __scope.lyric.NEWGROUP_TYPE_COMMUNITY;
        }

        function _onFinish() {
            __scope.$apply();
        }

    }

    function onSubmitClick() {

        __api.groupsCreate(
                __scope.newgroupDatum.type,
                __scope.newgroupDatum.name,
                __scope.newgroupDatum.tag
            )
            .success(_onSuccess);

        function _onSuccess(_response) {
            __location.path( "/group/" + _response.id );
            __scope.$apply();
        }

    }

}(window.allot));
(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __translator;
    var __user;

    (function () {

        _langpickerController.$inject = ["$scope", "$translator", "$user"];
        __ngApp.controller("LangpickerController", _langpickerController);

    }());

    function _langpickerController(_scope, _translator, _user) {

        __scope = _scope;
        __translator = _translator;
        __user = _user;
        _defineVariable();
        _defineFunction();
        _readAccount();

    }

    function _defineVariable() {

        __scope.modeName = "User";
        __scope.userDatum = {};
        __scope.languagesDictionary = __translator.languageDict;

    }

    function _defineFunction() {

        __scope.readAccount = _readAccount;
        __scope.onLanguageItemPress = _onLanguageItemPress;

    }

    function _readAccount() {

        __user.read(null, _onUserRead, null, _onFinish);

        function _onUserRead(_responseDatum) {
            __scope.userDatum = _responseDatum;
        }

        function _onFinish() {
            __scope.$apply();
        }

    }

    function _onLanguageItemPress(_targetLanguage) {

        __translator.set(_targetLanguage);

    }

}(window.allot));
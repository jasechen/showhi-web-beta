(function (__allot) {

    "use strict";

    var __ngApp = __allot("ng");
    var __scope;
    var __location;
    var __routeParams;
    var __api;
    var __group;
    var __dialogue;
    var __documentPress;

    (function () {

        _groupmemberController.$inject = ["$scope", "$location", "$routeParams", "$path", "$api", "$group", "$dialogue", "$documentPress"];
        __ngApp.controller("GroupmemberController", _groupmemberController);

    }());

    function _groupmemberController(_scope, _location, _routeParams, _path, _api, _group, _dialogue, _documentPress) {

        __scope = _scope;
        __location = _location;
        __routeParams = _routeParams;
        __api = _api;
        __group = _group;
        __dialogue = _dialogue;
        __documentPress = _documentPress;
        _defineVariable();
        _defineFunction();
        _readGroup();

    }

    function _defineVariable() {

        __scope.modeName = "Group";
        __scope.groupDatum = {};
        __scope.invitationDatum = {};
        __scope.roleDictionary = {
                owner: __scope.lyric.GROUPMEMBER_ROLE_OWNER,
                admin: __scope.lyric.GROUPMEMBER_ROLE_ADMIN,
                recruiter: __scope.lyric.GROUPMEMBER_ROLE_RECRUITER,
                editor: __scope.lyric.GROUPMEMBER_ROLE_EDITOR,
                member: __scope.lyric.GROUPMEMBER_ROLE_MEMBER,
            };

    }

    function _defineFunction() {

        __scope.readGroup = _readGroup;
        __scope.getMemberAvatarStyle = _getMemberAvatarStyle;
        __scope.confirmPrivacy = _confirmPrivacy;
        __scope.confirmNegativeRole = _confirmNegativeRole;
        __scope.confirmNegativeType = _confirmNegativeType;
        __scope.onAllowClick = _onAllowClick;
        __scope.onOptionClick = _onOptionClick;
        __scope.onDeleteClick = _onDeleteClick;
        __scope.onLeaveClick = _onLeaveClick;
        __scope.onRecruitableClick = _onRecruitableClick;
        __scope.onDisrecruitableClick = _onDisrecruitableClick;
        __scope.onChangeRoleClick = _onChangeRoleClick;
        __scope.onInviteClick = _onInviteClick;

    }

    function _readGroup() {

        var _id = __routeParams.groupID;
        if ( _id ) __group.read(_id, _onRead);

        function _onRead(_responseDatum) {
            __scope.groupDatum = _responseDatum;
        }

    }

    function _getMemberAvatarStyle(_memberItem) {

        return {
                "background-image": "url(" + _memberItem.avatarUrl + ")"
            };

    }

    function _confirmPrivacy(_privacyType) {

        var _currentMember = __scope.groupDatum.currentUser;
        if ( _currentMember ) return _processConfirm();

        function _processConfirm() {
            var _ROLE_OWNER = ( _currentMember.role === "owner" );
            var _ROLE_ADMIN = ( _currentMember.role === "admin" );
            var _ROLE_RECRUITER = ( _currentMember.role === "recruiter" );
            var _ROLE_EDITOR = ( _currentMember.role === "editor" );
            var _TYPE_OWN = ( _privacyType === "own" );
            var _TYPE_ROLE = ( _privacyType === "role" ) || ( _privacyType === "option" ) ;
            var _TYPE_ALLOW = ( _privacyType === "allow" );
            var _TYPE_PEOPLE = ( _privacyType === "invite" ) || ( _privacyType === "remove" );
            if ( _TYPE_OWN ) if ( _ROLE_OWNER ) return true;
            if ( _TYPE_ROLE ) if ( _ROLE_OWNER || _ROLE_ADMIN ) return true;
            if ( _TYPE_PEOPLE ) if ( _ROLE_OWNER || _ROLE_ADMIN || _ROLE_RECRUITER ) return true;
            if ( _TYPE_ALLOW ) if ( _ROLE_OWNER || _ROLE_ADMIN || _ROLE_RECRUITER || _ROLE_EDITOR ) return true;
        }

    }

    function _confirmNegativeRole(_memberItem) {

        var _roleArray = Array.prototype.slice.call(arguments);
        _roleArray.shift();
        if ( !(_roleArray) ) return true;
        var _regex = RegExp( "^(" + _roleArray.join("|") + ")$" );
        return !(_regex.test(_memberItem.role));

    }

    function _confirmNegativeType(_memberItem) {

        var _roleArray = Array.prototype.slice.call(arguments);
        _roleArray.shift();
        if ( !(_roleArray) ) return true;
        var _regex = RegExp( "^(" + _roleArray.join("|") + ")$" );
        return !(_regex.test(_memberItem.user_type));

    }

    function _onAllowClick(_memberItem) {

        __api.groupUsersAllow(
                __scope.groupDatum.id,
                _memberItem.id,
                "enable"
            )
            .success(_onSuccess);

        function _onSuccess() {
            _readGroup();
        }

    }

    function _onOptionClick(_memberItem) {

        _toggleOption(_memberItem);

    }

    function _onDeleteClick(_memberItem) {

        __dialogue.confirm(__scope.lyric.GROUPMEMBER_REMOVE_BUTTON, __scope.lyric.GROUPMEMBER_REMOVE_ASKING, _onConfirm);

        function _onConfirm() {
            __api.groupUsersDelete(__scope.groupDatum.id, _memberItem.id)
                .success(_onSuccess);
        }

        function _onSuccess() {
            _readGroup();
        }

    }

    function _onLeaveClick() {

        __dialogue.confirm(__scope.lyric.GROUPMEMBER_LEAVE_BUTTON, __scope.lyric.GROUPMEMBER_LEAVE_ASKING, _onConfirm);

        function _onConfirm() {
            __api.groupUsersLeave(__scope.groupDatum.id, __scope.groupDatum.currentUser.id)
                .success(_onSuccess);
        }

        function _onSuccess() {
            __location.path("/");
            __scope.$apply();
        }

    }

    function _onRecruitableClick() {

        __dialogue.confirm(__scope.lyric.GROUPMEMBER_ENABLE_RECRUITER_BUTTON, __scope.lyric.GROUPMEMBER_ENABLE_RECRUITER_ASKING, _onConfirm);

        function _onConfirm() {
            __api.groupSetWindower(__scope.groupDatum.id, 1)
                .success(_onSuccess);
        }

        function _onSuccess() {
            _readGroup();
        }

    }

    function _onDisrecruitableClick() {

        __dialogue.confirm(__scope.lyric.GROUPMEMBER_DISABLE_RECRUITER_BUTTON, __scope.lyric.GROUPMEMBER_DISABLE_RECRUITER_ASKING, _onConfirm);

        function _onConfirm() {
            __api.groupSetWindower(__scope.groupDatum.id, 0)
                .success(_onSuccess);
        }

        function _onSuccess() {
            _readGroup();
        }

    }

    function _onChangeRoleClick(_memberItem, _targetRole) {

        if ( !/^(admin|recruiter|editor|member)$/.test(_targetRole) ) return;
        __api.groupUsersRole(__scope.groupDatum.id, _memberItem.id, _targetRole)
            .success(_onSuccess);

        function _onSuccess() {
            _readGroup();
        }

    }

    function _onInviteClick() {

        __api.groupUsers(__scope.groupDatum.id, __scope.invitationDatum.mail)
            .success(_onSuccess);

        function _onSuccess() {
            _readGroup();
        }

    }

    function _toggleOption(_memberItem) {

        var _originalOptionedId = __scope.handleOptionedId;
        __scope.handleOptionedId = 0;
        if ( ( _memberItem || {} ).id === _originalOptionedId ) return;
        __scope.handleOptionedId = _memberItem.id;

    }

}(window.allot));
<?php

    require "class.JavaScriptPacker.php";

    class Minpacker {

        public static function conbine($aryofcontents) {

            $contentstr = "";

            while ( count($aryofcontents) ) {
                $contentstr .= array_shift($aryofcontents) . "\r\n\r\n";
            }

            return $contentstr;

        }

        public static function js($aryoffiles, $isdebug) {

            $contentstr = Minpacker::getconbinedfiles($aryoffiles);

            if ($isdebug) return $contentstr;

            $t1 = microtime(true);

            $packer = new JavaScriptPacker($contentstr, 'None', true, true);
            $packed = $packer->pack();

            $t2 = microtime(true);
            $time = sprintf('%.4f', ($t2 - $t1) );

            return $packed;

        }

        public static function css($aryoffiles) {

            $contentstr = Minpacker::getconbinedfiles($aryoffiles);
            $contentstr = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $contentstr);
            $contentstr = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $contentstr);

            return $contentstr;

        }

        public static function getconbinedfiles($aryoffiles) {

            $contentstr = "";

            while ( count($aryoffiles) ) {
                $contentstr .= file_get_contents(array_shift($aryoffiles)) . "\r\n\r\n";
            }

            return $contentstr;

        }

    }

?>
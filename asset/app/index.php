<?php

    header_remove();

    if ( empty($_GET["nm"]) ) {
        echo "nm EMPTY !!";
        exit();
    }

    require "minpacker/app.php";

    switch ( $_GET["nm"] ) {

        case "script":
            $etc_js_files = array(
                    "../script/config.js",
                    "../script/core/allot.js",
                    "../script/core/main.js",
                    "../script/core/const.js",
                    "../script/core/provider.js",
                    "../script/controller/dashboard.js",
                    "../script/controller/profile.js",
                    "../script/controller/group.js",
                    "../script/controller/newgroup.js",
                    "../script/controller/register.js",
                    "../script/controller/verify.js",
                    "../script/controller/invite.js",
                    "../script/controller/groupmember.js",
                    "../script/controller/groupedit.js",
                    "../script/controller/groupinvite.js",
                    "../script/controller/publicgroups.js",
                    "../script/controller/appoint.js",
                    "../script/controller/appointmaker.js",
                    "../script/controller/access.js",
                    "../script/controller/langpicker.js",
                    "../script/controller/home.js",
                    "../script/controller/pedia.js",
                    "../script/directive/header.js",
                    "../script/directive/dock.js",
                    "../script/directive/vitae.js",
                    "../script/directive/bulletin.js",
                    "../script/directive/board.js",
                    "../script/directive/resume.js",
                    "../script/directive/proffer.js",
                    "../script/directive/notification.js",
                    "../script/directive/login.js",
                    "../script/directive/footer.js",
                    "../script/factory/path.js",
                    "../script/factory/device.js",
                    "../script/factory/util.js",
                    "../script/factory/claim.js",
                    "../script/factory/api.js",
                    "../script/factory/storage.js",
                    "../script/factory/session.js",
                    "../script/factory/account.js",
                    "../script/factory/user.js",
                    "../script/factory/group.js",
                    "../script/factory/friend.js",
                    "../script/factory/translator.js",
                    "../script/factory/dialogue.js",
                    "../script/factory/cropper.js",
                    "../script/factory/documentPress.js",
                    "../script/factory/interim.js",
                    "../script/service/wheel.js",
                    "../script/core/boot.js"
                );
            header("content-type: text/javascript");
            echo Minpacker::js($etc_js_files, true);
            break;

        case "test_script":
            $etc_js_files = array(
                    "../script/core/allot.js",
                    "../script/core/consts.js",
                    "../script/core/main.js"
                );
            $etc_js_str = Minpacker::js($etc_js_files, true);
            $js_str = array(
                    file_get_contents("../lib/jquery.datepicker/jquery.datepicker.min.js"),
                    $etc_js_str
                );
            header("content-type: text/javascript");
            echo Minpacker::conbine($js_str);
            break;

        case "style":
            $etc_css_files = array(
                    "../style/basic.css",
                    "../style/container.css",
                    "../style/header.css",
                    "../style/home.css",
                    "../style/dock.css",
                    "../style/vitae.css",
                    "../style/bulletin.css",
                    "../style/board.css",
                    "../style/proffer.css",
                    "../style/resume.css",
                    "../style/notification.css",
                    "../style/dialogue.css",
                    "../style/login.css",
                    "../style/footer.css",
                    "../style/register.css",
                    "../style/newgroup.css",
                    "../style/groupmember.css",
                    "../style/groupedit.css",
                    "../style/publicgroups.css",
                    "../style/appoint.css",
                    "../style/appointmaker.css",
                    "../style/access.css",
                    "../style/crop.css",
                    "../style/langpicker.css",
                    "../style/404.css",
                    "../style/pedia.css"
                );
            header("content-type: text/css");
            echo Minpacker::css($etc_css_files);
            break;

        default:
            echo "nm ERROR !!";
            break;

    }

    exit();

?>